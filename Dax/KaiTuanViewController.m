//
//  KaiTuanViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/5/2.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "KaiTuanViewController.h"
#import "ConfirmViewController.h"
#import "DetailInfo.h"
#import "LoginViewController.h"
@interface KaiTuanViewController ()<UIWebViewDelegate>

@end

@implementation KaiTuanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:mTuan]];
    self.mWebView.delegate = self;
    [self.mWebView loadRequest:request];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
    
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *url = request.URL.absoluteString;
    if([url hasPrefix:@"native://order"]  && appDelegate().isLoginApp){
        //do something you want
        NSString *pId = [url substringFromIndex:19];
        JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
        NSString *reqType = mproductDetail;
        NSDictionary *dict = @{@"productId" : pId};
        [params addGetParams:dict];
        [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载..."];
        [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
            [[DAXUtils sharedInstance] hideProgressHud];
            if (resultCode == JRF_RESULT_OK && [resultObj isKindOfClass:[NSDictionary class]]) {

                NSDictionary *dic = [resultObj objectForKey:@"product"];
                NSMutableArray *taskArray = [NSMutableArray array];
                DetailInfo *detailInfo = [[DetailInfo alloc] initWithDictionary:dic];
                detailInfo.amount =  [NSNumber numberWithInteger:1];
                [taskArray addObject:detailInfo];
                [detailInfo release];
                ConfirmViewController *pdvc = [[ConfirmViewController alloc] initWithNibName:@"ConfirmViewController" bundle:nil];
                pdvc.hidesBottomBarWhenPushed = YES;
                pdvc.datasource = taskArray;
                UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
                self.navigationItem.backBarButtonItem = backItem;
                backItem.title = @"返回";
                [self.navigationController pushViewController:pdvc animated:YES];
                [pdvc release];
            }
            
        }];
    }
    else if ([url hasPrefix:@"native://group?action=add&"]  && appDelegate().isLoginApp){
        //do something you want
        NSString *gId = [url substringFromIndex:34];
        JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
        NSString *reqType = mgroupDetail;
        NSDictionary *dict = @{@"groupId" : gId};
        [params addGetParams:dict];
        [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载..."];
        [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
            [[DAXUtils sharedInstance] hideProgressHud];
            if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
                if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                    //int count = [self.datasource count]-indexPath.row-1;
                    NSDictionary *temp  = [resultObj objectForKey:@"object"];
                    NSDictionary *dic = [temp objectForKey:@"product"];
                    float price = [[temp objectForKey:@"personPrice"] floatValue];
                    NSMutableArray *taskArray = [NSMutableArray array];
                    DetailInfo *detailInfo = [[DetailInfo alloc] initWithDictionary:dic];
                    detailInfo.amount =  [NSNumber numberWithInteger:1];
                    detailInfo.price =  [NSNumber numberWithFloat:price];
                    [taskArray addObject:detailInfo];
                    [detailInfo release];
                    ConfirmViewController *pdvc = [[ConfirmViewController alloc] initWithNibName:@"ConfirmViewController" bundle:nil];
                    pdvc.hidesBottomBarWhenPushed = YES;
                    pdvc.datasource = taskArray;
                    pdvc.flag = 1;
                    pdvc.groupId = gId;
                    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
                    self.navigationItem.backBarButtonItem = backItem;
                    backItem.title = @"返回";
                    [self.navigationController pushViewController:pdvc animated:YES];
                    [pdvc release];
                                    }
                else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"0"]) {
                    NSString *msg = [resultObj objectForKey:@"info"];
                   [WFHudView showMsg:msg inView:nil];
                }
            }
            
        }];

    }
    else if ([url hasPrefix:@"native://group?action=attend&"]  && appDelegate().isLoginApp){
            //do something you want
            NSString *gId = [url substringFromIndex:45];
            JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
            NSString *reqType = mgroupInstance;
            NSDictionary *dict = @{@"groupinstanceId" : gId};
            [params addGetParams:dict];
            [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载..."];
            [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                [[DAXUtils sharedInstance] hideProgressHud];
                if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        //int count = [self.datasource count]-indexPath.row-1;
                        NSDictionary *temp  = [resultObj objectForKey:@"object"];
                        temp = [temp objectForKey:@"group"];
                        NSDictionary *dic = [temp objectForKey:@"product"];
                        float price = [[temp objectForKey:@"personPrice"] floatValue];
                        NSMutableArray *taskArray = [NSMutableArray array];
                        DetailInfo *detailInfo = [[DetailInfo alloc] initWithDictionary:dic];
                        detailInfo.amount =  [NSNumber numberWithInteger:1];
                        detailInfo.price =  [NSNumber numberWithFloat:price];
                        [taskArray addObject:detailInfo];
                        [detailInfo release];
                        ConfirmViewController *pdvc = [[ConfirmViewController alloc] initWithNibName:@"ConfirmViewController" bundle:nil];
                        pdvc.hidesBottomBarWhenPushed = YES;
                        pdvc.datasource = taskArray;
                        pdvc.flag = 2;
                        pdvc.groupId = gId;
                        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
                        self.navigationItem.backBarButtonItem = backItem;
                        backItem.title = @"返回";
                        [self.navigationController pushViewController:pdvc animated:YES];
                        [pdvc release];
                    }
                    else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"0"]) {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                    }
                }
                
            }];
    }
    else if (![url hasPrefix:mTuan]  && !appDelegate().isLoginApp){
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
     [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [[DAXUtils sharedInstance] hideProgressHud];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [[DAXUtils sharedInstance] hideProgressHud];
   // mAlertView(@"提示", @"网络异常");
}

- (void)dealloc {
    [_mWebView release];
    [super dealloc];
}

@end
