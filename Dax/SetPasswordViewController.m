//
//  SetPasswordViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/4/22.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "SetPasswordViewController.h"
#import "TabBarViewController.h"
#import <SMS_SDK/SMS_SDK.h>
#import "SecurityUtil.h"
#define Interval   30
@interface SetPasswordViewController ()<UITextFieldDelegate>
@property (retain, nonatomic) NSTimer *timer;
@property int timeCount;
@end

@implementation SetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tfPhone.delegate = self;
    self.tfPassword.delegate = self;
    self.tfConfirmPassword.delegate = self;
    self.tfVerify.delegate = self;
    self.tfPhone.keyboardType = UIKeyboardTypeNumberPad;
    self.tfPassword.keyboardType = UIKeyboardTypeAlphabet;
    self.tfConfirmPassword.keyboardType = UIKeyboardTypeAlphabet;
    self.tfVerify.keyboardType = UIKeyboardTypeNumberPad;
    self.tfPassword.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.tfPhone.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.tfConfirmPassword.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.tfVerify.clearButtonMode=UITextFieldViewModeWhileEditing;
    
    self.mFetchVerify.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.mFetchVerify.layer.cornerRadius = 5.0;
    self.mFetchVerify.layer.borderWidth = 1.5;
    self.mFetchVerify.layer.borderColor =[UIColor colorWithRed:1 green:.77 blue:0 alpha:1].CGColor;
    self.mModifyPassword.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.mModifyPassword.layer.cornerRadius = 5.0;
    self.mModifyPassword.layer.borderWidth = 1.0;
    self.mModifyPassword.layer.borderColor =[UIColor colorWithRed:1 green:.77 blue:0 alpha:1].CGColor;

    NSArray *list=self.navigationController.navigationBar.subviews;
    
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    //self.navigationItem.title = @"找回密码";
    [self.mFetchVerify addTarget:self action:@selector(clickVerifyNum:) forControlEvents:UIControlEventTouchUpInside];
    [self.mModifyPassword addTarget:self action:@selector(clickModifyPassword:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    if(self.isRegister)
        [self.mModifyPassword setTitle: @"注册" forState:UIControlStateNormal];
    
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[JRFHttpClient shareInstance] cancelHttpRequest:mZhuce];
    [[JRFHttpClient shareInstance] cancelHttpRequest:mPassword];
}

- (void)excuteTime {
    if (!self.timer) {
        self.mFetchVerify.userInteractionEnabled = NO;
        self.timeCount = Interval;
        [self.mFetchVerify setTitle:[NSString stringWithFormat:@"%d秒", self.timeCount] forState:UIControlStateNormal];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    }
}



#pragma mark - button 功能

- (void)clickVerifyNum:(id)sender{
    [self.tfPhone resignFirstResponder];
    [self.tfVerify resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    [self.tfConfirmPassword resignFirstResponder];
    //self.tfPhone.text = @"13127626898";
    if (![[AppFun sharedInstance] isConnectNetwork]) {
        [WFHudView showMsg:@"网络好像出了点问题" inView:nil];
        return;
    }
    NSString *phoneNum = [self.tfPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([phoneNum isEqualToString:@""]) {
        mAlertView(@"提示", @"手机号码不能为空！");
        return;
    }
    //手机格式错误
    NSString *regex = @"^1\\d{10}$";
    //    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
    //[a-z0-9A-Z]+[-|_\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}
    NSPredicate *result = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if (![result evaluateWithObject:phoneNum]) {
        mAlertView(@"提示", @"手机号码格式错误, 请您再检查下!");
        return;
    }
    else{
        self.mFetchVerify.backgroundColor = [UIColor whiteColor];
        [self.mFetchVerify setTitleColor:[UIColor colorWithRed:1 green:.77 blue:0 alpha:1] forState:UIControlStateNormal];
        [self excuteTime];
        [SMS_SDK getVerificationCodeBySMSWithPhone:self.tfPhone.text
                                              zone:@"86"
                                            result:^(SMS_SDKError *error)
         {
             if (!error)
             {
                 NSLog(@"成功");
             }
             else
             {
                 UIAlertView* alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"codesenderrtitle", nil)
                                                               message:[NSString stringWithFormat:@"状态码：%zi ,错误描述：%@",error.errorCode,error.errorDescription]
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"确定", nil)
                                                     otherButtonTitles:nil, nil];
                 [alert show];
             }
             
         }];
    }
    

}


- (void) clickModifyPassword:(id)sender{
    [self.tfPhone resignFirstResponder];
    [self.tfVerify resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    [self.tfConfirmPassword resignFirstResponder];
    //self.tfPhone.text = @"13127626898";
    if (![[AppFun sharedInstance] isConnectNetwork]) {
        [WFHudView showMsg:@"网络好像出了点问题" inView:nil];
        return;
    }
    NSString *phoneNum = [self.tfPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *passwordOld = [self.tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *passwordNew = [self.tfConfirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *codeVerify  = [self.tfVerify.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([phoneNum isEqualToString:@""]) {
        mAlertView(@"提示", @"手机号码不能为空！");
        return;
    }
    //手机格式错误
    NSString *regex = @"^1\\d{10}$";
    //    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
    //[a-z0-9A-Z]+[-|_\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}
    NSPredicate *result = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if (![result evaluateWithObject:phoneNum]) {
        mAlertView(@"提示", @"手机号码格式错误, 请您再检查下!");
        return;
    }
    if ([passwordOld isEqualToString:@""]) {
        mAlertView(@"提示", @"请输入密码！");
        return;
    }
    if ([passwordNew isEqualToString:@""]) {
        mAlertView(@"提示", @"请确认密码！");
        return;
    }
    if (![passwordNew isEqualToString:passwordOld]) {
        mAlertView(@"提示", @"两次输入密码不一样！");
        return;
    }
    if ([codeVerify isEqualToString:@""]) {
        mAlertView(@"提示", @"请输入验证码！");
        return;
    }

    else{
        if (self.isRegister) {
            JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
            //int code = [codeVerify intValue];
            passwordNew = [SecurityUtil encryptAESData:passwordOld app_key:@"dxyxx52dxyxx1314" ] ;
            passwordNew = [passwordNew stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
            NSDictionary *dict = @{@"phone": phoneNum,
                                   @"password":passwordNew,@"code":codeVerify,@"type":@"ios"};
            [params addPostParams:dict];
            //__block OrderListViewController *myself = self;
            [[JRFHttpClient shareInstance] fetchNetworkData:mZhuce param:params block:^(JRFResultCode resultCode, id resultObj) {
                [[DAXUtils sharedInstance] hideProgressHud];
                if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        //int count = [self.datasource count]-indexPath.row-1;
                        NSDictionary *temp  = [resultObj objectForKey:@"object"];
                        [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"name"] forKey:@"name"];
                        [[NSUserDefaults standardUserDefaults] setObject:passwordNew forKey:@"password"];
                        [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"phone"] forKey:@"phone"];
                        [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"image"] forKey:@"portrait"];
                        [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"username"] forKey:@"username"];
                        [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"sex"] forKey:@"sex"];
                        [[NSUserDefaults standardUserDefaults] setObject:[self transformTodate:[temp objectForKey:@"birthday"]] forKey:@"birthday"];
                         [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970]] forKey:@"lastLoginTime"];//保存登录时间
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        appDelegate().isLoginApp = true;
                        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        TabBarViewController *mTabBar = [storyBoard instantiateViewControllerWithIdentifier:@"idTabBar"];
                        mTabBar.selectedIndex = 2;
                        [(AppDelegate*)[UIApplication sharedApplication].delegate sendProviderDeviceToken:appDelegate().token];
                        [self presentViewController:mTabBar animated:NO completion:nil];
                        NSLog(@"temp");
                    }
                    else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"0"]) {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        mAlertView(@"提示", msg);
                        
                    }
                }
                
            }];

        }
        else{
            JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
            int code = [codeVerify intValue];
            passwordNew = [SecurityUtil encryptAESData:passwordOld app_key:@"dxyxx52dxyxx1314" ] ;
            passwordNew = [passwordNew stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];

            NSDictionary *dict = @{@"phone": phoneNum,
                                   @"password":passwordNew,@"code":[NSNumber numberWithInt:code],@"type":@"ios"};
            [params addPostParams:dict];
            //__block OrderListViewController *myself = self;
            [[JRFHttpClient shareInstance] fetchNetworkData:mPassword param:params block:^(JRFResultCode resultCode, id resultObj) {
                [[DAXUtils sharedInstance] hideProgressHud];
                if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        //int count = [self.datasource count]-indexPath.row-1;
                        [self.navigationController popViewControllerAnimated:NO];
                    }
                    else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"0"]) {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        mAlertView(@"提示", msg);
                        
                    }
                }
                
            }];

        }
    }
    
    
}


#pragma mark   ==============时间戳转时间的方法==============
- (NSString *)transformTodate:(NSString *) mTime
{
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    //设置时区,这个对于时间的处理有时很重要
    //例如你在国内发布信息,用户在国外的另一个时区,你想让用户看到正确的发布时间就得注意时区设置,时间的换算.
    //例如你发布的时间为2010-01-26 17:40:50,那么在英国爱尔兰那边用户看到的时间应该是多少呢?
    //他们与我们有7个小时的时差,所以他们那还没到这个时间呢...那就是把未来的事做了
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    // 时间戳转时间的方法
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[mTime longLongValue]/1000 ];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}


- (void)updateTime:(NSTimer *)time {
    if (self.timeCount <= 1) {
        self.mFetchVerify.userInteractionEnabled = YES;
        self.mFetchVerify.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
        
        [self.mFetchVerify setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.mFetchVerify setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.timer invalidate];
        self.timer = nil;
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mFetchVerify setTitle:[NSString stringWithFormat:@"%d秒", self.timeCount] forState:UIControlStateNormal];
            self.mFetchVerify.backgroundColor = [UIColor whiteColor];
            [self.mFetchVerify setTitleColor:[UIColor colorWithRed:1 green:.77 blue:0 alpha:1] forState:UIControlStateNormal];
        });
        self.timeCount--;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{   //开始编辑时，整体上移
    if (textField == self.tfVerify && [[AppDelegate deviceString] isEqualToString:@"iPhone 4"]) {
        [self moveView:-80];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{     //结束编辑时，整体下移
    if (textField == self.tfVerify && [[AppDelegate deviceString] isEqualToString:@"iPhone 4"]) {
        [self moveView:80];
    }
}
-(void)moveView:(float)move{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y +=move;//view的X轴上移
    self.view.frame = frame;
    //[UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}

//点击done 按钮 去掉键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.tfVerify resignFirstResponder];
    [self.tfConfirmPassword resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    [self.tfPhone resignFirstResponder];
}

- (void)dealloc {
    [_tfPhone release];
    [_tfPassword release];
    [_tfConfirmPassword release];
    [_tfVerify release];
    [super dealloc];
}
@end
