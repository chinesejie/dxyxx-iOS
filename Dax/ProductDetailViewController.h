//
//  ProductDetailViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/4/25.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailViewController : UIViewController
@property (nonatomic, copy) NSString *productId;
@property (nonatomic, retain) NSMutableArray *datasource;
- (IBAction)back:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIScrollView *detailScrollview;
@end
