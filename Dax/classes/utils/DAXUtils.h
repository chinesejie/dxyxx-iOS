//
//  DAXUtils.h
//  DAX
//
//  Created by fengjia on 14-5-23.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
@interface DAXUtils : NSObject {
    MBProgressHUD *HUD;
}
+ (id)sharedInstance;
- (void)showProgressHud:(UIView *)inView withMessage:(NSString *)message;
- (void)hideProgressHud;
- (void)showProgressHudAndHideDelay:(NSTimeInterval)delay inView:(UIView *)inView withMessage:(NSString *)message;

- (void)clearAppCache;
- (NSString *)fetchAppCache;
@end
