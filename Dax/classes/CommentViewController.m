//
//  CommentViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/12.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CommentViewController.h"
#import "FJPullTableView.h"
#import "EditViewController.h"
#import "LoginViewController.h"
#import "CommentInfo.h"
#import "YMTableViewCell.h"
#import "ContantHead.h"
#import "YMShowImageView.h"
#import "YMTextData.h"
#import "YMReplyInputView.h"
#import "WFReplyBody.h"
#import "WFMessageBody.h"
#import "WFPopView.h"
#import "WFHudView.h"
#import "WFActionSheet.h"
#import "Tools.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#define dataCount 10
#define kLocationToBottom 0
#define kAdmin [[NSUserDefaults standardUserDefaults] objectForKey:@"username"]

@interface CommentViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate,cellDelegate,InputDelegate,UIActionSheetDelegate>
{
    NSMutableArray *_imageDataSource;
    
    NSMutableArray *_contentDataSource;//模拟接口给的数据
    
    NSMutableArray *_tableDataSource;//tableview数据源
    
    NSMutableArray *_shuoshuoDatasSource;//说说数据源
    
    UIView *popView;
    
    YMReplyInputView *replyView ;
    
    NSInteger _replyIndex;
    
}
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (retain, nonatomic) NSArray *itemname;
@property (nonatomic,retain) UISegmentedControl *segment;
@property (nonatomic, retain) NSDictionary *dict;
@property (nonatomic, retain) NSDictionary *relpys;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL hasNextPage;
@property int offset;
@property int pagecount;
@property (nonatomic,strong) WFPopView *operationView;
@property (nonatomic,strong) NSIndexPath *selectedIndexPath;
//@property (nonatomic, retain) InboxInfo *Info;
- (void)addPullTableView;

@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datasource = [[NSMutableArray alloc] init];
    _contentDataSource = [[NSMutableArray alloc] init];
    self.navigationItem.title = @"热评";
    //分页实现
    self.itemname = @[@"我的评论", @"热门评论"];
    self.segment = [[UISegmentedControl alloc]initWithItems:self.itemname];
    self.segment.frame = CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20, [[UIScreen mainScreen] bounds].size.width, 40);
    [self.segment addTarget:self action:@selector(selected:) forControlEvents:UIControlEventValueChanged];
    self.segment.selectedSegmentIndex = 1;
    [self.view addSubview:self.segment ];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!self.isLoadTaskDone){
        self.offset = 1;
        [self fetchTaskLists];
    }
    
    if (self.segment.selectedSegmentIndex==0 && !appDelegate().isLoginApp) {
        [_tableDataSource removeAllObjects];
        [self.pullTableView reloadData];
        mAlertView(@"提示", @"请先登录");
    }
    [self.segment setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    self.segment.tintColor = [UIColor clearColor];
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor lightGrayColor],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateNormal];
    
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor colorWithRed:1 green:.77 blue:0 alpha:1],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateSelected];
    
    [self.segment setDividerImage:[UIImage imageNamed:@"divider.png"]
              forLeftSegmentState:UIControlStateNormal
                rightSegmentState:UIControlStateNormal
                       barMetrics:UIBarMetricsDefault];
    
    
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
    UIBarButtonItem *Comment= [[UIBarButtonItem alloc] initWithTitle:@"评论" style:UIBarButtonItemStyleDone target:self action:@selector(toComment:)];
    self.navigationItem.rightBarButtonItem = Comment;
    
}




- (void)fetchTaskLists{
    JRFParams *params = [[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_];
    NSString *mUrl ;
    if(self.segment.selectedSegmentIndex == 1){
        NSDictionary *dict = @{@"current":[NSNumber numberWithInt:self.offset],
                               @"count":@10};
        [params addGetParams:dict];
        mUrl = mNewComment;
    }
    else if (appDelegate().isLoginApp){
        NSDictionary *dict = @{@"current":[NSNumber numberWithInt:self.offset],
                               @"count":@10,
                               @"phone":[[NSUserDefaults standardUserDefaults] objectForKey:@"phone"]};
        [params addGetParams:dict];
        mUrl = mineComment;
    }
    else{
        [_tableDataSource removeAllObjects];
        [self.pullTableView reloadData];
        mAlertView(@"提示", @"请先登录");
        return;
    }
    //__block OrderListViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mUrl param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                self.isLoadTaskDone = YES;
                self.dict = [[resultObj objectForKey:@"object"] objectForKey:@"recordList"];
                self.hasNextPage = [[[resultObj objectForKey:@"object"] objectForKey:@"hasNextPage"] boolValue ];
                self.pagecount = [[[resultObj objectForKey:@"object"] objectForKey:@"pageCount"]  intValue];
                [self getData];
                if (!self.pullTableView) {
                    [self addPullTableView];
                }
                [self loadTextData];
            }
            
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        appDelegate().isLoginApp =  true;
                        [self fetchTaskLists];
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                        
                    }
                }];
            }
            else {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
        
    }];
}




#pragma mark   ==============数据提取==============
- (void) getData
{
    _tableDataSource = [[NSMutableArray alloc] init];
    _replyIndex = -1;//代表是直接评论
    
    //NSMutableArray *taskArray = [NSMutableArray array];
    if (!self.dict) {
        return;
    }
    if(self.offset <= self.pagecount)
        self.offset++;
    if(self.isRefresh){
        [_contentDataSource removeAllObjects];
        [self.datasource removeAllObjects];
    }
    
    
    for (NSDictionary *tempDict in self.dict) {
        
        CommentInfo *commentInfo = [[CommentInfo alloc] initWithDictionary:tempDict];
        WFMessageBody *messBody1 = [[WFMessageBody alloc] init];
        messBody1.posterContent = commentInfo.content;
        messBody1.posterPostImage = [commentInfo.images componentsSeparatedByString:@";"];
        messBody1.posterReplies = [NSMutableArray array];
        self.relpys = [tempDict objectForKey:@"replys"];
        //NSMutableArray *taskArray = [NSMutableArray array];
        for (NSDictionary *tempDict in self.relpys) {
            WFReplyBody *body1 = [[WFReplyBody alloc] init];
            body1.replyUser = [tempDict objectForKey:@"userName"];
            body1.repliedUser = @"";
            body1.replyInfo = [tempDict objectForKey:@"word"];
            [messBody1.posterReplies addObject:body1];
        }
        messBody1.posterImgstr = commentInfo.thumb;
        messBody1.posterName = commentInfo.user;
        messBody1.posterIntro = [Tools transTimeSp:commentInfo.time];//时间
        messBody1.posterFavour = [NSMutableArray arrayWithObjects: nil];
        messBody1.isFavour = YES;
        
        [_contentDataSource addObject:messBody1];
        [self.datasource addObject:commentInfo.commentId];
    }
    
    //[self.datasource removeAllObjects];
    //[self.datasource addObjectsFromArray:taskArray];
}

#pragma mark -加载数据
- (void)loadTextData{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray * ymDataArray =[[NSMutableArray alloc]init];
        for (int i = 0 ; i < _contentDataSource.count; i ++) {
            
            WFMessageBody *messBody = [_contentDataSource objectAtIndex:i];
            
            YMTextData *ymData = [[YMTextData alloc] init ];
            ymData.messageBody = messBody;
            
            [ymDataArray addObject:ymData];
            
        }
        [self calculateHeight:ymDataArray];
        
    });
}

#pragma mark - 计算高度
- (void)calculateHeight:(NSMutableArray *)dataArray{
    
    
    NSDate* tmpStartData = [NSDate date];
    for (YMTextData *ymData in dataArray) {
        
        ymData.shuoshuoHeight = [ymData calculateShuoshuoHeightWithWidth:self.view.frame.size.width withUnFoldState:NO];//折叠
        
        ymData.unFoldShuoHeight = [ymData calculateShuoshuoHeightWithWidth:self.view.frame.size.width withUnFoldState:YES];//展开
        
        ymData.replyHeight = [ymData calculateReplyHeightWithWidth:self.view.frame.size.width];
        
        ymData.favourHeight = [ymData calculateFavourHeightWithWidth:self.view.frame.size.width];
        
        [_tableDataSource addObject:ymData];
        
    }
    
    double deltaTime = [[NSDate date] timeIntervalSinceDate:tmpStartData];
    NSLog(@"cost time = %f", deltaTime);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
        //[self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
    });
    
    
}

-(void)selected:(id)sender{
    UISegmentedControl* control = (UISegmentedControl*)sender;
    switch (control.selectedSegmentIndex) {
        case 0:
            self.isRefresh = YES;
            self.offset = 1;
            [self fetchTaskLists];
            break;
        default:
            self.isRefresh = YES;
            self.offset = 1;
            [self fetchTaskLists];
            break;
    }
}
- (void)addPullTableView {
    
    self.pullTableView = [[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+60,  self.view.frame.size.width,  self.view.frame.size.height-155)];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
}

#pragma mark - UITableViewDelegate




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  _tableDataSource.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    YMTextData *ym = [_tableDataSource objectAtIndex:indexPath.row];
    BOOL unfold = ym.foldOrNot;
    return TableHeader + kLocationToBottom + ym.replyHeight + ym.showImageHeight  + kDistance + (ym.islessLimit?0:30) + (unfold?ym.shuoshuoHeight:ym.unFoldShuoHeight) + kReplyBtnDistance + ym.favourHeight + (ym.favourHeight == 0?0:kReply_FavourDistance)+20;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ILTableViewCell";
    
    YMTableViewCell *cell = (YMTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[YMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.stamp = indexPath.row;
    cell.replyBtn.appendIndexPath = indexPath;
    [cell.replyBtn addTarget:self action:@selector(replyAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.delegate = self;
    [cell setYMViewWith:[_tableDataSource objectAtIndex:indexPath.row]];
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    /*
     CATransform3D rotation;
     rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
     rotation.m34 = 1.0/ -600;
     
     cell.layer.shadowColor = [[UIColor blackColor]CGColor];
     cell.layer.shadowOffset = CGSizeMake(10, 10);
     cell.alpha = 0;
     cell.layer.transform = rotation;
     cell.layer.anchorPoint = CGPointMake(0, 0.5);
     
     
     [UIView beginAnimations:@"rotation" context:NULL];
     [UIView setAnimationDuration:0.8];
     cell.layer.transform = CATransform3DIdentity;
     cell.alpha = 1;
     cell.layer.shadowOffset = CGSizeMake(0, 0);
     [UIView commitAnimations];
     */
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}



////////////////////////////////////////////////////////////////////

#pragma mark - 按钮动画

- (void)replyAction:(YMButton *)sender{
    
    CGRect rectInTableView = [self.pullTableView rectForRowAtIndexPath:sender.appendIndexPath];
    CGFloat origin_Y = rectInTableView.origin.y + sender.frame.origin.y;
    CGRect targetRect = CGRectMake(CGRectGetMinX(sender.frame), origin_Y, CGRectGetWidth(sender.bounds), CGRectGetHeight(sender.bounds));
    if (self.operationView.shouldShowed) {
        [self.operationView dismiss];
        return;
    }
    _selectedIndexPath = sender.appendIndexPath;
    YMTextData *ym = [_tableDataSource objectAtIndex:_selectedIndexPath.row];
    [self.operationView showAtView:self.pullTableView rect:targetRect isFavour:ym.hasFavour];
}



- (WFPopView *)operationView {
    if (!_operationView) {
        _operationView = [WFPopView initailzerWFOperationView];
        WS(ws);
        _operationView.didSelectedOperationCompletion = ^(WFOperationType operationType) {
            switch (operationType) {
                case WFOperationTypeLike:
                    
                    [ws addLike];
                    break;
                case WFOperationTypeReply:
                    [ws replyMessage: nil];
                    break;
                default:
                    break;
            }
        };
    }
    return _operationView;
}

#pragma mark - 赞
- (void)addLike{
    
    YMTextData *ymData = (YMTextData *)[_tableDataSource objectAtIndex:_selectedIndexPath.row];
    WFMessageBody *m = ymData.messageBody;
    if (m.isFavour == YES) {//此时该取消赞
        [m.posterFavour removeObject:kAdmin];
        m.isFavour = NO;
    }else{
        [m.posterFavour addObject:kAdmin];
        m.isFavour = YES;
    }
    ymData.messageBody = m;
    
    
    //清空属性数组。否则会重复添加
    
    [ymData.attributedDataFavour removeAllObjects];
    
    
    ymData.favourHeight = [ymData calculateFavourHeightWithWidth:self.view.frame.size.width];
    [_tableDataSource replaceObjectAtIndex:_selectedIndexPath.row withObject:ymData];
    
    [self.pullTableView reloadData];
    
}

//写评论
-(void) toComment:(id) sender{
    if(appDelegate().isLoginApp){
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        EditViewController *pdvc = [[EditViewController alloc] initWithNibName:@"EditViewController" bundle:nil];
        [self.navigationController pushViewController:pdvc animated:YES];
        
    }
    else{
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
    }
    
}



//发送评论
-(void) toSendmsg:(NSString *)replyText commentId:(NSString *) mId{
    NSDictionary *dict =  @{@"commentId":mId,
                            @"word":replyText};
    
    
    
        //获取网络数据
        AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
        //异步访问网络，回调是主线程，更新UI
        [manager POST:mAddReplys parameters:dict success:^(AFHTTPRequestOperation *operation, id resultObj) {
            [[DAXUtils sharedInstance] hideProgressHud];
            if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
                if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                    NSLog(@"评论成功");
                }
    
                else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                    NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                    NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                    JRFParams *params = [[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_];
                    NSString *reqType = mLoginApp;
                    NSDictionary *dict1 = @{@"phone" : phoneNum,
                                            @"password" : passwordOld};
                    [params addPostParams:dict1];
                    [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                        [[DAXUtils sharedInstance] hideProgressHud];
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self toSendmsg:replyText commentId:mId];
                        }
                        else {
                            NSString *msg = [resultObj objectForKey:@"info"];
                            [WFHudView showMsg:msg inView:nil];
                            return ;
    
                        }
                    }];
                }
                else {
                    NSString *msg = [resultObj objectForKey:@"info"];
                    [WFHudView showMsg:msg inView:nil];
                    return ;
                }
            }
    
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[DAXUtils sharedInstance] hideProgressHud];
            [WFHudView showMsg:@"网络好像出了点问题" inView:nil];
            return ;
        }];
    
    
}


#pragma mark - 真の评论
- (void)replyMessage:(YMButton *)sender{
    if (appDelegate().isLoginApp) {
        
        if (replyView) {
            return;
        }
        replyView = [[YMReplyInputView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 44, screenWidth,44) andAboveView:self.view];
        replyView.delegate = self;
        replyView.replyTag = _selectedIndexPath.row;
        [self.view addSubview:replyView];
    }
    else{
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
    }
    
    
}




#pragma mark -cellDelegate
- (void)changeFoldState:(YMTextData *)ymD onCellRow:(NSInteger)cellStamp{
    
    [_tableDataSource replaceObjectAtIndex:cellStamp withObject:ymD];
    [self.pullTableView reloadData];
    
}

#pragma mark - 图片点击事件回调
- (void)showImageViewWithImageViews:(NSArray *)imageViews byClickWhich:(NSInteger)clickTag{
    
    UIView *maskview = [[UIView alloc] initWithFrame:self.view.bounds];
    maskview.backgroundColor = [UIColor blackColor];
    [self.view.window addSubview:maskview];
    
    YMShowImageView *ymImageV = [[YMShowImageView alloc] initWithFrame:self.view.bounds byClick:clickTag appendArray:imageViews];
    [ymImageV show:maskview didFinish:^(){
        
        [UIView animateWithDuration:0.1f animations:^{
            
            ymImageV.alpha = 0.0f;
            maskview.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
            
            [ymImageV removeFromSuperview];
            [maskview removeFromSuperview];
        }];
        
    }];
    
}




#pragma mark - 长按评论整块区域的回调
- (void)longClickRichText:(NSInteger)index replyIndex:(NSInteger)replyIndex{
    
    [self.operationView dismiss];
    YMTextData *ymData = (YMTextData *)[_tableDataSource objectAtIndex:index];
    WFReplyBody *b = [ymData.messageBody.posterReplies objectAtIndex:replyIndex];
    
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = b.replyInfo;
    
}

#pragma mark - 点评论整块区域的回调
- (void)clickRichText:(NSInteger)index replyIndex:(NSInteger)replyIndex{
    
    [self.operationView dismiss];
    
    _replyIndex = -1;
    
    //YMTextData *ymData = (YMTextData *)[_tableDataSource objectAtIndex:index];
    // WFReplyBody *b = [ymData.messageBody.posterReplies objectAtIndex:replyIndex];
    
    //回复
    if (replyView) {
        return;
    }
    replyView = [[YMReplyInputView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 44, screenWidth,44) andAboveView:self.view];
    replyView.delegate = self;
    //replyView.lblPlaceholder.text = [NSString stringWithFormat:@"回复%@:",b.replyUser];
    replyView.replyTag = index;
    [self.view addSubview:replyView];
    
}

#pragma mark - 评论说说回调
- (void)YMReplyInputWithReply:(NSString *)replyText appendTag:(NSInteger)inputTag{
    
    YMTextData *ymData = nil;
    if (_replyIndex == -1) {
        
        WFReplyBody *body = [[WFReplyBody alloc] init];
        body.replyUser = kAdmin;
        body.repliedUser = @"";
        body.replyInfo = replyText;
        
        ymData = (YMTextData *)[_tableDataSource objectAtIndex:inputTag];
        WFMessageBody *m = ymData.messageBody;
        [m.posterReplies addObject:body];
        ymData.messageBody = m;
        
    }else{
        
        ymData = (YMTextData *)[_tableDataSource objectAtIndex:inputTag];
        WFMessageBody *m = ymData.messageBody;
        
        WFReplyBody *body = [[WFReplyBody alloc] init];
        body.replyUser = kAdmin;
        body.repliedUser = [(WFReplyBody *)[m.posterReplies objectAtIndex:_replyIndex] replyUser];
        body.replyInfo = replyText;
        
        [m.posterReplies addObject:body];
        ymData.messageBody = m;
        
    }
    NSString *mId = [self.datasource objectAtIndex:inputTag];
    [self toSendmsg:replyText commentId:mId];
    
    //清空属性数组。否则会重复添加
    [ymData.completionReplySource removeAllObjects];
    [ymData.attributedDataReply removeAllObjects];
    
    
    ymData.replyHeight = [ymData calculateReplyHeightWithWidth:self.view.frame.size.width];
    [_tableDataSource replaceObjectAtIndex:inputTag withObject:ymData];
    
    [self.pullTableView reloadData];
    
}

- (void)destorySelf{
    
    //  NSLog(@"dealloc reply");
    [replyView removeFromSuperview];
    replyView = nil;
    _replyIndex = -1;
    
}

- (void)actionSheet:(WFActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        //delete
        YMTextData *ymData = (YMTextData *)[_tableDataSource objectAtIndex:actionSheet.actionIndex];
        WFMessageBody *m = ymData.messageBody;
        [m.posterReplies removeObjectAtIndex:_replyIndex];
        ymData.messageBody = m;
        [ymData.completionReplySource removeAllObjects];
        [ymData.attributedDataReply removeAllObjects];
        
        
        ymData.replyHeight = [ymData calculateReplyHeightWithWidth:self.view.frame.size.width];
        [_tableDataSource replaceObjectAtIndex:actionSheet.actionIndex withObject:ymData];
        
        [self.pullTableView reloadData];
        
    }else{
        
    }
    _replyIndex = -1;
}



#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidScroll:scrollView];
    }
    [self.operationView dismiss];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidEndDragging:scrollView];
    }
}


#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    
    self.isRefresh = YES;
    self.offset = 1;
    if(self.segment.selectedSegmentIndex == 0)
        [self fetchTaskLists];
    else {
        [self fetchTaskLists];
    }
}
- (void)beginToLoadMoreData:(FJPullTableView *)tableView {
    self.isRefresh = NO;
    if(self.segment.selectedSegmentIndex == 0)
        [self fetchTaskLists];
    else {
        [self fetchTaskLists];
    }
}

@end
