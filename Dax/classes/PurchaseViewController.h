//
//  PurchaseViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/3/16.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailInfo.h"

@interface PurchaseViewController : UIViewController
- (IBAction)back:(id)sender;
- (IBAction)share:(UIButton *)sender;
- (IBAction)comment:(UIButton *)sender;

@property (retain, nonatomic) IBOutlet UIButton *fav;
@property (retain, nonatomic) IBOutlet UIView *toolbar;
@property int curPage;
@property (retain, nonatomic) DetailInfo *mProuduct;
@property (nonatomic, retain) NSString *cId;
@end
