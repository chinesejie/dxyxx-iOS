//
//  UploadImage.h
//  Dax
//
//  Created by Jay_Ch on 15/5/30.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploadImage : NSObject
+ (void)uploadImageWithImage:(UIImage * ) image success:(void (^)(NSString * md5))success fail:(void (^)())fail;
+(void) uploadImagesWithImages:(NSArray *)images success:(void (^)(NSArray *))success fail:(void (^)())fail;
@end
