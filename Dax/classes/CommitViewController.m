//
//  CommitViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/20.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CommitViewController.h"
#import "ConfirmViewController.h"
#import "FJPullTableView.h"
#import "ListViewCell.h"
#import "TabBarViewController.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "Order.h"
#import "ProductInfo.h"
#import "DetailInfo.h"
#import "DataSigner.h"
#import <AlipaySDK/AlipaySDK.h>
#import "APAuthV2Info.h"
#import "WXApi.h"
#import "WFHudView.h"
@interface CommitViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (retain, nonatomic) UIButton *mBack;
@property (retain, nonatomic) UIButton *mAddtocart;
@property (retain, nonatomic) UIView *mBottom;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property int offset;
//@property (nonatomic, retain) InboxInfo *Info;
- (void)addPullTableView;
@end

@implementation CommitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datasource = [NSMutableArray array];
    self.navigationItem.title = @"订单提交成功";
    self.navigationItem.hidesBackButton = TRUE;
    [self addPullTableView];
    self.automaticallyAdjustsScrollViewInsets = false;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    //[self initializeDefaultDataList];
    [super viewWillAppear:animated];
    [self getData];
    [self bottombar];
}


//buttom bar
-(void)bottombar {
    //底栏
    CGRect rect = CGRectMake(0,[[UIScreen mainScreen] bounds].size.height,[[UIScreen mainScreen] bounds].size.width,44);
    self.mBottom = [[UIView alloc]initWithFrame:rect];
    //view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview: self.mBottom ];
    self.mBottom .layer.borderWidth = 0.5f;
    self.mBottom .layer.borderColor = [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor;
    self.mBottom.backgroundColor = [UIColor whiteColor];
    CGRect frame_1 = CGRectMake(0,0, 120,  self.mBottom.frame.size.height);
    self.mBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.mBack  setFrame:frame_1];
    [self.mBack  setFrame:frame_1];
    [self.mBack setTitle:@"返回首页" forState:UIControlStateNormal];
    [self.mBack setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.mBack.titleLabel.font=[UIFont systemFontOfSize:16];

    [self.mBack  addTarget:self action:@selector(toHome:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame_2= CGRectMake(self.mBottom.frame.size.width-120,0, 120,  self.mBottom.frame.size.height);
    self.mAddtocart = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.mAddtocart setFrame:frame_2];
    [self.mAddtocart setTitle:@"立即支付" forState:UIControlStateNormal];
    [self.mAddtocart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.mAddtocart.titleLabel.font=[UIFont systemFontOfSize:19];
    [self.mAddtocart addTarget:self action:@selector(toPay:) forControlEvents:UIControlEventTouchUpInside];
    self.mAddtocart.backgroundColor = [UIColor orangeColor];
    //[btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    
    [self.mBottom addSubview:self.mBack];
    [self.mBottom addSubview:self.mAddtocart];
    
    
    
    
    //底部滑出
    [UIView animateWithDuration:0.4f
                          delay:0.6
                        options:UIViewAnimationCurveEaseInOut
                     animations:^{
                         [self.view addSubview:self.mBottom];
                         self.mBottom.center = CGPointMake(self.mBottom.center.x, [[UIScreen mainScreen] bounds].size.height-22);
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}



- (void)addPullTableView {
    
    
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20,  self.view.frame.size.width,  self.view.frame.size.height-108)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
}

#pragma mark - UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return [self.datasource count];
            break;
        default:
            return 1;
            break;
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && self.flag != 3) {
        return 116.0f;
        
    }
    else
        return 35.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 90.0f;
        
    }
    else
        
        return 35.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UILabel * orderno = [[[UILabel alloc] init] autorelease];
        orderno.frame = CGRectMake(15,7,80, 21);
        orderno.font=[UIFont fontWithName:@"Arial" size:14];
        orderno.text = @"订单编号 ：";
        orderno.textColor = [UIColor darkGrayColor];
        
        UILabel * xiangqing = [[[UILabel alloc] init] autorelease];
        xiangqing.frame = CGRectMake(15,37,80, 21);
        xiangqing.font=[UIFont fontWithName:@"Arial" size:14];
        xiangqing.text = @"提货详情 ：";
        xiangqing.textColor = [UIColor darkGrayColor];
        
        UILabel * jine = [[[UILabel alloc] init] autorelease];
        jine.frame = CGRectMake(15,67,80, 21);
        jine.font=[UIFont fontWithName:@"Arial" size:14];
        jine.text = @"待付金额 ：";
        jine.textColor = [UIColor darkGrayColor];
        
        UILabel * no = [[[UILabel alloc] init] autorelease];
        no.frame = CGRectMake(90,7,165, 21);
        no.font=[UIFont fontWithName:@"Arial" size:16];
        no.text = [self.dict objectForKey:@"uuid"];
        no.textColor = [UIColor darkGrayColor];
        
        UILabel * date = [[[UILabel alloc] init] autorelease];
        date.frame = CGRectMake(self.view.frame.size.width-95,7,90,21);
        date.font=[UIFont fontWithName:@"Arial" size:11];
        date.text = [self transformTodate];
        date.textColor = [UIColor darkGrayColor];
        
        UILabel * quhuo = [[[UILabel alloc] init] autorelease];
        quhuo.frame = CGRectMake(90,37,80, 21);
        quhuo.font=[UIFont fontWithName:@"Arial" size:14];
        if([[self.dict objectForKey:@"bufferInterval"] intValue] == 0)
            quhuo.text = @"自提 当天";
        else  if([[self.dict objectForKey:@"bufferInterval"] intValue] == 1)
            quhuo.text = @"自提 第二天";
        else
            quhuo.text = @"自提 第三天";
        quhuo.textColor = [UIColor darkGrayColor];
        
        UILabel * unit = [[[UILabel alloc] init] autorelease];
        unit.frame = CGRectMake(90,67,10, 21);
        unit.font=[UIFont fontWithName:@"Arial" size:12];
        unit.text = @"￥";
        unit.textColor = [UIColor redColor];
        
        UILabel * amount = [[[UILabel alloc] init] autorelease];
        amount.frame = CGRectMake(103,65,200, 21);
        amount.font=[UIFont fontWithName:@"Arial" size:20];
        if([self.dict objectForKey:@"discount"] == [NSNull null] )
            amount.text = [NSString stringWithFormat:@"%.2f",[[self.dict objectForKey:@"money"] floatValue]];
        else
             amount.text  = [NSString stringWithFormat:@"%.2f",[[self.dict objectForKey:@"money"] floatValue] * [[self.dict objectForKey:@"discount"] floatValue] ]; //商品价格
        amount.textColor = [UIColor redColor];
        
        
        UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 90.0f)] autorelease];
        [sectionView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [sectionView addSubview:orderno];
        [sectionView addSubview:xiangqing];
        [sectionView addSubview:jine];
        [sectionView addSubview:no];
        [sectionView addSubview:date];
        [sectionView addSubview:quhuo];
        [sectionView addSubview:unit];
        [sectionView addSubview:amount];
        return sectionView;
        
        
    }
    else {
        UILabel * label = [[[UILabel alloc] init] autorelease];
        label.frame = CGRectMake(15,8,79, 21);
        label.font=[UIFont fontWithName:@"Arial" size:15];
        label.text = @"支付详情";
        label.textColor = [UIColor lightGrayColor];
        UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35.0f)] autorelease];
        
        [sectionView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [sectionView addSubview:label];
        return sectionView;
        
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section ==0){
        static NSString *identifyCell1 = @"ListCell";
        ListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (ListViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"ListViewCell"
                                                                  owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        ProductInfo *product = [self.datasource objectAtIndex:indexPath.row];
        DetailInfo *detail = [[DetailInfo alloc] initWithDictionary:product.mProduct];
        NSString *picUrl = mSetimage;
        picUrl = [picUrl  stringByAppendingFormat:@"%@", detail.thumbId];
        [cell.mThumb setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
        cell.mTitle.text = detail.mDescription;
        cell.mDescription.text = detail.mSpecification;
        cell.mPrice.text = [NSString stringWithFormat:@"%@", @([product.mPrice floatValue])];
        cell.stepper.value = [detail.amount intValue];
        cell.amount.text = [NSString stringWithFormat:@"%@", @([product.amount intValue])];
        float total = [product.mPrice floatValue]*[product.amount intValue];
        cell.total.text = [NSString stringWithFormat:@"%@",@(total)];
        [cell.total setNumberOfLines:1];
        [cell.total sizeToFit];
        cell.amount.hidden = FALSE;
        cell.mySymbol.hidden = FALSE;
        cell.myEqual.hidden = FALSE;
        cell.total.hidden = FALSE;
        //cell.unit.hidden = FALSE;
        return cell;
    }else{
        static NSString *identifyCell2 = @"PayCell";
        ListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell2];
        if (!cell) {
            cell=[[ListViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.lbValue.hidden = YES;
            //cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        //[cell configHomeTableViewCell];
        //config the cell
        UILabel * label = [[[UILabel alloc] init] autorelease];
        label.frame = CGRectMake(15,8,79, 21);
        label.font=[UIFont fontWithName:@"Arial" size:15];
        label.text = @"支付方式";
        label.textColor = [UIColor darkGrayColor];
        
        UILabel * label2 = [[[UILabel alloc] init] autorelease];
        label2.frame = CGRectMake(self.view.frame.size.width-82,8,79, 21);
        label2.font=[UIFont fontWithName:@"Arial" size:15];
        
        if ([[self.dict objectForKey:@"payType"]  isEqualToString:@"alipay"])
            label2.text = @"支付宝支付";
        else if([[self.dict objectForKey:@"payType"] isEqualToString:@"tenpay"])
            label2.text = @"微信支付";
        else
            label2.text = @"余额支付";
        label2.textColor = [UIColor lightGrayColor];
        
        [cell addSubview:label];
        [cell addSubview:label2];
        
        return cell;
    }
    
    
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     CATransform3D rotation;
     rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
     rotation.m34 = 1.0/ -600;
     
     cell.layer.shadowColor = [[UIColor blackColor]CGColor];
     cell.layer.shadowOffset = CGSizeMake(10, 10);
     cell.alpha = 0;
     cell.layer.transform = rotation;
     cell.layer.anchorPoint = CGPointMake(0, 0.5);
     
     
     [UIView beginAnimations:@"rotation" context:NULL];
     [UIView setAnimationDuration:0.8];
     cell.layer.transform = CATransform3DIdentity;
     cell.alpha = 1;
     cell.layer.shadowOffset = CGSizeMake(0, 0);
     [UIView commitAnimations];
     */
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    ConfirmViewController *pdvc = [[ConfirmViewController alloc] initWithNibName:@"ConfirmViewController" bundle:nil];
//    //int count = [self.datasource count]-indexPath.row-1;
//    
//    [self.navigationController pushViewController:pdvc animated:YES];
//    [pdvc release];
}



#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        CGFloat sectionHeaderHeight = 90.0f; //sectionHeaderHeight
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}


#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    
    self.isRefresh = YES;
    self.offset = 0;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
}
- (void)beginToLoadMoreData:(FJPullTableView *)tableView {
    self.isRefresh = NO;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
}


#pragma mark - button 功能
//进入主页面
-(void) toHome:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TabBarViewController *mTabBar = [storyBoard instantiateViewControllerWithIdentifier:@"idTabBar"];
    mTabBar.selectedIndex = 0;
    [self presentViewController:mTabBar animated:NO completion:nil];
    //     [UIView transitionFromView:self.view toView:tabbar.view duration:0.1f options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
    
    
}

//立即支付
-(void) toPay:(id)sender {
    
    if ([[self.dict objectForKey:@"payType"]  isEqualToString:@"alipay"])
        [self payorder];
    else if([[self.dict objectForKey:@"payType"] isEqualToString:@"tenpay"])
        [self sendPay];
    
    
}

#pragma mark   ==============数据提取==============
- (void) getData
{
    NSDictionary *dict;
    if (self.flag == 1 ||  self.flag == 2) {
        dict  = [self.dict objectForKey:@"group"];
        float price = [[dict objectForKey:@"personPrice"] floatValue];
        NSMutableArray *taskArray = [NSMutableArray array];
        ProductInfo *productInfo = [[ProductInfo alloc] initWithDictionary:dict];
        productInfo.mPrice =  [NSNumber numberWithFloat:price];
        productInfo.amount =  [NSNumber numberWithInteger:1];
        [taskArray addObject:productInfo];
        [productInfo release];
        
        [self.datasource removeAllObjects];
        [self.datasource addObjectsFromArray:taskArray];
    }
    else {
        dict = [self.dict objectForKey:@"orderDetails"];
        NSMutableArray *taskArray = [NSMutableArray array];
        for (NSDictionary *tempDict in dict) {
            ProductInfo *productInfo = [[ProductInfo alloc] initWithDictionary:tempDict];
            [taskArray addObject:productInfo];
            [productInfo release];
        }
        [self.datasource removeAllObjects];
        [self.datasource addObjectsFromArray:taskArray];
    }
    if (!dict) {
        return;
    }
    [self.pullTableView reloadData];
    NSLog(@"%@", dict);
    
    
}

#pragma mark   ==============时间戳转时间的方法==============
- (NSString *)transformTodate
{
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    
    //设置时区,这个对于时间的处理有时很重要
    //例如你在国内发布信息,用户在国外的另一个时区,你想让用户看到正确的发布时间就得注意时区设置,时间的换算.
    //例如你发布的时间为2010-01-26 17:40:50,那么在英国爱尔兰那边用户看到的时间应该是多少呢?
    //他们与我们有7个小时的时差,所以他们那还没到这个时间呢...那就是把未来的事做了
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    // 时间戳转时间的方法
     NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[self.dict objectForKey:@"postTime"] longLongValue]/1000 ];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}




#pragma mark   ==============支付宝点击订单支付行为==============
//
//选中商品调用支付宝极简支付
//
- (void) payorder
{
    
    /*
     *商户的唯一的parnter和seller。
     *签约后，支付宝会为每个商户分配一个唯一的 parnter 和 seller。
     */
    
    /*============================================================================*/
    /*=======================需要填写商户app申请的===================================*/
    /*============================================================================*/
    NSString *partner = @"2088611480899716 ";
    NSString *seller = @"dxyxx88@163.com";
    NSString *privateKey = @"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAN/RBX2K2N1VobOIeV5WQsDORIXzEW01eyk6cXHP+TIaYjL8hd7RpXYkrmPSmU7+dt3yZXTUs3nP124K5gj3jStIv+Fjpa4XuvKKJ/wfWEcDBwY74i7KgOy903t95TFxjNLkHF7wOOUazlNNh/3eeRzOlSnW5OmQkAKWqZG9C8U1AgMBAAECgYEAkUaQZMO6LjiGBA2ikew12gZJDhUPC676KXGo7zIKU04yzVuB3XaBCuWWWG77Wf3r0/ahiisB8CBLej8Sv2FHvEjohbRWPIqO4waxV57oZPtBIg1HvrNKYpTI7+OSGLUx8l24rCahionhca61xIJn0cbX6FypDgIINC6Va0NC4uUCQQDz8XTKBQhC70Jnfqnl4d9oTTRH1CqY7YOgbP7eMIDCw8XN1k5zOtgHPPFrS8zI6Hs3q7+J+kPxERbajPmCGgx7AkEA6uDoaM9P6EkQTLaI663/7aQqXg6hXaCK7EjFmFugxPa6/clDhOVYnKb1pJcIQELARwAk3ga6noy4BfqnbXn+DwJBANDHhKcqK2nuEC03sP04ldZUzTv0kAiWryLsZi2P4YPPvklu83GXmTCIri6gj0IcBukcqy/R67g0YhTmttzsomUCQC9t70CK7IXpiyMSkR+WaWHhjrSjm64+Zw9DurMDfbmIYUYySDIj5frsNBpibUYctJshylATZ8fwfpCmhvyyb70CQCnvSjbKIRAkfVieY1nM1jp/eu/1CRNOr7wxFrz634R/kozQOKYCQnn9m24QXpL2esMoeHfd5hq/nw/GcLQc9Tg=";
    /*============================================================================*/
    /*============================================================================*/
    /*============================================================================*/
    
    //partner和seller获取失败,提示
    if ([partner length] == 0 ||
        [seller length] == 0 ||
        [privateKey length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"缺少partner或者seller或者私钥。"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    Order *order = [[Order alloc] init];
    order.partner = partner;
    order.seller = seller;
    order.tradeNO = [self.dict objectForKey:@"uuid"]; //订单ID（由商家自行制定）
    order.productName = @"大象与猩猩水果"; //商品标题
    order.productDescription = @"水果"; //商品描述
    if([self.dict objectForKey:@"discount"] == [NSNull null] )
        order.amount = [NSString stringWithFormat:@"%.2f",[[self.dict objectForKey:@"money"] floatValue]]; //商品价格
    else
        order.amount = [NSString stringWithFormat:@"%.2f",[[self.dict objectForKey:@"money"] floatValue] * [[self.dict objectForKey:@"discount"] floatValue] ]; //商品价格
    order.notifyURL =  @"http://rs88881234.com:8081/pay/alipay/deal "; //回调URL
    
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showUrl = @"m.alipay.com";
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = @"dxyxx";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    NSLog(@"orderSpec = %@",orderSpec);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            if ([[resultDic objectForKey:@"resultStatus"] intValue] == 9000) {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                TabBarViewController *mTabBar = [storyBoard instantiateViewControllerWithIdentifier:@"idTabBar"];
                mTabBar.selectedIndex = 0;
                [self presentViewController:mTabBar animated:NO completion:nil];
            }
            NSLog(@"reslut = %@",resultDic);
        }];
        
    }
}



#pragma mark   ==============微信点击订单支付行为==============
- (void)sendPay
{
    //IOS5自带解析类NSJSONSerialization从response中解析出数据放到字典中
    
    
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"uuid":[self.dict objectForKey:@"uuid"]};
    [params addGetParams:dict];
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"处理中..."];
    [[JRFHttpClient shareInstance] fetchNetworkData:mWechatpay param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
            self.weChat = [resultObj objectForKey:@"object"];
            NSString *stamp  = [self.weChat objectForKey:@"timestamp"];
            
            //调起微信支付
            PayReq* req             = [[[PayReq alloc] init]autorelease];
            req.openID              = [self.weChat objectForKey:@"appid"];
            req.partnerId           = [self.weChat objectForKey:@"partnerid"];
            req.prepayId            = [self.weChat objectForKey:@"prepayid"];
            req.nonceStr            = [self.weChat objectForKey:@"noncestr"];
            req.timeStamp           = stamp.intValue;
            //            req.package             =  @"Sign=WXPay";
            req.package             =  [self.weChat objectForKey:@"package"];
            req.sign                = [self.weChat objectForKey:@"sign"];
            [WXApi sendReq:req];
            //日志输出
            NSLog(@"appid=%@\npartid=%@\nprepayid=%@\nnoncestr=%@\ntimestamp=%ld\npackage=%@\nsign=%@",req.openID,req.partnerId,req.prepayId,req.nonceStr,(long)req.timeStamp,req.package,req.sign );
            
            
            }else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self sendPay];
                        }
                        else {
                            NSString *msg = [resultObj objectForKey:@"info"];
                            [WFHudView showMsg:msg inView:nil];
                            
                        }
                    }
                }];
                
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }

        }
    }];
    
    

}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
