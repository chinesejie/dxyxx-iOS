//
// CommentViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/3/12.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property BOOL isLoadTaskDone;
@end
