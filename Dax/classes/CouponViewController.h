//
//  CouponViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/5/5.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponViewController : UIViewController
@property (retain, nonatomic) IBOutlet UILabel *Totalprice;
@property (retain, nonatomic) IBOutlet UITextField *Mymoney;
@property (retain, nonatomic) IBOutlet UIButton *tenpay;
@property (retain, nonatomic) IBOutlet UIButton *alipay;
@property (retain, nonatomic) IBOutlet UIButton *confirm;
@property (retain, nonatomic) IBOutlet UITextField *mNote;
- (IBAction)tes:(UITextField *)sender;
@property (retain, nonatomic) NSString *payType;
- (IBAction)test:(UITextField *)sender;
@property (retain, nonatomic) NSString *price;
@end

