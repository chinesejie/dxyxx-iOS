//
//  CommitViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/3/20.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommitViewController : UIViewController
@property (nonatomic, retain) NSMutableArray *datasource;
@property (nonatomic, retain) NSDictionary *dict;
@property (nonatomic, retain) NSDictionary *weChat;
@property int flag;
@end
