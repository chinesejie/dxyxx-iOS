//
//  HomeTableViewCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/12.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *homeimg;
@property (retain, nonatomic) IBOutlet UIView *borderview;
@property (nonatomic, retain) NSMutableArray *datasource;
- (void)configHomeTableViewCell;
@end
