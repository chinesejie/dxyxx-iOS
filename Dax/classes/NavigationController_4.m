//
//  NavigationController_4.m
//  Dax
//
//  Created by Jay_Ch on 15/3/20.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "NavigationController_4.h"

@interface NavigationController_4 ()

@end

@implementation NavigationController_4

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *selectedImage = [UIImage imageNamed:@"myfruit_active.png"];
    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.selectedImage = selectedImage ;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
