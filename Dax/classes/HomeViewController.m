//
//  HomeViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/11.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "HomeViewController.h"
#import "FJPullTableView.h"
#import "HomeTableViewCell.h"
#import "TopTableViewCell.h"
#import "PurchaseViewController.h"
#import "ProductInfo.h"
#import "DetailInfo.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "OrderListViewController.h"
#import "LoginViewController.h"
#import "KaiTuanViewController.h"
#import "CouponViewController.h"
#import "CouponListViewController.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "ProductInfo.h"
#import "MyfruitViewController.h"

#define  mCommonColor   mRGBColor(50, 42, 53)
@interface HomeViewController () <FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (nonatomic, retain) NSMutableArray *advertArray;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL isLoadAdvertDone;
@property BOOL isLoadTaskDone;
@property int offset;
- (void)fetchAdverts;
- (void)fetchTaskLists;
- (void)addPullTableView;
@end


@implementation HomeViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.title = @"首页";
    self.advertArray = [NSMutableArray array];
    self.datasource = [NSMutableArray array];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.isReloadPage = NO;
    
    //定制自己的风格的  UIBarButtonItem
    UIImage* image= [UIImage imageNamed:@"bar_location.png"];
    CGRect frame_1= CGRectMake(0, 0, image.size.width+100                                                                                   , image.size.height);
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:frame_1];
    [btn setTitle:@" 上海" forState:UIControlStateNormal];
    [btn setImage:image forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font=[UIFont systemFontOfSize:15];
    [btn addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    UIBarButtonItem *locationButton= [[UIBarButtonItem alloc] initWithCustomView:btn];
    //[self.navigationItem setLeftBarButtonItem:locationButton];
    
    NSArray *list=self.navigationController.navigationBar.subviews;
    
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
    if (!self.isLoadAdvertDone) 
        [self fetchAdverts];
    if(!self.isLoadTaskDone)
        [self fetchTaskLists];
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [animation setFromValue:[NSNumber numberWithFloat:1.2]];
    [animation setToValue:[NSNumber numberWithFloat:1]];
    [animation setDuration:0.6];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithControlPoints:.4 :1.3 :1 :1]];
    [self.pullTableView.layer addAnimation:animation forKey:@"bounceAnimation"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc {
    self.pullTableView = nil;
    self.datasource = nil;
    [[JRFHttpClient shareInstance] cancelHttpRequest:mTopbanner];
    [[JRFHttpClient shareInstance] cancelHttpRequest:mHomepagebanner];
    // [_tbView release];
    [super dealloc];
}


- (void)fetchAdverts {
    
    //获取网络数据
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    //异步访问网络，回调是主线程，更新UI
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [manager GET:mTopbanner parameters:nil success:^(AFHTTPRequestOperation *operation, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        NSDictionary *dict = resultObj;
        if (!dict) {
            return ;
        }
        NSLog(@"%@", dict);
        NSMutableArray *taskArray = [NSMutableArray array];
        for (NSDictionary *tempDict in dict) {
            ProductInfo *productInfo = [[ProductInfo alloc] initWithDictionary:tempDict];
            [taskArray addObject:productInfo];
            [productInfo release];
        }
        if (!taskArray) {
            return;
        }
        self.isLoadAdvertDone = YES;
        [self.advertArray removeAllObjects];
        [self.advertArray addObjectsFromArray:taskArray];
        if (!self.pullTableView) {
            [self addPullTableView];
        }
        [self.pullTableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[DAXUtils sharedInstance] hideProgressHud];
        mAlertView(@"提示",@"网络好像出了点问题");
    }];
    

}


- (void)fetchTaskLists {
    
    //获取网络数据
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    //异步访问网络，回调是主线程，更新UI
    [manager GET:mHomepagebanner parameters:nil success:^(AFHTTPRequestOperation *operation, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
            NSDictionary *dict = resultObj;
            if (!dict) {
                return ;
            }
            NSLog(@"%@", dict);
            NSMutableArray *taskArray = [NSMutableArray array];
            for (NSDictionary *tempDict in dict) {
                ProductInfo *productInfo = [[ProductInfo alloc] initWithDictionary:tempDict];
                [taskArray addObject:productInfo];
                [productInfo release];
            }
            if (!taskArray) {
                return;
            }
            self.isLoadTaskDone = YES;
            [self.datasource removeAllObjects];
            [self.datasource addObjectsFromArray:taskArray];
            if (!self.pullTableView) {
                [self addPullTableView];
            }
            [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
        

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[DAXUtils sharedInstance] hideProgressHud];
        mAlertView(@"提示",@"网络好像出了点问题");
    }];

}

- (void)addPullTableView {
    
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20, self.view.frame.size.width, self.view.frame.size.height-115)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
}

#pragma mark - UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
        default:
            return [self.datasource count];
            break;
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        return 254.0f;
    }
    else
        return ([[UIScreen mainScreen] bounds].size.width*3)/8;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        static NSString *identifyCell1 = @"TopCell";
        
        TopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopTableViewCell"
                                                                      owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.advertArray = self.advertArray;
        [cell configTopTableViewCell];
        [cell.mOrder addTarget:self action:@selector(mOrderList:) forControlEvents:UIControlEventTouchUpInside];
        [cell.mKaiTuan addTarget:self action:@selector(kaiTuan:) forControlEvents:UIControlEventTouchUpInside];
        [cell.mCoupon addTarget:self action:@selector(Coupon:) forControlEvents:UIControlEventTouchUpInside];
        [cell.mCouponlist addTarget:self action:@selector(CouponList:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else{
        static NSString *identifyCell2 = @"HomeCell";
        HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell2];
        if (!cell) {
            cell = (HomeTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"HomeTableViewCell"
                                                                       owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;;
        }
        if ([self.datasource count] != 0) {
            ProductInfo *product = [self.datasource objectAtIndex:indexPath.row];
            float width  = [[UIScreen mainScreen] bounds].size.width*1.5;
            NSString *picUrl = mSetimage;
            picUrl = [picUrl  stringByAppendingFormat:@"%@%@%f", product.imageId, @"?w=",width];
            [cell.homeimg setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
        }
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section != 0) {
        cell.layer.transform = CATransform3DMakeScale(1,1, 1);
        cell.layer.transform = CATransform3DMakeTranslation(0, 20, 0);
        
        //设置动画时间为0.25秒,xy方向缩放的最终值为1
        [UIView animateWithDuration:1.0f animations:^{
            cell.layer.transform = CATransform3DMakeScale(1, 1, 1);
            cell.layer.transform = CATransform3DMakeTranslation(0, 0, 0);
        } completion:^(BOOL finished) {
            //   [imageView setImageURL:[NSURL URLWithString:imgUrl]];
        }];
        
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        PurchaseViewController *pdvc = [[PurchaseViewController alloc] initWithNibName:@"PurchaseViewController" bundle:nil];
        ProductInfo *productInfo = [self.datasource objectAtIndex:indexPath.row];
        pdvc.mProuduct = [[DetailInfo alloc] initWithDictionary:productInfo.mProduct];
        pdvc.hidesBottomBarWhenPushed = YES;
        pdvc.cId = pdvc.mProuduct.cId;
        [self.navigationController pushViewController:pdvc animated:YES];
       // [self presentViewController:pdvc animated:YES completion:nil];
        [pdvc release];
       
    }
}

#pragma mark - 按钮功能
- (void) mOrderList:(id)sender{
    if (appDelegate().isLoginApp) {
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        OrderListViewController *pdvc = [[OrderListViewController alloc] initWithNibName:@"OrderListViewController" bundle:nil];
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
        
    }else{
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    
}


- (void) kaiTuan:(id)sender{
    KaiTuanViewController *pdvc = [[KaiTuanViewController alloc] initWithNibName:@"KaiTuanViewController" bundle:nil];
    pdvc.hidesBottomBarWhenPushed = YES;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    [self.navigationController pushViewController:pdvc animated:YES];
    //[self presentViewController:pdvc animated:YES completion:nil];
    [pdvc release];
    
}

- (void)Coupon:(id)sender{
    if (appDelegate().isLoginApp) {
        CouponViewController *pdvc = [[CouponViewController alloc] initWithNibName:@"CouponViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        //[self presentViewController:pdvc animated:YES completion:nil];
        [pdvc release];
    }else{
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    
}

- (void)CouponList:(id)sender{
    if (appDelegate().isLoginApp) {
        CouponListViewController *pdvc = [[CouponListViewController alloc] initWithNibName:@"CouponListViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        //[self presentViewController:pdvc animated:YES completion:nil];
        [pdvc release];
    }else{
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    
}




#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidScroll:scrollView];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidEndDragging:scrollView];
    }
}

#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    
    
    self.isRefresh = YES;
    self.offset = 0;
    [self fetchAdverts];
    [self fetchTaskLists];
    
}
- (void)beginToLoadMoreData:(FJPullTableView *)tableView {
    self.isRefresh = NO;
    [self fetchAdverts];
    [self fetchTaskLists];
   // [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
}



@end
