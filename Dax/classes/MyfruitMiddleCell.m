//
//  MyfruitMiddleCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/21.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "MyfruitMiddleCell.h"

@implementation MyfruitMiddleCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
// 自绘分割线
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor);
    CGContextStrokeRect(context, CGRectMake(0,rect.size.height, rect.size.width,0.1));
}

- (void)dealloc {
    [_myImage release];
    [_myTitle release];
    [super dealloc];
}
@end
