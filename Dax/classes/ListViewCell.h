//
//  ListViewCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/16.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKYStepper.h"

@interface ListViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIButton *orderCheck;

@property (retain, nonatomic) IBOutlet UIButton *mycart;
@property (retain, nonatomic) IBOutlet PKYStepper *stepper;
@property (retain, nonatomic) IBOutlet UILabel *mySymbol;
@property (retain, nonatomic) IBOutlet UILabel *amount;
@property (retain, nonatomic) IBOutlet UILabel *myEqual;
@property (retain, nonatomic) IBOutlet UILabel *total;
@property (retain, nonatomic) IBOutlet UILabel *unit;
@property (retain, nonatomic) IBOutlet UIImageView *mThumb;
@property (retain, nonatomic) IBOutlet UILabel *mPrice;
@property (retain, nonatomic) IBOutlet UILabel *mTitle;
@property (retain, nonatomic) IBOutlet UILabel *mDescription;

@end
