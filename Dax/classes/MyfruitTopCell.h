//
//  MyfruitTopCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/21.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyfruitTopCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIButton *mAccount;
@property (retain, nonatomic) IBOutlet UILabel *jifen;

@property (retain, nonatomic) IBOutlet UILabel *mMoney;
@property (retain, nonatomic) IBOutlet UILabel *mQuan;
@property (retain, nonatomic) IBOutlet UILabel *Name;

@end
