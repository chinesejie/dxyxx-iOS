//
//  TopPurhcaseCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/17.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "TopPurhcaseCell.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"

@implementation TopPurhcaseCell


- (void)awakeFromNib {
    // Initialization code

    self.stepper.valueChangedCallback = ^(PKYStepper *stepper, int count) {
        stepper.countLabel.text = [NSString stringWithFormat:@"%@", @(count)];
    };
    [self.stepper setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configTopCell{
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
        self.pageCtrl.pageIndicatorTintColor = [UIColor whiteColor];
        //self.pageCtrl.currentPageIndicatorTintColor = mCommonColor;
    }
    self.mPrice.text = self.price;
    NSLog(@"%@",self.price);
    self.title.text = self.mDescription;
    self.imageId = [self.imageIds componentsSeparatedByString:@";"];
    NSLog(@"%lu", (unsigned long)[self.imageId count]);
    self.advertScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([[UIScreen mainScreen] bounds]), CGRectGetWidth([[UIScreen mainScreen] bounds]))];
    self.advertScrollView.contentSize = CGSizeMake([self.imageId count] * [[UIScreen mainScreen] bounds].size.width,50);
    self.advertScrollView.pagingEnabled = YES;
    self.advertScrollView.showsHorizontalScrollIndicator = NO;
    self.advertScrollView.showsVerticalScrollIndicator = NO;
    self.advertScrollView.delegate = self;
    [self addSubview:self.advertScrollView];
    for (int i = 0; i < [self.imageId count]; i++) {
        UIImageView *igv = [[UIImageView alloc] initWithFrame:CGRectMake(i * [[UIScreen mainScreen] bounds].size.width, 0, CGRectGetWidth([[UIScreen mainScreen] bounds]), CGRectGetHeight(self.advertScrollView.frame))];
        float width  = [[UIScreen mainScreen] bounds].size.width*1;
        NSString *picUrl = mSetimage;
        picUrl = [picUrl  stringByAppendingFormat:@"%@%@%f", [self.imageId objectAtIndex:i], @"?w=",width];
        [igv setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];        
        [self.advertScrollView addSubview:igv];
        [igv release];
    }
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.advertScrollView addGestureRecognizer:gesture];
    [gesture release];
    
    self.pageCtrl.numberOfPages = [self.imageId count];
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(handleScrollTimer:) userInfo:nil repeats:YES];
    
}

#pragma mark - UIScrollView
- (void)handleScrollTimer:(NSTimer *)time {
    self.curPage++;
    if (self.curPage >= [self.imageId count]) {
        self.curPage = 0;
    }
    self.pageCtrl.currentPage = self.curPage;
    CGRect rect = CGRectMake(self.curPage * self.advertScrollView.frame.size.width, 0, self.advertScrollView.frame.size.width, self.advertScrollView.frame.size.height);
    [self.advertScrollView scrollRectToVisible:rect animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.advertScrollView) {
        int index = floor(scrollView.contentOffset.x / scrollView.frame.size.width);
        self.curPage = index;
        self.pageCtrl.currentPage = index;
    }
}
- (void)handleTap:(UITapGestureRecognizer *)gesture {
    /*
     NSLog(@"tap index: %d", self.curPage);
     TaskInfo *task = [self.advertArray objectAtIndex:self.curPage];
     TaskDetailViewController *pdvc = [[TaskDetailViewController alloc] initWithNibName:@"TaskDetailViewController" bundle:nil];
     pdvc.taskId = task.tId;
     [self.navigationController pushViewController:pdvc animated:YES];
     [pdvc release];
     */
}


- (void)dealloc {
    [_stepper release];
    [_title release];
    [_mPrice release];
    [super dealloc];
}
@end
