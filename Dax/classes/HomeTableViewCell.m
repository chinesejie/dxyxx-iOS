//
//  HomeTableViewCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/12.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "ProductInfo.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.borderview.layer.borderWidth = 0.5f;
    self.borderview.layer.borderColor = [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)configHomeTableViewCell{
    
}

- (void)dealloc {
    [_homeimg release];
    [_borderview release];
    [super dealloc];
}
@end
