//
//  MyfruitMiddleCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/21.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyfruitMiddleCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *myImage;
@property (retain, nonatomic) IBOutlet UILabel *myTitle;

@end
