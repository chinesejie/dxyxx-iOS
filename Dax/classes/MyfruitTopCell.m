//
//  MyfruitTopCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/21.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "MyfruitTopCell.h"

@implementation MyfruitTopCell

- (void)awakeFromNib {
    // Initialization code
    UIImage *pic = [ UIImage imageNamed:@"bg.png"];
    UIImageView *imageView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size .height)];
    [imageView setImage:pic];
    [self setBackgroundColor:[UIColor colorWithRed:1 green:.77 blue:0 alpha:0.8]];
    [self setBackgroundView:imageView];
    
    self.mAccount.layer.cornerRadius = 35.0;
    self.mAccount.layer.borderWidth = 2.0;
    self.mAccount.layer.borderColor =[UIColor orangeColor].CGColor;
    self.mAccount.clipsToBounds = TRUE;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_mAccount release];
    [_jifen release];
    [_mMoney release];
    [_mQuan release];
    [super dealloc];
}
@end
