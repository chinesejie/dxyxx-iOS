//
//  WebViewController.m
//  DAX
//
//  Created by feng jia on 14-5-13.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import "ZXingObjc.h"
#import "ScanViewController.h"


@interface ScanViewController ()
@property BOOL isConnect;
@end

@implementation ScanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    hasGenerateBarcode = false;
    self.view.backgroundColor = [UIColor colorWithRed:233.0/255 green:220.0/255 blue:207.0/255 alpha:1];
    NSError* error=nil;
    if(self.uuid)
        self.navigationItem.title = @"提货券";
    else
        self.navigationItem.title = @"我的付款码";

    [self toCode];
    if (self.isConnect) {
        mAlertView(@"提示", @"请检查网络设置");
        return;
    }
    ZXMultiFormatWriter* writer = [ZXMultiFormatWriter writer];
    ZXBitMatrix* result = [writer encode:self.uuid format:kBarcodeFormatCode128 width:200 height:120 error:&error];
    if (error)
    {
        NSLog("生成条形码失败");
        return;
    }
    CGImageRef imageref = [[ZXImage imageWithMatrix:result] cgimage ];
    UIImage* image = [UIImage imageWithCGImage:imageref scale:0.7 orientation:UIImageOrientationUp];
    m_pImageView = [[[UIImageView alloc] initWithImage:image] autorelease];
    m_pImageView.backgroundColor = [UIColor whiteColor];
    CGRect frame = m_pImageView.frame;
    m_pImageView.frame = CGRectMake(15, 80, frame.size.width, frame.size.height/2);
    [self.view addSubview:m_pImageView];
    UILabel* label = [[[UILabel alloc] initWithFrame:CGRectMake(25, 80+frame.size.height/2, 300, 49)] autorelease];
    label.text = self.uuid;
    [self.view addSubview:label];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
}

#pragma mark - 网络模块

-(void) toCode {
    NSError* error=nil;
    if(!self.uuid){
        NSString* httpstring = @"https://rs88881234.com/chinesejie/phones/SEND_PHONE_FOR_SCAN";
        NSMutableString* urlString = [NSMutableString stringWithString:httpstring];
        NSURL* url = [NSURL URLWithString:urlString];
        self.uuid = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
        if (error)
        {
            NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
            NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
            JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
            NSString *reqType = mLoginApp;
            NSDictionary *dict1 = @{@"phone" : phoneNum,
                                    @"password" : passwordOld};
            [params addPostParams:dict1];
            [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                [[DAXUtils sharedInstance] hideProgressHud];
                [self toCode];
            }];
            self.isConnect = YES;
            
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [super dealloc];
}


////////////////////////////////////////////////////////////////////////////////
@end
