//
//  CategoryViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/11.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CategoryViewController.h"
#import "CategoryCollectionViewCell.h"
#import "ProductInfo.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "ListViewController.h"

@interface CategoryViewController ()
@property (nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic, retain) NSMutableArray *advertArray;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL isLoadAdvertDone;

- (void)fetchAdverts;
- (void)fetchAdvertsFinished:(JRFResultCode)code result:(id)adverts;
@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"品类";
    self.advertArray = [NSMutableArray array];
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake( 0,self.navigationController.navigationBar.frame.size.height+22,[[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height-115)  collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.automaticallyAdjustsScrollViewInsets = false;
    //self.collectionView.bounces = NO;
    //设置代理
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    // self.collectionView.backgroundColor = [UIColor whiteColor];
    //注册cell和ReusableView（相当于头部）
    [self.collectionView registerClass:[CategoryCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.isReloadPage = NO;
    if (!self.isLoadAdvertDone) {
        [self fetchAdverts];
    }
    NSArray *list=self.navigationController.navigationBar.subviews;
    
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    //self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:normalImage selectedImage:selectedImage];
    // Dispose of any resources that can be recreated.
}

- (void)fetchAdverts {
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    __block CategoryViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mCategory param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultCode == JRF_RESULT_OK) {
            [myself fetchAdvertsFinished:resultCode result:resultObj];
        } else {
            mAlertView(@"提示", resultObj);
        }
        
    }];
}

- (void)fetchAdvertsFinished:(JRFResultCode)code result:(id)adverts {
    self.isLoadAdvertDone = YES;
    [self.advertArray removeAllObjects];
    [self.advertArray addObjectsFromArray:adverts];
    [self.collectionView reloadData];
}




#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.advertArray count];
}
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"cell";
    CategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    
    if (!cell) {
        NSLog(@"无法创建CollectionViewCell时打印，自定义的cell就不可能进来了。");
    }
    ProductInfo *product = [self.advertArray objectAtIndex:indexPath.row];
    NSString *picUrl = mSetimage;
    picUrl = [picUrl  stringByAppendingFormat:@"%@%@", product.imageId, @"?p=0"];
    [cell.imgView setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
    cell.text.text = product.mDescription;
    [cell sizeToFit];
    return cell;
}



#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //边距占5*4=20 ，2个
    //图片为正方形，边长：(fDeviceWidth-20)/2-5-5 所以总高(fDeviceWidth-20)/2-5-5 +20+30+5+5 label高20 btn高30 边
    return CGSizeMake(100,110);
}
//定义每个UICollectionView 的间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 5, 5, 5);
}
//定义每个UICollectionView 纵向的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    ProductInfo *product = [self.advertArray objectAtIndex:indexPath.row];
    ListViewController *pdvc = [[ListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    pdvc.cId = product.mCategoryId;
    pdvc.mTitle = product.mDescription;
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}



@end
