//
//  CouponTableViewCell.m
//  Dax
//
//  Created by Jay_Ch on 15/5/7.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CouponTableViewCell.h"

@implementation CouponTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_mMoney release];
    [_mCode release];
    [_mImage release];
    [super dealloc];
}
@end
