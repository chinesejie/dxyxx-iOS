//
//  DXDataParser.m
//  DAX
//
//  Created by fengjia on 14-5-22.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import "DXDataParser.h"
#import "ProductInfo.h"
#import "DetailInfo.h"
#import "UserInfo.h"

@implementation DXDataParser

+ (id)parserData:(id)jsonValue reqType:(NSString *)reqType resultCode:(int *)resultCode {

    if ([reqType isEqualToString:mTopbanner]||[reqType isEqualToString:mHomepagebanner]||[reqType isEqualToString:mCategory]) {
        NSDictionary *dict = jsonValue;
        if (!dict) {
            return nil;
        }
        NSLog(@"%@", dict);
        NSMutableArray *taskArray = [NSMutableArray array];
        for (NSDictionary *tempDict in dict) {
            ProductInfo *productInfo = [[ProductInfo alloc] initWithDictionary:tempDict];
            [taskArray addObject:productInfo];
            [productInfo release];
        }
        return taskArray;
        
    }else if ([reqType isEqualToString:mListbycid]) {
        NSDictionary *dict = jsonValue;
        if (!dict) {
            return nil;
        }
        NSLog(@"%@", dict);
        NSMutableArray *taskArray = [NSMutableArray array];
        for (NSDictionary *tempDict in dict) {
            DetailInfo *detailInfo = [[DetailInfo alloc] initWithDictionary:tempDict];
            [taskArray addObject:detailInfo];
            [detailInfo release];
        }
        return taskArray;
        
    }
       return jsonValue;
}

@end
