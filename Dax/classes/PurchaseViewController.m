//
//  PurchaseViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/16.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "PurchaseViewController.h"
#import "ProductDetailViewController.h"
#import "FJPullTableView.h"
#import "TopPurhcaseCell.h"
#import "MiddlePurhcaseCell.h"
#import "PurchaseCollectionViewCell.h"
#import "TabBarViewController.h"
#import "NavigationController_1.h"
#import "CustomBadge.h"
#import "CartViewController.h"
#import <QuartzCore/CoreAnimation.h>
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "EditViewController.h"
#import "ProductCommentViewController.h"
#import <AGCommon/UINavigationBar+Common.h>
@interface PurchaseViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate,UIScrollViewDelegate,ISSShareViewDelegate>
@property (nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (nonatomic, retain) NSMutableArray *collectiondata;
@property (nonatomic, retain) NSIndexPath *indexPath;
@property (retain, nonatomic) UIButton *mCart;
@property (retain, nonatomic) UIButton *mAddtocart;
@property (retain, nonatomic) UIView *mBottom;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL isRepeat;
@property int offset;
@property int count;

@end

@implementation PurchaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datasource = [NSMutableArray array];
    self.collectiondata = [NSMutableArray array];
    [self addPullTableView];
    self.automaticallyAdjustsScrollViewInsets = false;
    [self bottombar];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)dealloc {
    [_fav release];
    [_toolbar release];
    [super dealloc];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = TRUE;
    [self fetchTaskLists];
    [self initializeDefaultDataList];
    //设置badge
    if ([self.datasource count]!= 0) {
        NSString *temp1 = [NSString stringWithFormat:@"%lu",(unsigned long)[self.datasource count]];
        CustomBadge *badge = [CustomBadge customBadgeWithString:temp1];
        [badge setFrame:CGRectMake(24,0, 20,20)];
        CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [animation setFromValue:[NSNumber numberWithFloat:1.5]];
        [animation setToValue:[NSNumber numberWithFloat:1]];
        [animation setDuration:0.2];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithControlPoints:.4 :1.3 :1 :1]];
        [badge.layer addAnimation:animation forKey:@"bounceAnimation"];
        [self.mCart addSubview:badge];
    }
    
    [self.mBottom addSubview:self.mCart];
    [self.mBottom addSubview:self.mAddtocart];
    
    
    //[self.view addSubview:navigationController];
}

- (void)fetchTaskLists{
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"categoryId": self.cId,
                           @"current": @1,
                           @"count": @5,
                           @"order":@"hot"};
    [params addGetParams:dict];
    __block PurchaseViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mListbycid param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultCode == JRF_RESULT_OK) {
            [myself fetchTaskListsFinished:resultCode result:resultObj];
        } else {
             [WFHudView showMsg:resultObj inView:nil];
        }
    }];
}
- (void)fetchTaskListsFinished:(JRFResultCode)code result:(NSMutableArray *)tasks {
    NSLog(@"%@", tasks);
    if (!tasks) {
        return;
    }
    
    [self.collectiondata removeAllObjects];
    [self.collectiondata addObjectsFromArray:tasks];
}

//buttom bar
-(void)bottombar {
    //底栏
    CGRect rect = CGRectMake(0,[[UIScreen mainScreen] bounds].size.height,[[UIScreen mainScreen] bounds].size.width,44);
    self.mBottom = [[UIView alloc]initWithFrame:rect];
    //view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview: self.mBottom ];
    self.mBottom .layer.borderWidth = 0.5f;
    self.mBottom .layer.borderColor = [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor;
    
    self.mBottom.backgroundColor = [UIColor whiteColor];
    
    CGRect frame_1= CGRectMake(16,0, self.mBottom .frame.size.height, self.mBottom.frame.size.height);
    self.mCart = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.mCart  setFrame:frame_1];
    [self.mCart  setImage:[UIImage imageNamed:@"cart_active.png"] forState:UIControlStateNormal];
    [self.mCart  addTarget:self action:@selector(toCart:) forControlEvents:UIControlEventTouchUpInside];
    
    
    CGRect frame_2= CGRectMake(self.mBottom.frame.size.width-120,0, 120,  self.mBottom.frame.size.height);
    self.mAddtocart = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.mAddtocart setFrame:frame_2];
    [self.mAddtocart setTitle:@"加入购物车" forState:UIControlStateNormal];
    [self.mAddtocart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.mAddtocart.titleLabel.font=[UIFont systemFontOfSize:19];
    [self.mAddtocart addTarget:self action:@selector(addTocart:) forControlEvents:UIControlEventTouchUpInside];
    self.mAddtocart.backgroundColor = [UIColor orangeColor];
    //[btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    
    
    
    
    
    
    
    //底部滑出
    [UIView animateWithDuration:0.4f
                          delay:0.6
                        options:UIViewAnimationCurveEaseInOut
                     animations:^{
                         [self.view addSubview:self.mBottom];
                         self.mBottom.center = CGPointMake(self.mBottom.center.x, [[UIScreen mainScreen] bounds].size.height-22);
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}


- (void)addPullTableView {
    
    
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,0,  self.view.frame.size.width, self.view.frame.size.height-45)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
    // self.pullTableView.backgroundColor = [UIColor clearColor];
}

#pragma mark - UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 1;
            break;
        case 1 :
            return 3;
            break;
        default:
            return 1;
            break;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 2) {
        return 35.0f;
        
    }
    else
        
        return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 94+[[UIScreen mainScreen] bounds].size.width;
    }
    else if(indexPath.section == 1){
        return 60.0f;
    }
    else{
        return 210.0f;
    }
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section

{
    return @"购买该商品的果友也购买了";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return  nil;
    }
    
    UILabel * label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(10, 3, 320, 28);
    
    label.font=[UIFont fontWithName:@"Arial-BoldItalicMT" size:15];
    label.text = sectionTitle;
    label.textColor = [UIColor darkGrayColor];
    UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35.0f)] autorelease];
    [sectionView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [sectionView addSubview:label];
    return sectionView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *identifyCell1 = @"TopCell";
        
        TopPurhcaseCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopPurhcaseCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopPurhcaseCell"
                                                                     owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.price = [NSString stringWithFormat:@"%@", @([self.mProuduct.price intValue])];;
        cell.imageIds = self.mProuduct.imageIds;
        cell.mDescription = self.mProuduct.mDescription;
        [cell configTopCell];
        self.indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        return cell;
    }
    else if (indexPath.section == 1){
        static NSString *identifyCell2 = @"MiddleCell";
        
        MiddlePurhcaseCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell2];
        if (!cell) {
            cell = (MiddlePurhcaseCell *)[[[NSBundle mainBundle] loadNibNamed:@"MiddlePurhcaseCell" owner:self options:nil] objectAtIndex:0];
            //cell.lbValue.hidden = YES;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (indexPath.row == 0) {
            cell.title.text = self.mProuduct.mSpecification;
            cell.title.textColor = [UIColor darkGrayColor];
        }
        else if (indexPath.row == 1){
            cell.title.text = @"评论晒单";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else {
            cell.title.text = @"商品详情";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        return cell;
    }
    else{
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake( 0,0,[[UIScreen mainScreen] bounds].size.width,210)   collectionViewLayout:flowLayout];
        self.collectionView.backgroundColor = [UIColor whiteColor];
        self.automaticallyAdjustsScrollViewInsets = false;
        //self.collectionView.bounces = NO;
        
        //设置代理
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.showsHorizontalScrollIndicator = FALSE;
        
        static NSString *CellIdentifier = @"CellIdentifier";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell.contentView addSubview:self.collectionView];
        return cell;
    }
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //获的当前选择项
    
    if (indexPath.section == 1 && indexPath.row == 2){
        ProductDetailViewController *pdvc = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
        //int count = [self.datasource count]-indexPath.row-1;
        pdvc.productId = [NSString stringWithFormat:@"%@", @([self.mProuduct.productId intValue])];;
        [self presentViewController:pdvc animated:YES completion:nil];
        //[self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
        
    }
    if (indexPath.section == 1 && indexPath.row == 1){
        ProductCommentViewController *pdvc = [[ProductCommentViewController alloc] initWithNibName:@"ProductCommentViewController" bundle:nil];
        //int count = [self.datasource count]-indexPath.row-1;
        pdvc.productId = [NSString stringWithFormat:@"%@", @([self.mProuduct.productId intValue])];;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
      [self.navigationController pushViewController:pdvc animated:YES];
        //[self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
        
    }
    
    
}



#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.collectiondata count];
}

//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Register nib file for the cell
    UINib *nib = [UINib nibWithNibName:@"PurchaseCollectionViewCell"
                                bundle: [NSBundle mainBundle]];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"PurchaseCollectionViewCell"];
    PurchaseCollectionViewCell *cell = [[PurchaseCollectionViewCell alloc]init];
    
    // Set up the reuse identifier
    cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"PurchaseCollectionViewCell"
                                                     forIndexPath:indexPath];
    
    DetailInfo *detail = [self.collectiondata objectAtIndex:indexPath.row];
    NSString *picUrl = mSetimage;
    picUrl = [picUrl  stringByAppendingFormat:@"%@", detail.thumbId];
    [cell.img setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
    cell.title.text = detail.mDescription;
    cell.detail.text = detail.mSpecification;
    cell.price.text = [NSString stringWithFormat:@"%@", @([detail.price floatValue])];;
    return cell;
}



#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //边距占5*4=20 ，2个
    //图片为正方形，边长：(fDeviceWidth-20)/2-5-5 所以总高(fDeviceWidth-20)/2-5-5 +20+30+5+5 label高20 btn高30 边
    return CGSizeMake(100,180);
}
//定义每个UICollectionView 的间距
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,10,5, 5);
}

//定义每个UICollectionView 纵向的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PurchaseViewController *pdvc = [[PurchaseViewController alloc] initWithNibName:@"PurchaseViewController" bundle:nil];
    pdvc.mProuduct = (DetailInfo *)[self.collectiondata objectAtIndex:indexPath.row];
    pdvc.cId = self.cId;
    pdvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:pdvc animated:YES];
    
    //[self presentViewController:pdvc animated:YES completion:nil];
    // [UIView transitionFromView:self.view toView:pdvc.view duration:0.1f options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
    [pdvc release];
    
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


#pragma mark -  NSMutableArray、NSData互转
//NSMutableArray转成NSData
-(void)addInfoWithName{
    //save data
    NSData *cartInfo = [NSKeyedArchiver archivedDataWithRootObject:self.datasource ];
    [[NSUserDefaults standardUserDefaults] setObject:cartInfo forKey:@"cartInfo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// NSData转成NSMutableArray
-(void)initializeDefaultDataList{
    NSData *savedEncodedData = [[NSUserDefaults standardUserDefaults] objectForKey:@"cartInfo"];
    if(savedEncodedData == nil)
    {
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        self.datasource = temp;
    }
    else{
        self.datasource = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:savedEncodedData];
        //appDelegate().datasource = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:savedEncodedData];
        NSLog(@"datasource的信息！！%@",self.datasource);
    }
}




#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    
    self.isRefresh = YES;
    self.offset = 0;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
}
- (void)beginToLoadMoreData:(FJPullTableView *)tableView {
    self.isRefresh = NO;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
}


#pragma mark - 按钮功能

//加入购物车功能
-(void) addTocart:(id)sender {
    //获取购买数量&&同步数据
    TopPurhcaseCell *cell = (TopPurhcaseCell *)[self.pullTableView cellForRowAtIndexPath:self.indexPath ];
    self.count = cell.stepper.value;
    NSLog(@"%d",self.count);
    self.mProuduct.amount =  [NSNumber numberWithInteger:self.count];
    if ([self.datasource count]==0) {
        self.mProuduct.cId = self.cId;
        [self.mProuduct setValue:self.mProuduct.amount forKey:@"amount"];
        [self.datasource insertObject:self.mProuduct atIndex:0];
        [self addInfoWithName];
    }
    else{
        for (int i=0; i< [self.datasource count]; i++) {
            DetailInfo *temp = (DetailInfo *)[self.datasource objectAtIndex:i];
            if ([temp.productId intValue]== [self.mProuduct.productId intValue] ) {
                int count = [temp.amount floatValue] + [self.mProuduct.amount intValue];
                temp.amount =  [NSNumber numberWithInteger:count];
                temp.cId = self.cId;
                [temp setValue: temp.amount forKey:@"amount"];
                [self.datasource replaceObjectAtIndex:i withObject:temp];
                self.isRepeat = TRUE;
            }
        }
        if (!self.isRepeat) {
            [self.mProuduct setValue:self.mProuduct.amount forKey:@"amount"];
            self.mProuduct.cId = self.cId;
            [self.datasource insertObject:self.mProuduct atIndex:0];
        }
        [self addInfoWithName];
    }
    
    NSLog(@"datasource的信息 %lu",(unsigned long)[self.datasource count]);
    DetailInfo *t1 = (DetailInfo *) [self.datasource objectAtIndex:0];
    NSLog(@"该物品在购物车内的数量：%@",t1.amount);
    
    //设置badge
    NSString *temp1 = [NSString stringWithFormat:@"%lu",(unsigned long)[self.datasource count]];
    CustomBadge *badge = [CustomBadge customBadgeWithString:temp1];
    [badge setFrame:CGRectMake(24,0, 20,20)];
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [animation setFromValue:[NSNumber numberWithFloat:1.5]];
    [animation setToValue:[NSNumber numberWithFloat:1]];
    [animation setDuration:0.2];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithControlPoints:.4 :1.3 :1 :1]];
    [badge.layer addAnimation:animation forKey:@"bounceAnimation"];
    [self.mCart addSubview:badge];
}

//进入购物车
-(void) toCart:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TabBarViewController *mTabBar = [storyBoard instantiateViewControllerWithIdentifier:@"idTabBar"];
    mTabBar.selectedIndex = 3;
    [self presentViewController:mTabBar animated:NO completion:nil];
    
    //     [UIView transitionFromView:self.view toView:tabbar.view duration:0.1f options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
    
    
}


- (IBAction)back:(id)sender {
    //self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)viewOnWillDisplay:(UIViewController *)viewController shareType:(ShareType)shareType
{
    [viewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"background_top_bar.png"]];
    
}

- (IBAction)share:(UIButton *)sender {
    NSArray *imageId = [self.mProuduct.imageIds componentsSeparatedByString:@";"];
    NSString *picUrl = mSetimage;
    picUrl = [picUrl  stringByAppendingFormat:@"%@%@", [imageId objectAtIndex:0], @"?p=0"];
    NSString *contentPath = @"https://rs88881234.com/chinesejie/html/wx/index.html#/wcPay?id=";
    contentPath = [contentPath stringByAppendingString:[self.mProuduct.productId stringValue]];
    NSString *content = @"好吃的";
    content = [content stringByAppendingFormat:@"%@,大家快来抢！%@",self.mProuduct.mDescription,contentPath];
    
    //构造分享内容
    
    id<ISSContent> publishContent = [ShareSDK content:content
                                       defaultContent:@"新鲜的水果"
                                                image:[ShareSDK imageWithUrl:picUrl]
                                                title:@"新鲜的水果"
                                                  url:contentPath
                                          description:content
                                            mediaType:SSPublishContentMediaTypeNews];
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES      allowCallback:NO
                                                                scopes:nil powerByHidden:YES followAccounts:nil authViewStyle:SSAuthViewStyleFullScreenPopup viewDelegate:self authManagerViewDelegate:nil];
    id<ISSShareOptions> shareOptions = [ShareSDK defaultShareOptionsWithTitle:@"内容分享"
                                                              oneKeyShareList:[NSArray defaultOneKeyShareList]
                                                               qqButtonHidden:YES
                                                        wxSessionButtonHidden:YES
                                                       wxTimelineButtonHidden:YES
                                                         showKeyboardOnAppear:NO
                                                            shareViewDelegate:self
                                                          friendsViewDelegate:self
                                                        picViewerViewDelegate:nil];
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:authOptions
                      shareOptions:shareOptions
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSString *str2 = NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功");
                                    NSLog(@"%@ ", str2);
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    
                                    NSLog( @"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
                                }
                            }];
}

- (IBAction)comment:(UIButton *)sender {
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    EditViewController *pdvc = [[EditViewController alloc] initWithNibName:@"EditViewController" bundle:nil];
    pdvc.productId = [self.mProuduct.productId stringValue] ;
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
}





@end
