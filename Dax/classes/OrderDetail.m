//
//  OrderDetail.m
//  Dax
//
//  Created by Jay_Ch on 15/3/22.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "OrderDetail.h"
#import "CommitViewController.h"
#import "FJPullTableView.h"
#import "TopConfirmViewCell.h"
#import "ListViewCell.h"
#import "ProductInfo.h"
#import "DetailInfo.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "DataSigner.h"
#import <AlipaySDK/AlipaySDK.h>
#import "APAuthV2Info.h"
#import "WXApi.h"
#import "Order.h"
#import "OrderListViewController.h"
#import "CouponListViewController.h"
#import "Star.h"
#import "WFHudView.h"
#import "TabBarViewController.h"
#import "UserInfo.h"
#import "Tools.h"
#import "ScanViewController.h"
@interface OrderDetail ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (retain, nonatomic) NSArray *itemname;
@property (retain, nonatomic) UIButton *btn;
@property (retain, nonatomic) Star *star;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property int offset;
@property int tcount;
//@property (nonatomic, retain) InboxInfo *Info;
- (void)addPullTableView;

@end

@implementation OrderDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datasource = [NSMutableArray array];
    self.tuanyuandata = [NSMutableArray array];
    //self.navigationItem.title = @"已完成";
    [self addPullTableView];
    self.automaticallyAdjustsScrollViewInsets = false;
    CGRect rect = CGRectMake(0,[[UIScreen mainScreen] bounds].size.height,[[UIScreen mainScreen] bounds].size.width,30);
    UIView *view = [[UIView alloc]initWithFrame:rect];
    view.backgroundColor = [UIColor whiteColor];
    
    self.star = [[Star alloc] initWithFrame:CGRectMake(10.0f, 5.0f, [[UIScreen mainScreen] bounds].size.width-10, 50.0f)];
    self.star.font_size = 40;
    
    [self.view addSubview:view];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [self getData];
    if(self.isFromComment )
        [self getComment];
    if (self.isFromTuan && !self.isReloadPage) {
        [self getTYuan];
    }
}

//buttom bar
-(void)bottombar {
    //底栏
    CGRect rect = CGRectMake(0,[[UIScreen mainScreen] bounds].size.height,[[UIScreen mainScreen] bounds].size.width,44);
    UIView *view = [[UIView alloc]initWithFrame:rect];
    //view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    view.layer.borderWidth = 0.5f;
    view.layer.borderColor = [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor;
    view.backgroundColor = [UIColor whiteColor];
    
    //定义label
    UILabel * label1 = [[[UILabel alloc] init] autorelease];
    label1.frame = CGRectMake(16,15,51, 21);
    label1.font=[UIFont fontWithName:@"Arial" size:17];
    label1.text = @"合计：";
    label1.textColor = [UIColor blackColor];
    
    UILabel * label2 = [[[UILabel alloc] init] autorelease];
    label2.frame = CGRectMake(60,17,12, 21);
    label2.font=[UIFont fontWithName:@"Arial" size:14];
    label2.text = @"￥";
    label2.textColor = [UIColor redColor];
    
    
    UILabel * label3 = [[[UILabel alloc] init] autorelease];
    label3.frame = CGRectMake(74,15,79, 21);
    label3.font=[UIFont fontWithName:@"Arial" size:22];
    label3.text = [NSString stringWithFormat:@"%@",self.total];
    label3.textColor = [UIColor redColor];
    
    UILabel * label4 = [[[UILabel alloc] init] autorelease];
    label4.frame = CGRectMake(112,17,79, 21);
    label4.font=[UIFont fontWithName:@"Arial" size:14];
    label4.text = @"元";
    label4.textColor = [UIColor lightGrayColor];
    
    //定制自己的风格的  UIBarButtonItem
    
    CGRect frame_1= CGRectMake(0,0, 120, view.frame.size.height);
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setFrame:frame_1];
    [btn1 setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(toCancel:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame_2= CGRectMake(view.frame.size.width-120,0, 120, view.frame.size.height);
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setFrame:frame_2];
    [btn2 setImage:[UIImage imageNamed:@"paymoney"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(toPay:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame_3= CGRectMake(view.frame.size.width-120,0, 120, view.frame.size.height);
    self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btn setFrame:frame_3];
    [self.btn setTitle:@"评论" forState:UIControlStateNormal];
    [self.btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn.titleLabel.font=[UIFont systemFontOfSize:19];
    [self.btn addTarget:self action:@selector(toComment:) forControlEvents:UIControlEventTouchUpInside];
    self.btn.backgroundColor = [UIColor orangeColor];
    
    
    CGRect frame_4= CGRectMake(view.frame.size.width-120,0, 120, view.frame.size.height);
    UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn4 setFrame:frame_4];
    [btn4 setImage:[UIImage imageNamed:@"coupon"] forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(toCode:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.flag == 2|| self.flag == 3) {
        [view addSubview:btn1];
        [view addSubview:btn2];
    }
    else{
        [view addSubview:label1];
        [view addSubview:label2];
        [view addSubview:label3];
        [view addSubview:label4];
    }
    if (self.isFromComment) {
        [view addSubview:self.btn];
    }
    if (self.isFromTuan && self.tcount == [self.tuanyuandata count]) {
        [view addSubview:btn4];
    }
    
    //底部滑出
    [UIView animateWithDuration:0.4f
                          delay:0.6
                        options:UIViewAnimationCurveEaseInOut
                     animations:^{
                         [self.view addSubview:view];
                         view.center = CGPointMake(view.center.x, [[UIScreen mainScreen] bounds].size.height-22);
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}


- (void)addPullTableView {
    if (!self.isFromTuan) {
        [self bottombar];
    }
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20,  self.view.frame.size.width,  self.view.frame.size.height-108)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
   
}

#pragma mark - UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.isFromComment)
        return 4;
    if (self.isFromTuan) {
        return 5;
    }
    else
        return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 1;
            break;
        case 1 :
            return 1;
            break;
        case 2:
            return [self.datasource count];
        case 3 :
            return 1;
        default:
            return [self.tuanyuandata count];
            break;
    }
    
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2) {
        return 116.0f;
        
    }
    if (indexPath.section == 4) {
        return 80.0f;
        
    }
    else
        return 60.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 45.0f;
    }
    else if (self.isFromTuan && (section==3||section==4))
        return 45.0f;

    else
        return 0.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
 if (section == 1) {
    UILabel * label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(15,15,79, 21);
    label.font=[UIFont fontWithName:@"Arial" size:17];
    label.text = @"自提时间";
    label.textColor = [UIColor lightGrayColor];
    UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35.0f)] autorelease];
    
    UILabel * label2 = [[[UILabel alloc] init] autorelease];
    label2.frame = CGRectMake(92,15,79, 21);
    label2.font=[UIFont fontWithName:@"Arial" size:17];
    if([[self.dict objectForKey:@"bufferInterval"] intValue] == 0)
        label2.text = @"当天";
    else  if([[self.dict objectForKey:@"bufferInterval"] intValue] == 1)
        label2.text = @"第二天";
    else
        label2.text = @"第三天";
    label2.textColor = [UIColor darkGrayColor];
    
    [sectionView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [sectionView addSubview:label];
    [sectionView addSubview:label2];
    
    return sectionView;
    }
    else if(section == 3 && self.isFromTuan){
        UILabel * label = [[[UILabel alloc] init] autorelease];
        label.frame = CGRectMake(15,15,79, 21);
        label.font=[UIFont fontWithName:@"Arial" size:17];
        label.text = @"团长";
        label.textColor = [UIColor lightGrayColor];
        UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35.0f)] autorelease];
        
        UILabel * label2 = [[[UILabel alloc] init] autorelease];
        label2.frame = CGRectMake(60,15,79, 21);
        label2.font=[UIFont fontWithName:@"Arial" size:17];
        NSString *temp = @"";
        temp = [temp  stringByAppendingFormat:@"【%@人团】", [[self.dict objectForKey:@"group"] objectForKey:@"personAmount"]];
        label2.text = temp;
        label2.textColor = [UIColor darkGrayColor];
        
        [sectionView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [sectionView addSubview:label];
        [sectionView addSubview:label2];
        return sectionView;

    }
    else{
        UILabel * label = [[[UILabel alloc] init] autorelease];
        label.frame = CGRectMake(15,15,79, 21);
        label.font=[UIFont fontWithName:@"Arial" size:17];
        label.text = @"团员";
        label.textColor = [UIColor lightGrayColor];
        UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35.0f)] autorelease];
        [sectionView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [sectionView addSubview:label];
        return sectionView;

    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 4 && self.isFromTuan) {
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        UserInfo *userInfo = [self.tuanyuandata objectAtIndex:indexPath.row];
        cell.title.hidden = true;
        UIImageView *cpimage = [[UIImageView alloc] initWithFrame:CGRectMake(15,7,46, 46)];
        NSString *picUrl = mSetimage;
        picUrl = [picUrl  stringByAppendingFormat:@"%@",userInfo.thumd] ;
        [cpimage setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
        cpimage.layer.cornerRadius = 23.0;
        cpimage.clipsToBounds = TRUE;
        
        UILabel * label2 = [[[UILabel alloc] init] autorelease];
        label2.frame = CGRectMake(67,20,79, 21);
        label2.font=[UIFont fontWithName:@"Arial" size:17];
        label2.text = userInfo.uname;
        label2.textColor = [UIColor darkGrayColor];
        
        UILabel * label3 = [[[UILabel alloc] init] autorelease];
        label3.frame = CGRectMake(17,53,320, 21);
        label3.font=[UIFont fontWithName:@"Arial" size:17];
        NSString *temp = @"";
        temp = [temp  stringByAppendingFormat:@"下单时间：%@", [self transformTodate:userInfo.createtime]];
        label3.text = temp;
        label3.textColor = [UIColor darkGrayColor];
        
        [cell addSubview:label3];
        [cell addSubview:label2];
        [cell addSubview:cpimage];
        return cell;

    }
    else if (indexPath.section == 3 && self.isFromComment) {
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.title.hidden = true;
        [cell addSubview:self.star];
        return cell;

    }
    else if (indexPath.section == 3 && self.isFromTuan) {
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.title.hidden = true;
        UIImageView *cpimage = [[UIImageView alloc] initWithFrame:CGRectMake(15,7,46, 46)];
        NSString *picUrl = mSetimage;
        NSDictionary *temp = [[self.dict objectForKey:@"groupInstance"] objectForKey:@"captain"];
        picUrl = [picUrl  stringByAppendingFormat:@"%@",[temp objectForKey:@"image"]] ;
        [cpimage setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
        cpimage.layer.cornerRadius = 23.0;
        cpimage.clipsToBounds = TRUE;
        
        UILabel * label2 = [[[UILabel alloc] init] autorelease];
        label2.frame = CGRectMake(67,20,79, 21);
        label2.font=[UIFont fontWithName:@"Arial" size:17];
        label2.text = [temp objectForKey:@"username"];
        label2.textColor = [UIColor darkGrayColor];

        [cell addSubview:label2];
        [cell addSubview:cpimage];
        return cell;
        
    }
     else if (indexPath.section == 2) {
        static NSString *identifyCell2 = @"ListCell";
        ListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell2];
        if (!cell) {
            cell = (ListViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"ListViewCell"
                                                                  owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        ProductInfo *product = [self.datasource objectAtIndex:indexPath.row];
        DetailInfo *detail = [[DetailInfo alloc] initWithDictionary:product.mProduct];
        if (self.flag == 1 || self.flag == 3) {
            product.amount = [NSNumber numberWithInteger:1];
            NSDictionary *dic = [self.dict objectForKey:@"group"];
            product.mPrice = [dic objectForKey:@"personPrice"];
        }
        NSString *picUrl = mSetimage;
        picUrl = [picUrl  stringByAppendingFormat:@"%@", detail.thumbId];
        [cell.mThumb setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
        cell.mTitle.text = detail.mDescription;
        cell.mDescription.text = detail.mSpecification;
        cell.mPrice.text = [NSString stringWithFormat:@"%@", @([product.mPrice floatValue])];
        cell.stepper.value = [detail.amount intValue];
        cell.amount.text = [NSString stringWithFormat:@"%@", @([product.amount intValue])];
        float total = [product.mPrice floatValue]*[product.amount intValue];
        cell.total.text = [NSString stringWithFormat:@"%@",@(total)];
        [cell.total setNumberOfLines:1];
        [cell.total sizeToFit];
        cell.amount.hidden = FALSE;
        cell.mySymbol.hidden = FALSE;
        cell.myEqual.hidden = FALSE;
        cell.total.hidden = FALSE;
        //cell.unit.hidden = FALSE;
        return cell;
    }
    else  if(indexPath.section ==1){
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.title.text = @"付款方式";
        if ([[self.dict objectForKey:@"payType"]  isEqualToString:@"alipay"])
            [cell.pay1 setImage:[UIImage imageNamed:@"zhifubaoactive.png"] forState:UIControlStateNormal];
        else if([[self.dict objectForKey:@"payType"] isEqualToString:@"tenpay"])
            [cell.pay1 setImage:[UIImage imageNamed:@"weixinactive.png"] forState:UIControlStateNormal];
        cell.pay1.hidden =FALSE;
        cell.pay1.enabled = FALSE;
        return cell;
    }
    else {
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.lbValue.hidden = YES;
            //cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        //[cell configHomeTableViewCell];
        //config the cell
        UILabel * label = [[[UILabel alloc] init] autorelease];
        label.frame = CGRectMake(92,18,79, 21);
        label.font=[UIFont fontWithName:@"Arial" size:20];
        label.text = @"自提";
        label.textColor = [UIColor darkGrayColor];
        [cell addSubview:label];
        return cell;
    }
    
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     CATransform3D rotation;
     rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
     rotation.m34 = 1.0/ -600;
     
     cell.layer.shadowColor = [[UIColor blackColor]CGColor];
     cell.layer.shadowOffset = CGSizeMake(10, 10);
     cell.alpha = 0;
     cell.layer.transform = rotation;
     cell.layer.anchorPoint = CGPointMake(0, 0.5);
     
     
     [UIView beginAnimations:@"rotation" context:NULL];
     [UIView setAnimationDuration:0.8];
     cell.layer.transform = CATransform3DIdentity;
     cell.alpha = 1;
     cell.layer.shadowOffset = CGSizeMake(0, 0);
     [UIView commitAnimations];
     */
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


#pragma mark   ==============时间戳转时间的方法==============
- (NSString *)transformTodate:(NSString *)createtime
{
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    
    //设置时区,这个对于时间的处理有时很重要
    //例如你在国内发布信息,用户在国外的另一个时区,你想让用户看到正确的发布时间就得注意时区设置,时间的换算.
    //例如你发布的时间为2010-01-26 17:40:50,那么在英国爱尔兰那边用户看到的时间应该是多少呢?
    //他们与我们有7个小时的时差,所以他们那还没到这个时间呢...那就是把未来的事做了
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    // 时间戳转时间的方法
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[createtime longLongValue]/1000 ];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}


#pragma mark   ==============数据提取==============
- (void) getData
{
    NSDictionary *dict = nil;
    NSMutableArray *taskArray = [NSMutableArray array];
    if (self.flag == 1 ||self.flag == 3){
        dict = [self.dict objectForKey:@"group"];
        if (!dict) {
            return;
        }
        
        ProductInfo *productInfo = [[ProductInfo alloc] initWithDictionary:dict];
        [taskArray addObject:productInfo];
        [productInfo release];
    }
    else {
        dict = [self.dict objectForKey:@"orderDetails"];
        if (!dict) {
            return;
        }
        NSLog(@"%@", dict);
        
        for (NSDictionary *tempDict in dict) {
            ProductInfo *productInfo = [[ProductInfo alloc] initWithDictionary:tempDict];
            [taskArray addObject:productInfo];
            [productInfo release];
        }
        
    }
    if (self.isFromTuan) {
        self.tcount = [[[self.dict objectForKey:@"group"] objectForKey:@"personAmount"] intValue];
    }
    [self.datasource removeAllObjects];
    [self.datasource addObjectsFromArray:taskArray];
}

#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat sectionHeaderHeight = 90.0f; //sectionHeaderHeight
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(void) orderCancel:(NSString *)url {
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"orderId":[self.dict objectForKey:@"id"]};
    [params addGetParams:dict];
    //__block ConfirmViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"处理中..."];
    [[JRFHttpClient shareInstance] fetchNetworkData:url param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                if (self.flag ==2){
                    OrderListViewController *pdvc =  [self.navigationController.viewControllers objectAtIndex:1];
                    pdvc.isLoadTaskDone = false;
                    [self.navigationController popToViewController:pdvc animated:YES];
                }
                if (self.flag ==3){
                    CouponListViewController *pdvc =  [self.navigationController.viewControllers objectAtIndex:1];
                    pdvc.isLoadTaskDone = false;
                    [self.navigationController popToViewController:pdvc animated:YES];
                }
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self orderCancel:url];
                        }
                        else {
                            NSString *msg = [resultObj objectForKey:@"info"];
                            [WFHudView showMsg:msg inView:nil];
                        }
                    }
                }];
                
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
    }];
    
    
}



-(void) getTYuan{
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    
    NSDictionary *dict = @{@"groupinstanceId":[[self.dict objectForKey:@"groupInstance"] objectForKey:@"id"]};
    [params addGetParams:dict];
    //__block ConfirmViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mGetTuanyuan param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                self.isReloadPage =  TRUE;
                 NSMutableArray *taskArray = [NSMutableArray array];
                NSDictionary *dict = [resultObj objectForKey:@"object"];
                for (NSDictionary *tempDict in dict) {
                    UserInfo *userInfo = [[UserInfo alloc] initWithDictionary:tempDict];
                    [taskArray addObject:userInfo];
                    [userInfo release];
                }
                
            [self.tuanyuandata removeAllObjects];
            [self.tuanyuandata addObjectsFromArray:taskArray];
            [self.pullTableView reloadData];
            [self bottombar];
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                mAlertView(@"提示",msg);
            }
        }
    }];
    
    
}


//获得店员星级评分
-(void) getComment {
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"uuid":[self.dict objectForKey:@"uuid"]};
    [params addGetParams:dict];
    //__block ConfirmViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"处理中..."];
    [[JRFHttpClient shareInstance] fetchNetworkData:mOrderfen param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                NSInteger number = [[[resultObj objectForKey:@"object"] objectForKey:@"score" ]integerValue];
                self.star.show_star = number*20;
                [self.btn setTitle:@"已评论" forState:UIControlStateNormal];
                self.btn.enabled = false;
                self.star.isSelect = false;
                [self.star setNeedsDisplay];
                [self.pullTableView reloadData];
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"2"]) {
                self.btn.enabled = true;
                self.star.isSelect = true;
                [self.star setNeedsDisplay];
                [self.pullTableView reloadData];
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                mAlertView(@"提示",msg);
            }
        }
    }];
    
    
}



-(void) postComment:(NSInteger) x {
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"order_id":[self.dict objectForKey:@"uuid"],
                           @"score":[NSNumber numberWithInteger:x]};
    [params addGetParams:dict];
    //__block ConfirmViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"处理中..."];
    [[JRFHttpClient shareInstance] fetchNetworkData:mOrderPingfen param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
               [self.btn setTitle:@"已评论" forState:UIControlStateNormal];
                self.btn.enabled = false;
                self.star.isSelect = false;
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self postComment:x];
                        }
                        else {
                            NSString *msg = [resultObj objectForKey:@"info"];
                            mAlertView(@"提示",msg);
                            
                        }
                    }
                }];
                
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                mAlertView(@"提示",msg);
            }
        }
    }];
    
    
}



//条形码
-(void) toCode:(id)sender {
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    ScanViewController *pdvc = [[ScanViewController alloc] initWithNibName:@"ScanViewController" bundle:nil];
    pdvc.uuid = [self.dict objectForKey:@"uuid"];
    pdvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
    
}


//取消订单
-(void) toCancel:(id)sender {
    
    if (self.flag ==2){
        [self orderCancel:mOrderCancel];
    }
    else if(self.flag == 3){
        [self orderCancel:mGorderCancel];
    }
}

//立即支付
-(void) toPay:(id)sender {
    if ([Tools transTimeSp2:[self.dict objectForKey:@"postTime"]]) {
        if ([[self.dict objectForKey:@"payType"]  isEqualToString:@"alipay"])
            [self payorder];
        else if([[self.dict objectForKey:@"payType"] isEqualToString:@"tenpay"])
            [self sendPay];
    }
    else
        [WFHudView showMsg:@"订单超时了" inView:nil];
    
    
    
}

//评论
-(void) toComment:(id)sender {
    
    NSInteger x = self.star.show_star;
    x = x/20;
    [self postComment:x];
}


#pragma mark   ==============支付宝点击订单支付行为==============
//
//选中商品调用支付宝极简支付
//
- (void) payorder
{
    
    /*
     *商户的唯一的parnter和seller。
     *签约后，支付宝会为每个商户分配一个唯一的 parnter 和 seller。
     */
    
    /*============================================================================*/
    /*=======================需要填写商户app申请的===================================*/
    /*============================================================================*/
    NSString *partner = @"2088611480899716 ";
    NSString *seller = @"dxyxx88@163.com";
    NSString *privateKey = @"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAN/RBX2K2N1VobOIeV5WQsDORIXzEW01eyk6cXHP+TIaYjL8hd7RpXYkrmPSmU7+dt3yZXTUs3nP124K5gj3jStIv+Fjpa4XuvKKJ/wfWEcDBwY74i7KgOy903t95TFxjNLkHF7wOOUazlNNh/3eeRzOlSnW5OmQkAKWqZG9C8U1AgMBAAECgYEAkUaQZMO6LjiGBA2ikew12gZJDhUPC676KXGo7zIKU04yzVuB3XaBCuWWWG77Wf3r0/ahiisB8CBLej8Sv2FHvEjohbRWPIqO4waxV57oZPtBIg1HvrNKYpTI7+OSGLUx8l24rCahionhca61xIJn0cbX6FypDgIINC6Va0NC4uUCQQDz8XTKBQhC70Jnfqnl4d9oTTRH1CqY7YOgbP7eMIDCw8XN1k5zOtgHPPFrS8zI6Hs3q7+J+kPxERbajPmCGgx7AkEA6uDoaM9P6EkQTLaI663/7aQqXg6hXaCK7EjFmFugxPa6/clDhOVYnKb1pJcIQELARwAk3ga6noy4BfqnbXn+DwJBANDHhKcqK2nuEC03sP04ldZUzTv0kAiWryLsZi2P4YPPvklu83GXmTCIri6gj0IcBukcqy/R67g0YhTmttzsomUCQC9t70CK7IXpiyMSkR+WaWHhjrSjm64+Zw9DurMDfbmIYUYySDIj5frsNBpibUYctJshylATZ8fwfpCmhvyyb70CQCnvSjbKIRAkfVieY1nM1jp/eu/1CRNOr7wxFrz634R/kozQOKYCQnn9m24QXpL2esMoeHfd5hq/nw/GcLQc9Tg=";
    /*============================================================================*/
    /*============================================================================*/
    /*============================================================================*/
    
    //partner和seller获取失败,提示
    if ([partner length] == 0 ||
        [seller length] == 0 ||
        [privateKey length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"缺少partner或者seller或者私钥。"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    Order *order = [[Order alloc] init];
    order.partner = partner;
    order.seller = seller;
    order.tradeNO = [self.dict objectForKey:@"uuid"]; //订单ID（由商家自行制定）
    order.productName = @"大象与猩猩水果"; //商品标题
    order.productDescription = @"水果"; //商品描述
    order.amount = [NSString stringWithFormat:@"%.2f",[[self.dict objectForKey:@"money"] floatValue]]; //商品价格
    order.notifyURL =  @"http://rs88881234.com:8081/pay/alipay/deal "; //回调URL
    
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showUrl = @"m.alipay.com";
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = @"dxyxx";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    NSLog(@"orderSpec = %@",orderSpec);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            if ([[resultDic objectForKey:@"resultStatus"] intValue] == 9000) {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                TabBarViewController *mTabBar = [storyBoard instantiateViewControllerWithIdentifier:@"idTabBar"];
                mTabBar.selectedIndex = 0;
                [self presentViewController:mTabBar animated:NO completion:nil];
            }
            NSLog(@"reslut = %@",resultDic);
        }];
        
    }
}



#pragma mark   ==============微信点击订单支付行为==============
- (void)sendPay
{
    //IOS5自带解析类NSJSONSerialization从response中解析出数据放到字典中
    
    
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"uuid":[self.dict objectForKey:@"uuid"]};
    [params addGetParams:dict];
    //__block ConfirmViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"处理中..."];
    [[JRFHttpClient shareInstance] fetchNetworkData:mWechatpay param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                self.weChat = [resultObj objectForKey:@"object"];
                NSString *stamp  = [self.weChat objectForKey:@"timestamp"];
                
                //调起微信支付
                PayReq* req             = [[[PayReq alloc] init]autorelease];
                req.openID              = [self.weChat objectForKey:@"appid"];
                req.partnerId           = [self.weChat objectForKey:@"partnerid"];
                req.prepayId            = [self.weChat objectForKey:@"prepayid"];
                req.nonceStr            = [self.weChat objectForKey:@"noncestr"];
                req.timeStamp           = stamp.intValue;
                //            req.package             =  @"Sign=WXPay";
                req.package             =  [self.weChat objectForKey:@"package"];
                req.sign                = [self.weChat objectForKey:@"sign"];
                [WXApi sendReq:req];
                //日志输出
                NSLog(@"appid=%@\npartid=%@\nprepayid=%@\nnoncestr=%@\ntimestamp=%ld\npackage=%@\nsign=%@",req.openID,req.partnerId,req.prepayId,req.nonceStr,(long)req.timeStamp,req.package,req.sign );
                
                
            }else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self sendPay];
                        }
                        else {
                            NSString *msg = [resultObj objectForKey:@"info"];
                            [WFHudView showMsg:msg inView:nil];
                            
                        }
                    }
                }];
                
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
    }];
    
    
    
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
