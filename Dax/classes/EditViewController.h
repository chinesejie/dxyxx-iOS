//
//  EditViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/4/20.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZAddImage.h"
@interface EditViewController : UIViewController<UITextViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (retain, nonatomic)  UITextView *backgroundTextView;
@property (retain, nonatomic)  UITextView *textEditor;
@property (retain, nonatomic) SZAddImage *addimg;
@property (retain, nonatomic) NSString* productId;
@end
