//
//  ConfirmViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/18.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "ConfirmViewController.h"
#import "CommitViewController.h"
#import "FJPullTableView.h"
#import "TopConfirmViewCell.h"
#import "ListViewCell.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "DetailInfo.h"
#import "Tojson.h"
#import "LMContainsLMComboxScrollView.h"

@interface ConfirmViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>{
    LMContainsLMComboxScrollView *bgScrollView;
    NSString *selected;
    NSMutableArray *store;
}
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (retain, nonatomic) NSArray *itemname;
@property (retain, nonatomic) NSArray *storelist;
@property (retain, nonatomic) NSString *payType;
@property (retain, nonatomic) NSString *deliveryType;
@property (retain, nonatomic) NSString *jsonText;
@property (retain, nonatomic) NSString *storeId;
@property (retain, nonatomic) NSString *bufferdate;
@property (nonatomic, retain) NSIndexPath *indexPath;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property int offset;

//@property (nonatomic, retain) InboxInfo *Info;
- (void)addPullTableView;

@end

@implementation ConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //UINavigationController *navC = [[UINavigationController alloc]initWithRootViewController:[[UIViewController alloc]init]];
    self.jsonText = [[NSString alloc] init];
    store = [[NSMutableArray alloc] init];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //title 颜色
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
    self.navigationItem.title = @"确认订单";
    [self addPullTableView];
    self.automaticallyAdjustsScrollViewInsets = false;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [self getStore];
    [self.pullTableView reloadData];
    [self bottombar];
}

//buttom bar
-(void)bottombar {
    //底栏
    CGRect rect = CGRectMake(0,[[UIScreen mainScreen] bounds].size.height,[[UIScreen mainScreen] bounds].size.width,44);
    UIView *view = [[UIView alloc]initWithFrame:rect];
    //view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    view.layer.borderWidth = 0.5f;
    view.layer.borderColor = [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor;
    view.backgroundColor = [UIColor whiteColor];
    //定义label
    UILabel * label1 = [[[UILabel alloc] init] autorelease];
    label1.frame = CGRectMake(16,15,51, 21);
    label1.font=[UIFont fontWithName:@"Arial" size:17];
    label1.text = @"合计：";
    label1.textColor = [UIColor blackColor];
    
    UILabel * label2 = [[[UILabel alloc] init] autorelease];
    label2.frame = CGRectMake(60,17,12, 21);
    label2.font=[UIFont fontWithName:@"Arial" size:14];
    label2.text = @"￥";
    label2.textColor = [UIColor redColor];
    
    float total = 0.0;
    for (int i=0; i<[self.datasource count]; i++) {
        DetailInfo *temp = (DetailInfo *)[self.datasource objectAtIndex:i];
        total += [temp.price floatValue]*[temp.amount intValue];
    }
    UILabel * label3 = [[[UILabel alloc] init] autorelease];
    label3.frame = CGRectMake(74,15,79, 21);
    label3.font=[UIFont fontWithName:@"Arial" size:22];
    label3.text = [NSString stringWithFormat:@"%@",@(total)];
    label3.textColor = [UIColor redColor];
    
    UILabel * label4 = [[[UILabel alloc] init] autorelease];
    label4.frame = CGRectMake(112,17,79, 21);
    label4.font=[UIFont fontWithName:@"Arial" size:14];
    label4.text = @"元";
    label4.textColor = [UIColor lightGrayColor];
    
    //定制自己的风格的  UIBarButtonItem
    CGRect frame_1= CGRectMake(view.frame.size.width-120,0, 120, view.frame.size.height);
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:frame_1];
    [btn setTitle:@" 提交订单" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font=[UIFont systemFontOfSize:19];
    [btn addTarget:self action:@selector(confirmOrder:) forControlEvents:UIControlEventTouchUpInside];
    btn.backgroundColor = [UIColor orangeColor];
    //[btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [view addSubview:label1];
    [view addSubview:label2];
    [view addSubview:label3];
    [view addSubview:label4];
    [view addSubview:btn];
    //底部滑出
    [UIView animateWithDuration:0.4f
                          delay:0.6
                        options:UIViewAnimationCurveEaseInOut
                     animations:^{
                         [self.view addSubview:view];
                         view.center = CGPointMake(view.center.x, [[UIScreen mainScreen] bounds].size.height-22);
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}

- (void)addPullTableView {
    
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20,  self.view.frame.size.width,  self.view.frame.size.height-108)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    //self.pullTableView.tableHeaderView.hidden = TRUE;
}

#pragma mark - UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 3;
            break;
        case 1 :
            return 1;
            break;
        default:
            return [self.datasource count];
            break;
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2) {
        return 116.0f;
        
    }
    else if(indexPath.section == 0 && indexPath.row == 2)
        return 45.0f;
    else
        return 60.0f;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2) {
        static NSString *identifyCell2 = @"ListCell";
        ListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell2];
        if (!cell) {
            cell = (ListViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"ListViewCell"
                                                                  owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        DetailInfo *detail = [self.datasource objectAtIndex:indexPath.row];
        NSString *picUrl = mSetimage;
        picUrl = [picUrl  stringByAppendingFormat:@"%@", detail.thumbId];
        [cell.mThumb setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
        cell.mTitle.text = detail.mDescription;
        cell.mDescription.text = detail.mSpecification;
        cell.mPrice.text = [NSString stringWithFormat:@"%@", @([detail.price floatValue])];
        cell.stepper.value = [detail.amount intValue];
        cell.amount.text = [NSString stringWithFormat:@"%@", @(cell.stepper.value)];
        float total = [detail.price floatValue]*[detail.amount intValue];
        cell.total.text = [NSString stringWithFormat:@"%@",@(total)];
        [cell.total setNumberOfLines:1];
        [cell.total sizeToFit];
        cell.amount.hidden = FALSE;
        cell.mySymbol.hidden = FALSE;
        cell.myEqual.hidden = FALSE;
        cell.total.hidden = FALSE;
        //cell.unit.hidden = FALSE;
        return cell;
    }
    else  if(indexPath.section ==1){
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.lbValue.hidden = YES;
            //cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        //[cell configHomeTableViewCell];
        //config the cell
        cell.title.text = @"付款方式";
        //self.payType = cell.payType;
        cell.pay3.hidden =FALSE;
        cell.pay2.hidden =FALSE;
        cell.pay1.hidden =FALSE;
        return cell;
    }
    else if(indexPath.section ==0 && indexPath.row == 1){
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.lbValue.hidden = YES;
            //cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        //[cell configHomeTableViewCell];
        //config the cell
        cell.title.text = @"自提门店";
        if (store.count > 0) {
            selected = @"";
            bgScrollView = [[LMContainsLMComboxScrollView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds ].size.width, 60)];
            bgScrollView.backgroundColor = [UIColor clearColor];
            bgScrollView.showsVerticalScrollIndicator = NO;
            bgScrollView.showsHorizontalScrollIndicator = NO;
            [cell addSubview:bgScrollView];
            //[self.pullTableView bringSubviewToFront:cell];
            LMComBoxView *comBox = [[LMComBoxView alloc]initWithFrame:CGRectMake(92, 17, 92, 30)];
            comBox.backgroundColor = [UIColor whiteColor];
            comBox.arrowImgName = @"down_dark0.png";
            comBox.titlesList = store;
            comBox.delegate = self;
            comBox.supView = self.pullTableView;
            [comBox defaultSettings];
           // comBox.tag = kDropDownListTag ;
            [bgScrollView addSubview:comBox];

        }
       
        
        return cell;
    }
    else if(indexPath.section ==0 && indexPath.row == 0){
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.lbValue.hidden = YES;
            //cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        //[cell configHomeTableViewCell];
        //config the cell
        self.deliveryType = @"bound";
        cell.express.hidden = FALSE;
        cell.ziti.hidden =FALSE;
        return cell;
        
    }
    else{
        static NSString *identifyCell1 = @"ListCell";
        TopConfirmViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell1];
        if (!cell) {
            cell = (TopConfirmViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"TopConfirmViewCell"
                                                                        owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.lbValue.hidden = YES;
            //cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        cell.title.hidden = true;
        UILabel * label = [[[UILabel alloc] init] autorelease];
        label.frame = CGRectMake(15,15,79, 21);
        
        //定义button
        label.font=[UIFont fontWithName:@"Arial" size:17];
        label.text = @"自提时间";
        label.textColor = [UIColor lightGrayColor];
        UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35.0f)] autorelease];
        
        self.date1 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.date2 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.date3 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.date1.layer.cornerRadius = 3.0;
        self.date2.layer.cornerRadius = 3.0;
        self.date3.layer.cornerRadius = 3.0;
        [self.date1 setFrame:CGRectMake(92, 13, 65, 25)];
        [self.date2 setFrame:CGRectMake(170, 13, 65, 25)];
        [self.date3 setFrame:CGRectMake(248, 13, 65, 25)];
        [self.date1 setTitle:@"当天" forState:UIControlStateNormal];
        [self.date1 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.date2 setTitle:@"第二天" forState:UIControlStateNormal];
        [self.date2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.date3 setTitle:@"第三天" forState:UIControlStateNormal];
        [self.date3 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.date1.selected = FALSE;
        self.date2.selected = FALSE;
        self.date3.selected = FALSE;
        self.date1.backgroundColor = [UIColor whiteColor];
        self.date2.backgroundColor = [UIColor whiteColor];
        self.date3.backgroundColor = [UIColor whiteColor];
        self.date1.tag = 1;
        self.date2.tag = 2;
        self.date3.tag = 3;
        [cell.contentView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [cell addSubview:label];
        [cell addSubview:self.date1];
        [cell addSubview:self.date2];
        [cell addSubview:self.date3];
        [self.date1 addTarget:self action:@selector(datechoose:) forControlEvents:UIControlEventTouchUpInside];
        [self.date2 addTarget:self action:@selector(datechoose:) forControlEvents:UIControlEventTouchUpInside];
        [self.date3 addTarget:self action:@selector(datechoose:) forControlEvents:UIControlEventTouchUpInside];
        return cell;


    }
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     CATransform3D rotation;
     rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
     rotation.m34 = 1.0/ -600;
     
     cell.layer.shadowColor = [[UIColor blackColor]CGColor];
     cell.layer.shadowOffset = CGSizeMake(10, 10);
     cell.alpha = 0;
     cell.layer.transform = rotation;
     cell.layer.anchorPoint = CGPointMake(0, 0.5);
     
     
     [UIView beginAnimations:@"rotation" context:NULL];
     [UIView setAnimationDuration:0.8];
     cell.layer.transform = CATransform3DIdentity;
     cell.alpha = 1;
     cell.layer.shadowOffset = CGSizeMake(0, 0);
     [UIView commitAnimations];
     */
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}




#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        CGFloat sectionHeaderHeight = 90.0f; //sectionHeaderHeight
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}


#pragma mark -LMComBoxViewDelegate
-(void)selectAtIndex:(int)index inCombox:(LMComBoxView *)_combox
{
    NSInteger tag = 0;
    switch (tag) {
        case 0:
        {
            
            selected = [store objectAtIndex:index];
            NSDictionary * temp = (NSDictionary *) [self.storelist objectAtIndex:index];
            self.storeId = [temp objectForKey:@"id"];
            break;
        }
    }
}

#pragma mark - 按钮功能
- (void)datechoose:(id)sender {
    UIButton *temp = (UIButton *) sender;
    
    temp.selected = TRUE;
    if (temp.tag == 2) {
        self.date1.selected = FALSE;
        self.date3.selected = FALSE;
        self.date1.backgroundColor = [UIColor whiteColor];
        self.date3.backgroundColor = [UIColor whiteColor];
        self.date2.backgroundColor = [UIColor orangeColor];
        [self.date1 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.date3 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.date2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.bufferdate = @"1";
        NSLog(@"self.date2");
    }
    else if (temp.tag == 3){
        self.date1.selected = FALSE;
        self.date2.selected = FALSE;
        self.date1.backgroundColor = [UIColor whiteColor];
        self.date2.backgroundColor = [UIColor whiteColor];
        self.date3.backgroundColor = [UIColor orangeColor];
        [self.date1 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.date2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.date3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.bufferdate = @"2";
    }
    else {
        self.date2.selected = FALSE;
        self.date3.selected = FALSE;
        self.date2.backgroundColor = [UIColor whiteColor];
        self.date3.backgroundColor = [UIColor whiteColor];
        self.date1.backgroundColor = [UIColor orangeColor];
        [self.date2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.date3 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.date1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.bufferdate = @"0";
    }
    
}

- (void) confirmOrder :(id) sender{
    //     NSError *error = nil;
    NSString *tempText =[[NSString alloc] init];
    self.jsonText = @"[";
    for (int i=0; i<[self.datasource count]; i++) {
        DetailInfo *temp = (DetailInfo *)[self.datasource objectAtIndex:i];
        NSData *jsonData = [Tojson getJSON:temp options:NSJSONWritingPrettyPrinted error:nil];
        tempText = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if(i ==([self.datasource count]-1))
            self.jsonText = [self.jsonText stringByAppendingFormat:@"%@",tempText];
        else
            self.jsonText = [self.jsonText stringByAppendingFormat:@"%@,",tempText];
    }
    self.jsonText = [self.jsonText stringByAppendingFormat:@"]"];
    NSLog(@"%@", self.jsonText);
    
    self.indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    TopConfirmViewCell *cell = (TopConfirmViewCell *)[self.pullTableView cellForRowAtIndexPath:self.indexPath ];
    self.payType = cell.payType;
    
    [self sendOrder];
}

#pragma mark - 网络模块

-(void) sendOrder {
    //网络数据获取解析
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    if(self.storeId == nil){
        mAlertView(@"提示", @"请选择自提门店");
        return;
    }
    else if(self.bufferdate == nil){
        mAlertView(@"提示", @"请选择自提时间");
        return;
    }else if (self.flag == 1) {
        NSDictionary *dict = @{@"payType": self.payType,
                               @"deliveryType":self.deliveryType,
                               @"bufferInterval":self.bufferdate,
                               @"storeId":self.storeId,
                               @"groupId":self.groupId};
        [params addGetParams:dict];
        self.mUrl = mGroupadd;
    }
    else if (self.flag == 2) {
        NSDictionary *dict = @{@"payType": self.payType,
                               @"deliveryType":self.deliveryType,
                               @"bufferInterval":self.bufferdate,
                               @"storeId":self.storeId,
                               @"grouInstanceId":self.groupId};
        [params addGetParams:dict];
        self.mUrl = mGroupInstanceadd;
    }
    else {
        NSDictionary *dict = @{@"payType": self.payType,
                               @"deliveryType":self.deliveryType,
                               @"bufferInterval":self.bufferdate,
                               @"storeId":self.storeId,
                               @"orderDetails":self.jsonText};
        [params addGetParams:dict];
        self.mUrl = mOrderadd;
    }
    //__block ConfirmViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:self.mUrl param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                self.datasource = [NSMutableArray array];
                [self addInfoWithName];
                CommitViewController *pdvc = [[CommitViewController alloc] initWithNibName:@"CommitViewController" bundle:nil];
                pdvc.dict = [resultObj objectForKey:@"object"];
                pdvc.flag = self.flag;
                //int count = [self.datasource count]-indexPath.row-1;
                [self.navigationController pushViewController:pdvc animated:YES];
                [pdvc release];
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self sendOrder];
                        }
                        else {
                            NSString *msg = [resultObj objectForKey:@"info"];
                            [WFHudView showMsg:msg inView:nil];
                            
                        }
                    }
                }];
                
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
        
    }];
    
}

#pragma mark - 网络模块

-(void) getStore {
    //网络数据获取解析
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mStoreList param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultCode == JRF_RESULT_OK) {
            self.storelist = [[NSArray alloc] init];
            self.storelist = resultObj;
            //self.storeId = [[self.storelist objectAtIndex:0] objectForKey:@"id"];
            for (int i = 0; i < self.storelist.count; i++) {
                NSDictionary * temp = (NSDictionary *) [self.storelist objectAtIndex:i];
                NSString *storename = [temp objectForKey:@"name"];
                [store addObject:storename];
                [self.pullTableView reloadData];
            }
        }
        
        else   {
            [WFHudView showMsg:@"店铺获取失败" inView:nil];
        }
        
        
    }];
    
}

#pragma mark -  NSMutableArray、NSData互转
//NSMutableArray转成NSData

-(void)addInfoWithName{
    //save data
    NSData *cartInfo = [NSKeyedArchiver archivedDataWithRootObject:self.datasource ];
    [[NSUserDefaults standardUserDefaults] setObject:cartInfo forKey:@"cartInfo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
