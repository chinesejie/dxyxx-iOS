//
//  TopConfirmViewCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/20.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "TopConfirmViewCell.h"

@implementation TopConfirmViewCell

- (void)awakeFromNib {
    // Initialization code
    self.ziti.layer.cornerRadius = 4.0;
    self.express.layer.cornerRadius = 4.0;
        self.ziti.backgroundColor = [UIColor orangeColor];
    self.express.backgroundColor = [UIColor lightGrayColor];
   // [self.express addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    [self.ziti addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    [self.pay1 addTarget:self action:@selector(paychoose:) forControlEvents:UIControlEventTouchUpInside];
    [self.pay2 addTarget:self action:@selector(paychoose:) forControlEvents:UIControlEventTouchUpInside];
    self.payType = @"alipay";
    self.pay1.tag = 1;
    self.pay2.tag = 2;
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)choose:(id)sender {
    UIButton *temp = (UIButton *) sender;
    temp.selected = TRUE;
    //NSString *str1 = @"快递";
    /*
    if ([temp.titleLabel.text isEqualToString:str1]) {
        self.ziti.selected = TRUE;
        self.ziti.backgroundColor = [UIColor lightGrayColor];
        self.express.backgroundColor = [UIColor orangeColor];
        NSLog(@"快递");
    }
    else {
        self.express.selected = FALSE;
        self.express.backgroundColor = [UIColor lightGrayColor];
         self.ziti.backgroundColor = [UIColor orangeColor];
        NSLog(@"快递");
    }
*/
    
}

- (void)paychoose:(id)sender {
    UIButton *temp = (UIButton *) sender;
    
    temp.selected = TRUE;
    if (temp.tag == 2) {
        self.pay1.selected = FALSE;
        //self.pay3.selected = FALSE;
        [self.pay2 setImage:[UIImage imageNamed:@"zhifubaoactive.png"] forState:UIControlStateSelected];
        [self.pay1 setImage:[UIImage imageNamed:@"weixindeactive.png"] forState:UIControlStateNormal];
        self.payType = @"alipay";
        NSLog(@"pay2");
    }
    else {
        //self.pay3.selected = FALSE;
        self.pay2.selected = FALSE;
        [self.pay1 setImage:[UIImage imageNamed:@"weixinactive.png"] forState:UIControlStateSelected];
        [self.pay2 setImage:[UIImage imageNamed:@"zhifubaodeactive.png"] forState:UIControlStateNormal];
        self.payType = @"tenpay";
        NSLog(@"pay1");
    }
}
// 自绘分割线
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor);
    CGContextStrokeRect(context, CGRectMake(0,rect.size.height, rect.size.width,0.1));
}
- (void)dealloc {
    [_express release];
    [_ziti release];
    [_title release];
    [_pay1 release];
    [_pay2 release];
    [_pay3 release];
    [super dealloc];
}
@end
