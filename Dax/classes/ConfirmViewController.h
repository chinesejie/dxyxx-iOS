//
//  ConfirmViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/3/19.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMComBoxView.h"

@interface ConfirmViewController : UIViewController<LMComBoxViewDelegate>
@property (retain, nonatomic)  UIButton *date1;
@property (retain, nonatomic)  UIButton *date2;
@property (retain, nonatomic)  UIButton *date3;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (nonatomic, copy) NSString *mUrl;
@property int flag;
@property (nonatomic, copy) NSString *groupId;
@end
