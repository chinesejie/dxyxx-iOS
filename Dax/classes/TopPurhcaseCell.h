//
//  TopPurhcaseCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/17.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKYStepper.h"
@interface TopPurhcaseCell : UITableViewCell
@property (retain, nonatomic)  UIScrollView *advertScrollView;
@property (retain, nonatomic) IBOutlet UIPageControl *pageCtrl;
@property (nonatomic, retain) NSTimer *myTimer;
@property (retain, nonatomic) IBOutlet PKYStepper *stepper;
@property (retain, nonatomic) IBOutlet UILabel *title;
@property (retain, nonatomic) IBOutlet UILabel *mPrice;
@property (retain, nonatomic) NSString *imageIds;
@property (retain, nonatomic) NSArray *imageId;
@property (retain, nonatomic) NSString *price;
@property (retain, nonatomic) NSString *mDescription;
@property int curPage;
- (void)configTopCell;
@end
