//
//  OrderListCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/22.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderListCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *orderNo;
@property (retain, nonatomic) IBOutlet UIView *borderview;
@property (retain, nonatomic) IBOutlet UILabel *time;
@property (retain, nonatomic) IBOutlet UILabel *date;
@property (retain, nonatomic) IBOutlet UILabel *money;
@property (retain, nonatomic) IBOutlet UIButton *checkOrder;
@property (retain, nonatomic) IBOutlet UIButton *takeGoods;
@property (retain, nonatomic) IBOutlet UIButton *payMoney;
@property (retain, nonatomic) IBOutlet UIButton *mComment;
@property (retain, nonatomic) IBOutlet UILabel *orderName;

@end
