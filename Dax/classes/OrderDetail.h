//
//  OrderDetail.h
//  Dax
//
//  Created by Jay_Ch on 15/3/22.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetail : UIViewController
@property (nonatomic, retain) NSMutableArray *datasource;
@property (nonatomic, retain) NSMutableArray *tuanyuandata;
@property (nonatomic, retain) NSDictionary *dict;
@property (nonatomic, retain) NSDictionary *weChat;
@property int flag;
@property BOOL isFromComment;
@property BOOL isFromTuan;
@property (nonatomic, retain) NSString *total;
@end
