//
//  EditViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/4/20.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "EditViewController.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "UploadImage.h"
#import "LoginViewController.h"
#import "CommentViewController.h"
@interface EditViewController ()<UITextViewDelegate>
@property (nonatomic, strong) NSMutableArray *md5s;
@end

@implementation EditViewController

- (void)viewDidLoad

{
    
    self.md5s=[NSMutableArray array];
    //输入框显示区域
    self.backgroundTextView = [[UITextView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20,  [[UIScreen mainScreen] bounds].size.width,  100)];
    self.backgroundTextView.delegate = self;
    self.backgroundTextView.font = [UIFont systemFontOfSize:16];
    self.backgroundTextView.text = @"尽情吐槽吧...";
    self.backgroundTextView.textColor = [UIColor lightGrayColor];
    
    
    self.textEditor = [[UITextView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20,  [[UIScreen mainScreen] bounds].size.width,  150)];
    self.textEditor.delegate = self;
    self.textEditor.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.textEditor.keyboardType = UIKeyboardTypeDefault;
    self.textEditor.returnKeyType = UIReturnKeyDone;
    self.textEditor.font = [UIFont systemFontOfSize:16];
    self.textEditor.backgroundColor = [UIColor clearColor];
    //[self.textEditor becomeFirstResponder];
    
    self.addimg = [[SZAddImage alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+170, [[UIScreen mainScreen] bounds].size.width, 200)];
    self.addimg.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.addimg];
    [self.view addSubview:self.backgroundTextView];
    [self.view addSubview:self.textEditor];
    NSLog(@"%f",self.addimg.frame.size.width);
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    self.navigationItem.title = @"评论";
    UIBarButtonItem *submit= [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleDone target:self action:@selector(toSubmit:)];
    self.navigationItem.rightBarButtonItem = submit;
    
}




//通过判断表层TextView的内容来实现底层TextView的显示于隐藏
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if(![text isEqualToString:@""])
    {
        [_backgroundTextView setHidden:YES];
    }
    if([text isEqualToString:@""]&&range.length==1&&range.location==0){
        [_backgroundTextView setHidden:NO];
    }
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    else if (range.location >= 200){
        //[WFHudView showMsg:@"超过字数限制啦" inView:nil];
        //[self.superview makeToast:@"超过字数限制啦" duration:0.5f position:@"center-38"];
        return  NO;
    }
    else if (range.location + text.length >200)
        return  YES;
    return YES;
}


//点击done 按钮 去掉键盘
- (BOOL)textViewShouldReturn:(UITextView *)textField {
    [textField resignFirstResponder];
    return YES;
}
//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textEditor resignFirstResponder];
}

-(void) toSubmit:(id) sender{
    [self.textEditor resignFirstResponder];
    NSLog(@"%lu",(unsigned long)self.addimg.images.count);
    if(appDelegate().isLoginApp){
        if (self.textEditor != nil && self.textEditor.text.length >200) {
            [WFHudView showMsg:@"超过字数限制啦" inView:nil];
            return;
        }
        if (self.addimg.images.count > 0 ) {
            [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在发布"];
            [UploadImage uploadImagesWithImages:self.addimg.images success:^(NSArray  *md5s) {
                [self.md5s addObjectsFromArray:md5s];
                [self toSendmsg];
            } fail:^{
                [[DAXUtils sharedInstance] hideProgressHud];
                mAlertView(@"提示", @"上传失败，请重新选择");
            }];
        }
        else{
            mAlertView(@"提示", @"至少选择一张图片");
        }
        
    }
    else{
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    
}


//发送评论
-(void) toSendmsg{
    
    NSString *images = @"";
    for (int i=0; i<self.md5s.count-1; i++) {
        images  = [images  stringByAppendingFormat:@"%@;", self.md5s[i]];
    }
    images  = [images  stringByAppendingFormat:@"%@", self.md5s[self.md5s.count-1]];
    NSDictionary *dict = [[NSDictionary alloc] init];
    if(self.textEditor == nil)
        self.textEditor.text = @"";
    if (self.productId != nil) {
        dict = @{@"word":self.textEditor.text,
                 @"images":images,
                 @"product_id":self.productId};
    }else
        dict = @{@"word":self.textEditor.text,
                 @"images":images};
//    NSDictionary *dict = @{@"word":self.textEditor.text};

    //__block OrderListViewController *myself = self;
    
    
    //获取网络数据
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    //异步访问网络，回调是主线程，更新UI
    [manager POST:mAddComment parameters:dict success:^(AFHTTPRequestOperation *operation, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                if (self.productId == nil) {
                    CommentViewController *pdvc =  [self.navigationController.viewControllers objectAtIndex:0];
                    pdvc.isLoadTaskDone = false;
                    [self.navigationController popToViewController:pdvc animated:YES];
                }
                else
                    [self.navigationController popViewControllerAnimated:YES];
            }
            
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        appDelegate().isLoginApp =  true;
                        [self toSendmsg];
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                        
                    }
                }];
            }
            else {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[DAXUtils sharedInstance] hideProgressHud];
        [WFHudView showMsg:@"网络好像出了点问题" inView:nil];
    }];
    
    
}





@end
