//
//  OrderListViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/22.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "OrderListViewController.h"
#import "FJPullTableView.h"
#import "OrderListCell.h"
#import "OrderDetail.h"
#import "LoginViewController.h"
#import "ProductInfo.h"
#import "DetailInfo.h"
#import "ScanViewController.h"
#import <AGCommon/UINavigationBar+Common.h>

@interface OrderListViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate,ISSShareViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *orderlist;
@property (retain, nonatomic) NSArray *itemname;
@property (nonatomic,retain) UISegmentedControl *segment;
@property (retain, nonatomic) NSArray *navitemname;
@property (nonatomic,retain) UISegmentedControl *navsegment;
@property (nonatomic, retain) NSDictionary *dict;
@property (nonatomic, retain) NSDictionary *orderdetail;
@property (nonatomic, retain) NSIndexPath *indexPath;
@property (nonatomic, retain) NSString *url;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL isTuan;
@property int offset;
//@property (nonatomic, retain) InboxInfo *Info;
- (void)fetchTaskLists:(NSString *)propId;
- (void)addPullTableView;
@end

@implementation OrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navitemname = @[@"订单", @"团券"];
    self.navsegment = [[UISegmentedControl alloc]initWithItems:self.navitemname];
    self.navsegment.frame = CGRectMake(80.0f, 8.0f, 150.0f, 30.0f);
    [self.navigationItem setTitleView:_navsegment];
    [self.navsegment addTarget:self action:@selector(navselected:) forControlEvents:UIControlEventValueChanged];
    self.navsegment.selectedSegmentIndex=0;
    
    
    self.automaticallyAdjustsScrollViewInsets = false;
    //分页实现
    self.itemname = @[@"待付款", @"已付款", @"已提货"];
    self.segment = [[UISegmentedControl alloc]initWithItems:self.itemname];
    self.segment.frame = CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20, [[UIScreen mainScreen] bounds].size.width, 40);
    [self.segment addTarget:self action:@selector(selected:) forControlEvents:UIControlEventValueChanged];
    self.segment.selectedSegmentIndex = 0;
    [self.view addSubview:self.segment ];
    [self setSegmentStyle];
    self.automaticallyAdjustsScrollViewInsets = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.segment setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    self.segment.tintColor = [UIColor clearColor];
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor lightGrayColor],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateNormal];
    
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor colorWithRed:1 green:.77 blue:0 alpha:1],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateSelected];
    
    [self.segment setDividerImage:[UIImage imageNamed:@"divider.png"]
              forLeftSegmentState:UIControlStateNormal
                rightSegmentState:UIControlStateNormal
                       barMetrics:UIBarMetricsDefault];
    
    
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    if (!self.isLoadTaskDone) {
        self.offset = 1;
        [self fetchTaskLists:@"1"];
    }
    
}
- (void)setSegmentStyle {
    [self.segment setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    self.segment.tintColor = [UIColor clearColor];
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor lightGrayColor],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateNormal];
    
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor colorWithRed:1 green:.77 blue:0 alpha:1],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateSelected];
    
    [self.segment setDividerImage:[UIImage imageNamed:@"divider.png"]
              forLeftSegmentState:UIControlStateNormal
                rightSegmentState:UIControlStateNormal
                       barMetrics:UIBarMetricsDefault];
    
}

-(void)navselected:(id)sender{
    UISegmentedControl* control = (UISegmentedControl*)sender;
    switch (control.selectedSegmentIndex) {
        case 0:
            self.isTuan = false;
            self.itemname = @[@"未付款", @"已付款",@"已提货"];
            self.segment = [[UISegmentedControl alloc]initWithItems:self.itemname];
            self.segment.frame = CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20, [[UIScreen mainScreen] bounds].size.width, 40);
            [self.segment addTarget:self action:@selector(selected:) forControlEvents:UIControlEventValueChanged];
            self.segment.selectedSegmentIndex = 0;
            [self.segment removeFromSuperview];
            [self.view addSubview:self.segment ];
            [self selected:self.segment];
            [self setSegmentStyle];
            break;
        default:
            self.isTuan = true;
            self.itemname = @[@"未付款", @"已付款",@"已提货"];
            self.segment = [[UISegmentedControl alloc]initWithItems:self.itemname];
            self.segment.frame = CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20, [[UIScreen mainScreen] bounds].size.width, 40);
            [self.segment addTarget:self action:@selector(selected:) forControlEvents:UIControlEventValueChanged];
            self.segment.selectedSegmentIndex = 0;
            [self.segment removeFromSuperview];
            [self.view addSubview:self.segment ];
            [self selected:self.segment];
            [self setSegmentStyle];
            break;
    }
}


- (void)fetchTaskLists:(NSString *)propId{
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    if(self.isTuan){
        self.url = mGetGroupOrders;
    }
    else {
        self.url = mOrderfind;
    }
    
    NSDictionary *dict = @{@"propId": propId,
                           @"currentPage":[NSNumber numberWithInt:self.offset],
                           @"pageSize":@5};
    [params addGetParams:dict];
    //__block OrderListViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:self.url param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                self.isLoadTaskDone = YES;
                NSDictionary *temp  = [resultObj objectForKey:@"object"];
                
                if (self.offset == 1) {
                    self.orderlist = [temp objectForKey:@"recordList"];
                    self.offset += 1;
                }
                else if([[temp objectForKey:@"pageCount"] intValue] >= self.offset){
                    NSMutableArray *order = [temp objectForKey:@"recordList"];
                    NSMutableArray *orderlist = [[NSMutableArray alloc] init];
                    [orderlist addObjectsFromArray:self.orderlist];
                    [orderlist addObjectsFromArray:order];
                    self.orderlist = orderlist;
                    self.offset += 1;
                }
                
                if (!self.pullTableView) {
                    [self addPullTableView];
                }
                [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
            }
            
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        appDelegate().isLoginApp =  true;
                        [self fetchTaskLists:propId];
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                        
                    }
                }];
                
                
            }
            else {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
        
    }];
}

-(void)selected:(id)sender{
    UISegmentedControl* control = (UISegmentedControl*)sender;
    switch (control.selectedSegmentIndex) {
        case 0:
            self.offset = 1;
            [self fetchTaskLists:@"1"];
            break;
        case 1:
            self.offset = 1;
            [self fetchTaskLists:@"2"];
            
            break;
        default:
            self.offset = 1;
            [self fetchTaskLists:@"3"];
            break;
    }
}

- (void)addPullTableView {
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+60,  self.view.frame.size.width,  self.view.frame.size.height-150)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
}

#pragma mark - UITableViewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.orderlist count];
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 116.0f;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifyCell = @"OrderListCell";
    OrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
    if (!cell) {
        cell = (OrderListCell *)[[[NSBundle mainBundle] loadNibNamed:@"OrderListCell"
                                                               owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //cell.lbValue.hidden = YES;
        //cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
    }
    NSDictionary *temp1 = [self.orderlist objectAtIndex:indexPath.row];
    // NSDictionary *temp2 = [temp1 objectForKey:@"orderDetails"];
    if (self.isTuan && self.segment.selectedSegmentIndex != 0) {
        cell.orderName.text = @"开团吧号：";
        cell.orderNo.text = [[[temp1 objectForKey:@"groupInstance"] objectForKey:@"id"] stringValue];
    }
    else
        cell.orderNo.text = [temp1 objectForKey:@"uuid"];
    cell.date.text = [self transformTodate1:[temp1 objectForKey:@"postTime"]];
    cell.time.text = [self transformTodate2:[temp1 objectForKey:@"postTime"]];
    cell.money.text = [[temp1 objectForKey:@"money"] stringValue];
    
    if(self.segment.selectedSegmentIndex == 0){
        cell.payMoney.hidden = FALSE;
        cell.payMoney.tag = indexPath.row;
        [cell.payMoney addTarget:self action:@selector(payMoney:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else if(self.segment.selectedSegmentIndex == 1){
        cell.checkOrder.hidden = FALSE;
        cell.takeGoods.hidden = FALSE;
        cell.checkOrder.tag = indexPath.row;
        cell.takeGoods.tag = indexPath.row;
        if(self.isTuan){
            [cell.takeGoods setImage:[UIImage imageNamed:@"tuanhaofenxiang"] forState:UIControlStateNormal];
            [cell.takeGoods addTarget:self action:@selector(toShare:) forControlEvents:UIControlEventTouchUpInside];
            cell.checkOrder.hidden = TRUE;
        }
        else
            [cell.takeGoods addTarget:self action:@selector(toCode:) forControlEvents:UIControlEventTouchUpInside];
        [cell.checkOrder addTarget:self action:@selector(checkOrder:) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        cell.mComment.hidden = FALSE;
        [cell.mComment addTarget:self action:@selector(toComment:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderListCell *cell = (OrderListCell *)[self.pullTableView cellForRowAtIndexPath:indexPath ];
    if (!cell.mComment.hidden) {
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"已提货";
        OrderDetail *pdvc = [[OrderDetail alloc] initWithNibName:@"OrderDetail" bundle:nil];
        pdvc.isFromComment = true;
        if(self.isTuan)
            pdvc.flag = 1;
        pdvc.dict = [self.orderlist objectAtIndex:indexPath.row];
        pdvc.hidesBottomBarWhenPushed = YES;
        pdvc.total = cell.money.text;
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    if (self.segment.selectedSegmentIndex == 1) {
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"已付款";
        OrderDetail *pdvc = [[OrderDetail alloc] initWithNibName:@"OrderDetail" bundle:nil];
        pdvc.dict = [self.orderlist objectAtIndex:indexPath.row];
        if(self.isTuan){
            pdvc.flag = 1;
            pdvc.isFromTuan = YES;
        }
        pdvc.hidesBottomBarWhenPushed = YES;
        pdvc.total = cell.money.text;
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    if (!cell.payMoney.hidden) {
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"待付款";
        OrderDetail *pdvc = [[OrderDetail alloc] initWithNibName:@"OrderDetail" bundle:nil];
        pdvc.dict = [self.orderlist objectAtIndex:indexPath.row];
        pdvc.hidesBottomBarWhenPushed = YES;
        if(self.isTuan)
            pdvc.flag = 3;
        else
            pdvc.flag = 2;
        pdvc.total = cell.money.text;
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
}

#pragma mark   ==============时间戳转时间的方法==============
- (NSString *)transformTodate1:(NSString *) mTime
{
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MM月dd日"];
    
    //设置时区,这个对于时间的处理有时很重要
    //例如你在国内发布信息,用户在国外的另一个时区,你想让用户看到正确的发布时间就得注意时区设置,时间的换算.
    //例如你发布的时间为2010-01-26 17:40:50,那么在英国爱尔兰那边用户看到的时间应该是多少呢?
    //他们与我们有7个小时的时差,所以他们那还没到这个时间呢...那就是把未来的事做了
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    // 时间戳转时间的方法
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[mTime longLongValue]/1000 ];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}

- (NSString *)transformTodate2:(NSString *) mTime
{
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    //设置时区,这个对于时间的处理有时很重要
    //例如你在国内发布信息,用户在国外的另一个时区,你想让用户看到正确的发布时间就得注意时区设置,时间的换算.
    //例如你发布的时间为2010-01-26 17:40:50,那么在英国爱尔兰那边用户看到的时间应该是多少呢?
    //他们与我们有7个小时的时差,所以他们那还没到这个时间呢...那就是把未来的事做了
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    // 时间戳转时间的方法
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[mTime longLongValue]/1000 ];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}



#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidScroll:scrollView];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidEndDragging:scrollView];
    }
}


#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    
    self.isRefresh = YES;
    self.offset = 1;
    if(self.segment.selectedSegmentIndex == 0)
        [self fetchTaskLists:@"1"];
    else if(self.segment.selectedSegmentIndex == 1){
        [self fetchTaskLists:@"2"];
    }
    else {
        [self fetchTaskLists:@"3"];
    }
}
- (void)beginToLoadMoreData:(FJPullTableView *)tableView {
    self.isRefresh = NO;
    if(self.segment.selectedSegmentIndex == 0)
        [self fetchTaskLists:@"1"];
    else if(self.segment.selectedSegmentIndex == 1){
        [self fetchTaskLists:@"2"];
    }
    else {
        [self fetchTaskLists:@"3"];
    }
}


#pragma mark - 按钮功能

//评论
-(void)  toComment:(id)sender {
    
    UIButton *temp = (UIButton *)sender;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"已提货";
    OrderDetail *pdvc = [[OrderDetail alloc] initWithNibName:@"OrderDetail" bundle:nil];
    pdvc.isFromComment = true;
    pdvc.dict = [self.orderlist objectAtIndex:temp.tag];
    pdvc.hidesBottomBarWhenPushed = YES;
    if(self.isTuan){
        pdvc.flag = 1;
        NSDictionary *temp1 = [pdvc.dict objectForKey:@"group"];
        NSString *price = [[temp1 objectForKey:@"personPrice"] stringValue];
        pdvc.total = price;
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    else{
        self.indexPath = [NSIndexPath indexPathForRow:temp.tag inSection:0];
        OrderListCell *cell = (OrderListCell *)[self.pullTableView cellForRowAtIndexPath:self.indexPath ];
        pdvc.total = cell.money.text;
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
}

//查看订单
-(void)  checkOrder:(id)sender {
    
    UIButton *temp = (UIButton *)sender;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"已付款";
    OrderDetail *pdvc = [[OrderDetail alloc] initWithNibName:@"OrderDetail" bundle:nil];
    pdvc.dict = [self.orderlist objectAtIndex:temp.tag];
    pdvc.hidesBottomBarWhenPushed = YES;
    self.indexPath = [NSIndexPath indexPathForRow:temp.tag inSection:0];
    OrderListCell *cell = (OrderListCell *)[self.pullTableView cellForRowAtIndexPath:self.indexPath ];
    pdvc.total = cell.money.text;
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
}

- (void)viewOnWillDisplay:(UIViewController *)viewController shareType:(ShareType)shareType
{
    [viewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"background_top_bar.png"]];

}

//查看订单
-(void)  toShare:(id)sender {
    UIButton *temp = (UIButton *)sender;
    NSDictionary *temp1 = [self.orderlist objectAtIndex:temp.tag];
    NSString *groupid = [[[temp1 objectForKey:@"groupInstance"] objectForKey:@"id"] stringValue];
    NSArray *imageId = [[[[temp1 objectForKey:@"group"] objectForKey:@"product"] objectForKey:@"imageIds"] componentsSeparatedByString:@";"];
    NSString *picUrl = mSetimage;
    picUrl = [picUrl  stringByAppendingFormat:@"%@%@", [imageId objectAtIndex:0], @"?p=0"];
    NSString *contentPath = @"https://rs88881234.com/chinesejie/html/help/group/GroupHelp.html?id=";
    contentPath = [contentPath stringByAppendingString:groupid];
    NSString *content = @"实惠好吃的";
    content = [content stringByAppendingFormat:@"%@,大家快来一起参团啊！%@",[[[temp1 objectForKey:@"group"] objectForKey:@"product"] objectForKey:@"description"],contentPath];
    
    //构造分享内容
    
    id<ISSContent> publishContent = [ShareSDK content:content
                                       defaultContent:@"实惠的团购"
                                                image:[ShareSDK imageWithUrl:picUrl]
                                                title:@"实惠的团购"
                                                  url:contentPath
                                          description:content
                                            mediaType:SSPublishContentMediaTypeNews];
    //powerByHidden:这个参数是去掉授权界面Powered by ShareSDK的标志
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES      allowCallback:NO
                                                                scopes:nil powerByHidden:YES followAccounts:nil authViewStyle:SSAuthViewStyleFullScreenPopup viewDelegate:self authManagerViewDelegate:nil];
    id<ISSShareOptions> shareOptions = [ShareSDK defaultShareOptionsWithTitle:@"内容分享"
                                                              oneKeyShareList:[NSArray defaultOneKeyShareList]
                                                               qqButtonHidden:YES
                                                        wxSessionButtonHidden:YES
                                                       wxTimelineButtonHidden:YES
                                                         showKeyboardOnAppear:NO
                                                            shareViewDelegate:self
                                                          friendsViewDelegate:self
                                                        picViewerViewDelegate:nil];
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:authOptions
                      shareOptions:shareOptions
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSString *str2 = NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功");
                                    NSLog(@"%@ ", str2);
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    
                                    NSLog( @"分享失败,错误码:%ld,错误描述:%@", (long)[error errorCode], [error errorDescription]);
                                }
                            }];
    
}



//立即付款
-(void) payMoney:(id)sender {
    
    UIButton *temp = (UIButton *)sender;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"待付款";
    OrderDetail *pdvc = [[OrderDetail alloc] initWithNibName:@"OrderDetail" bundle:nil];
    pdvc.dict = [self.orderlist objectAtIndex:temp.tag];
    pdvc.hidesBottomBarWhenPushed = YES;
    if(self.isTuan){
        pdvc.flag = 3;
        NSDictionary *temp1 = [pdvc.dict objectForKey:@"group"];
        NSString *price = [[temp1 objectForKey:@"personPrice"] stringValue];
        pdvc.total = price;
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    else{
        pdvc.flag = 2;
        self.indexPath = [NSIndexPath indexPathForRow:temp.tag inSection:0];
        OrderListCell *cell = (OrderListCell *)[self.pullTableView cellForRowAtIndexPath:self.indexPath ];
        pdvc.total = cell.money.text;
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
}


//条形码
-(void) toCode:(id)sender {
    
    UIButton *temp = (UIButton *)sender;
    NSDictionary *temp1 = [self.orderlist objectAtIndex:temp.tag];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    ScanViewController *pdvc = [[ScanViewController alloc] initWithNibName:@"ScanViewController" bundle:nil];
    pdvc.uuid = [temp1 objectForKey:@"uuid"];
    pdvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
