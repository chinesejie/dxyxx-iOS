//
//  TopConfirmViewCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/20.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopConfirmViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIButton *express;
@property (retain, nonatomic) IBOutlet UIButton *ziti;
@property (retain, nonatomic) IBOutlet UILabel *title;
@property (retain, nonatomic) IBOutlet UIButton *pay1;
@property (retain, nonatomic) IBOutlet UIButton *pay2;
@property (retain, nonatomic) IBOutlet UIButton *pay3;
@property (retain, nonatomic) NSString *payType;
@property (retain, nonatomic) NSString *deliveryType;
@end
