//
//  CouponViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/5/5.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CouponViewController.h"
#import "CommitViewController.h"

@interface CouponViewController () <UITextFieldDelegate>

@end

@implementation CouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tenpay.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.alipay.backgroundColor = [UIColor whiteColor];
    [self.alipay setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.confirm.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.payType = @"tenpay";
    [self.tenpay addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    [self.alipay addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
    [self.confirm addTarget:self action:@selector(Buycoupon:) forControlEvents:UIControlEventTouchUpInside];
    self.tenpay.tag = 1;
    self.alipay.tag = 2;
    
    self.Mymoney.keyboardType = UIKeyboardTypeDecimalPad;
    self.Mymoney.delegate = self;
    self.mNote.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSArray *list=self.navigationController.navigationBar.subviews;
    
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
    
    
}



#pragma mark UITextField
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.Mymoney) {
        NSScanner      *scanner    = [NSScanner scannerWithString:string];
        NSCharacterSet *numbers;
        NSRange         pointRange = [textField.text rangeOfString:@"."];
        
        if ( (pointRange.length > 0) && (pointRange.location < range.location  || pointRange.location > range.location + range.length) )
        {
            numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        }
        else
        {
            numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
        }
        
        if ( [textField.text isEqualToString:@""] && [string isEqualToString:@"."] )
        {
            return NO;
        }
        
        short remain = 2; //默认保留2位小数
        
        NSString *tempStr = [textField.text stringByAppendingString:string];
        NSUInteger strlen = [tempStr length];
        if(pointRange.length > 0 && pointRange.location > 0){ //判断输入框内是否含有“.”。
            if([string isEqualToString:@"."]){ //当输入框内已经含有“.”时，如果再输入“.”则被视为无效。
                return NO;
            }
            if(strlen > 0 && (strlen - pointRange.location) > remain+1){ //当输入框内已经含有“.”，当字符串长度减去小数点前面的字符串长度大于需要要保留的小数点位数，则视当次输入无效。
                return NO;
            }
            if(strlen > 0 && pointRange.location > 4){ //当输入框内已经含有“.”，当字符串长度减去小数点前面的字符串长度大于需要要保留的小数点位数，则视当次输入无效。
                return NO;
            }
        }
        else{
            if(strlen > 4 && ![string isEqualToString:@"."]){ //超过千无效。
                return NO;
            }
        }
        
        NSRange zeroRange = [textField.text rangeOfString:@"0"];
        if(zeroRange.length == 1 && zeroRange.location == 0){ //判断输入框第一个字符是否为“0”
            if(![string isEqualToString:@"0"] && ![string isEqualToString:@"."] && [textField.text length] == 1){ //当输入框只有一个字符并且字符为“0”时，再输入不为“0”或者“.”的字符时，则将此输入替换输入框的这唯一字符。
                textField.text = string;
                return NO;
            }else{
                if(pointRange.length == 0 && pointRange.location > 0){ //当输入框第一个字符为“0”时，并且没有“.”字符时，如果当此输入的字符为“0”，则视当此输入无效。
                    if([string isEqualToString:@"0"]){
                        return NO;
                    }
                }
            }
        }
        
        NSString *buffer;
        if ( ![scanner scanCharactersFromSet:numbers intoString:&buffer] && ([string length] != 0) )
        {
            return NO;
        }
        
    }
    
    return YES;
}

#pragma mark - 键盘功能
//点击done 按钮 去掉键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.Mymoney resignFirstResponder];
    [self.mNote resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{   //开始编辑时，整体上移
    if (textField == self.mNote) {
        [self moveView:-80];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{     //结束编辑时，整体下移
    if (textField == self.mNote) {
        [self moveView:80];
    }
    if ([textField.text isEqualToString:@"" ] && textField == self.Mymoney){
        self.Totalprice.hidden = true;
       
    }
    
    if (![textField.text isEqualToString:@""]&&textField == self.Mymoney) {
        self.Totalprice.hidden = false;
        float price = [textField.text floatValue] * 0.98;
        self.price = [NSString stringWithFormat:@"%.02f",price];
        self.Totalprice.text = @"￥";
        self.Totalprice.text =  [self.Totalprice.text stringByAppendingString:self.price];
    }
        
}
-(void)moveView:(float)move{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y +=move;//view的X轴上移
    self.view.frame = frame;
    //[UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
    
    
}

#pragma mark - 网络模块

-(void) addTicket {
    //网络数据获取解析
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"payType": self.payType,
                           @"description":self.mNote.text,
                           @"money":self.price};
    [params addGetParams:dict];
    //__block ConfirmViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mTicketadd param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                CommitViewController *pdvc = [[CommitViewController alloc] initWithNibName:@"CommitViewController" bundle:nil];
                pdvc.dict = [resultObj objectForKey:@"object"];
                pdvc.flag = 3;
                //int count = [self.datasource count]-indexPath.row-1;
                [self.navigationController pushViewController:pdvc animated:YES];
                [pdvc release];
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        appDelegate().isLoginApp =  true;
                        [self addTicket];
                    }
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];

                    }
                }];

            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
        
    }];

}

#pragma mark - 按钮功能

//立即结算
-(void) Buycoupon:(id)sender {
    float price = [self.Mymoney.text floatValue] ;
    self.price = [NSString stringWithFormat:@"%.02f",price];
//    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    id obj = [formatter numberFromString:self.price];
    if (price == 0.0) {
        mAlertView(@"提示", @"请输入金额");
        return;
    }
    [self addTicket];
}

- (void)choose:(id)sender {
    UIButton *temp = (UIButton *) sender;
    
    temp.selected = TRUE;
    if (temp.tag == 1) {
        self.alipay.selected = FALSE;
        self.tenpay.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
        [self.tenpay setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        self.alipay.backgroundColor = [UIColor whiteColor];
        [self.alipay setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.payType = @"tenpay";
    }
    else {
        self.tenpay.selected = FALSE;
        self.alipay.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
        [self.alipay setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        self.tenpay.backgroundColor = [UIColor whiteColor];
        [self.tenpay setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.payType = @"alipay";
    }
}


- (void)dealloc {
    [_Mymoney release];
    [_tenpay release];
    [_alipay release];
    [_confirm release];
    [_mNote release];
    [_Totalprice release];
    [super dealloc];
}

- (IBAction)test:(UITextField *)sender {
    self.Totalprice.hidden = false;
    float price = [self.Mymoney.text floatValue] * 0.98;
    self.price = [NSString stringWithFormat:@"%.02f",price];
    self.Totalprice.text = @"￥";
    self.Totalprice.text =  [self.Totalprice.text stringByAppendingString:self.price];

}
@end
