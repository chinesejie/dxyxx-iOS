//
//  NavigationController_2.m
//  Dax
//
//  Created by Jay_Ch on 15/3/14.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "NavigationController_2.h"

@interface NavigationController_2 ()

@end

@implementation NavigationController_2

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *selectedImage = [UIImage imageNamed:@"category_active.png"];
    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.selectedImage = selectedImage ;
    self.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    //title 颜色
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationBar.titleTextAttributes = dict;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
