//
//  TopTableViewCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/12.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "TopTableViewCell.h"
#import "ProductInfo.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"


@implementation TopTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
    [_bg release];
    [_mOrder release];
    [_mCoupon release];
    [_mCouponlist release];
    [super dealloc];
}



- (void)configTopTableViewCell{
    
    
    NSMutableArray *viewsArray = [@[] mutableCopy];
    for (int i = 0; i<self.advertArray.count; ++i) {
        ProductInfo *product = [self.advertArray objectAtIndex:i];
        float width  = [[UIScreen mainScreen] bounds].size.width*1.5;
        NSString *picUrl = mSetimage;
        picUrl = [picUrl  stringByAppendingFormat:@"%@%@%f", product.imageId, @"?w=",width];
        [viewsArray addObject:picUrl];
    }
    
    
    //网络加载 --- 创建带标题的图片轮播器
    SDCycleScrollView *cycleScrollView2 = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 160) imageURLStringsGroup:viewsArray]; // 模拟网络延时情景
    cycleScrollView2.pageControlAliment = SDCycleScrollViewPageContolStyleClassic;
    cycleScrollView2.delegate = self;
    cycleScrollView2.dotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    cycleScrollView2.placeholderImage = [UIImage imageNamed:@"loading.png"];
    [self addSubview:cycleScrollView2];
    if (self.advertArray.count == 1) {
        cycleScrollView2.showPageControl = false;
    }
    }

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---点击了第%ld张图片", index);
    ProductInfo *product = (ProductInfo *)[self.advertArray objectAtIndex:index];
    if (![product.mDescription isEqualToString:@""])
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:product.mDescription]];

}


@end





