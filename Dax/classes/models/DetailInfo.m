//
//  DetailInfo.m
//  Dax
//
//  Created by Jay_Ch on 15/3/27.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "DetailInfo.h"

@implementation DetailInfo
- (id)initWithDictionary:(NSDictionary *)theDictionary {
    if (self = [super initWithDictionary:theDictionary]) {
        self.imageIds = [self safeStrValue:[theDictionary objectForKey:@"imageIds"] replace:@""];
        self.productId = [self safeNumValue:[theDictionary objectForKey:@"id"] replace:0];
        self.mDescription = [self safeStrValue:[theDictionary objectForKey:@"description"] replace:@""];
        self.price = [self safeNumValue:[theDictionary objectForKey:@"price"] replace:0];
        self.thumbId =  [self safeStrValue:[theDictionary objectForKey:@"thumbId"] replace:@""];
        self.cId =  [self safeStrValue:[[theDictionary objectForKey:@"category"] objectForKey:@"id"] replace:@""];
        self.mSpecification =  [self safeStrValue:[theDictionary objectForKey:@"specification"] replace:@""];
        //self.amount =  [self safeStrValue:[theDictionary objectForKey:@"amount"] replace:@""];
        self.amount= [self safeNumValue:[theDictionary objectForKey:@"amount"] replace:0];
        NSLog(@"%@",self.amount);
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    //encode properties/values
    [aCoder encodeObject:self.imageIds      forKey:@"imageIds"];
    [aCoder encodeObject:self.productId  forKey:@"id"];
    [aCoder encodeObject:self.mDescription  forKey:@"description"];
    [aCoder encodeObject:self.price  forKey:@"price"];
    [aCoder encodeObject:self.thumbId  forKey:@"thumbId"];
    [aCoder encodeObject:self.mSpecification  forKey:@"specification"];
    [aCoder encodeObject:self.amount  forKey:@"amount"];
    [aCoder encodeObject:self.cId  forKey:@"cId"];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if((self = [super init])) {
        //decode properties/values
        self.imageIds       = [aDecoder decodeObjectForKey:@"imageIds"];
        self.productId   = [aDecoder decodeObjectForKey:@"id"];
        self.mDescription   = [aDecoder decodeObjectForKey:@"description"];
        self.price   = [aDecoder decodeObjectForKey:@"price"];
        self.thumbId   = [aDecoder decodeObjectForKey:@"thumbId"];
        self.mSpecification   = [aDecoder decodeObjectForKey:@"specification"];
        self.amount   = [aDecoder decodeObjectForKey:@"amount"];
        self.cId   = [aDecoder decodeObjectForKey:@"cId"];
    }
    
    return self;
}

@end
