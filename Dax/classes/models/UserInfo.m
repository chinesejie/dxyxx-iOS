//
//  UserInfo.m
//  DAX
//
//  Created by Jay_Ch on 15/3/27.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

- (id)initWithDictionary:(NSDictionary *)theDictionary {
    if (self = [super initWithDictionary:theDictionary]) {
        
        self.uname = [self safeStrValue:[theDictionary objectForKey:@"username"] replace:@""];
        self.createtime = [self safeStrValue:[theDictionary objectForKey:@"createTime"] replace:@""];
        self.thumd = [self safeStrValue:[theDictionary objectForKey:@"image"] replace:@""];

    }
    return self;
}

@end
