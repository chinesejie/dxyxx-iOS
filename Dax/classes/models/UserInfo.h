//
//  UserInfo.h
//  DAX
//
//  Created by Jay_Ch on 15/3/27.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "JRFBaseObject.h"

@interface UserInfo : JRFBaseObject

@property (nonatomic, copy) NSString *collect;
@property (nonatomic, copy) NSString *share;
@property (nonatomic, copy) NSString *exchange;
@property (nonatomic, copy) NSString *uname;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *birthday;
@property (nonatomic, copy) NSString *job;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *income;
@property (nonatomic, copy) NSString *interests;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *createtime;
@property (nonatomic, copy) NSString *thumd;

@end
