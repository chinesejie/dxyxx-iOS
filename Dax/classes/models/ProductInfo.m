//
//  ProductInfo.m
//  Dax
//
//  Created by Jay_Ch on 15/3/27.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "ProductInfo.h"
#import "DetailInfo.h"

@implementation ProductInfo

- (id)initWithDictionary:(NSDictionary *)theDictionary {
    if (self = [super initWithDictionary:theDictionary]) {
        self.imageId = [self safeStrValue:[theDictionary objectForKey:@"imageId"] replace:@""];
        self.mDescription = [self safeStrValue:[theDictionary objectForKey:@"description"] replace:@""];
        self.mPrice = [self safeNumValue:[theDictionary objectForKey:@"price"] replace:0];
        self.mCategoryId = [self safeStrValue:[theDictionary objectForKey:@"id"] replace:@""];
        self.mProduct = dictGetFromDict(theDictionary, @"product");
        self.amount= [self safeNumValue:[theDictionary objectForKey:@"amount"] replace:0];
    }
    return self;
}

@end
