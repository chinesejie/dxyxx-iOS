//
//  CommentInfo.m
//  Dax
//
//  Created by Jay_Ch on 15/6/1.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CommentInfo.h"

@implementation CommentInfo
- (id)initWithDictionary:(NSDictionary *)theDictionary {
    if (self = [super initWithDictionary:theDictionary]) {
        self.content = [self safeStrValue:[theDictionary objectForKey:@"word"] replace:@""];
        self.user = [self safeStrValue:[theDictionary objectForKey:@"userName"] replace:@""];
        self.images = [self safeStrValue:[theDictionary objectForKey:@"images"] replace:@""];
        self.time = [self safeStrValue:[theDictionary objectForKey:@"postTime"] replace:@""];
        self.thumb = [self safeStrValue:[theDictionary objectForKey:@"user_image"] replace:@""];
        self.commentId = [self safeStrValue:[theDictionary objectForKey:@"id"] replace:@""];
    }
    return self;
}
@end
