//
//  DetailInfo.h
//  Dax
//
//  Created by Jay_Ch on 15/3/27.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "JRFBaseObject.h"

@interface DetailInfo : JRFBaseObject
@property (nonatomic, copy) NSString *imageIds;
@property (nonatomic, copy) NSNumber *productId;
@property (nonatomic, copy) NSString *thumbId;
@property (nonatomic, copy) NSString *cId;
@property (nonatomic, copy) NSString *mSpecification;
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, copy) NSString *mDescription;
@property (nonatomic, copy) NSNumber *amount;
- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;
@end
