//
//  WLLocation.h
//  WeiLu
//
//  Created by fengjia on 7/8/13.
//  Copyright (c) 2013 fengjia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^LocationInfo)(NSString *strLocation);
typedef void (^LocationCityInfo)(NSString *strLocation, NSString *strCity);
typedef void (^Location)(CLLocation *location);
@interface WLLocation : NSObject <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;  
    CLGeocoder *geocoder;//CLGeocoder
    LocationInfo _locaitonBlock;
    LocationCityInfo _cityBlock;
//    Location _locBlock;
    BOOL isCity;
}
@property (nonatomic, copy)  Location _locBlock;
+ (id)shareInstance;
- (NSString *)fetchLoactionInfo:(LocationInfo)lcoationBlock;
- (NSString *)fetchCity:(LocationCityInfo)block;

- (void)fetchLocation:(Location)block;
- (void)fetchCity:(CLLocation *)location block:(LocationCityInfo)block;
@end
