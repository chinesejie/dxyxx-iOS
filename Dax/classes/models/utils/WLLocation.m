//
//  WLLocation.m
//  WeiLu
//
//  Created by fengjia on 7/8/13.
//  Copyright (c) 2013 fengjia. All rights reserved.
//

#import "WLLocation.h"
static WLLocation *myLocation = nil;
@implementation WLLocation


- (void)dealloc {
    [locationManager release];
    [geocoder release];
    [super dealloc];
}
+ (id)shareInstance {
    if (!myLocation) {
        myLocation = [[WLLocation alloc] init];
    }
    return myLocation;
}

- (NSString *)fetchLoactionInfo:(LocationInfo)locationBlock {
    isCity = NO;
    _locaitonBlock = [locationBlock copy];
    if ([CLLocationManager locationServicesEnabled]) {
        if (!locationManager) {
            locationManager = [[CLLocationManager alloc] init];//创建位置管理器
            locationManager.delegate = self; //设置代理
            locationManager.desiredAccuracy = kCLLocationAccuracyBest; //指定需要的精度级别
            locationManager.distanceFilter = kCLDistanceFilterNone; //
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [locationManager requestAlwaysAuthorization];
            }
        }
        if (!geocoder) {
            geocoder = [[CLGeocoder alloc] init];
        }
        [locationManager startUpdatingLocation]; //启动位置管理器
    }
    
    return nil;
}
- (NSString *)fetchCity:(LocationCityInfo)block {
    isCity = YES;
    _cityBlock = [block copy];
    if ([CLLocationManager locationServicesEnabled]) {
        if (!locationManager) {
            locationManager = [[CLLocationManager alloc] init];//创建位置管理器
            locationManager.delegate = self; //设置代理
            locationManager.desiredAccuracy = kCLLocationAccuracyBest; //指定需要的精度级别
            locationManager.distanceFilter = 1; //
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [locationManager requestAlwaysAuthorization];
            }
        }
        if (!geocoder) {
            geocoder = [[CLGeocoder alloc] init];
            
        }
        [locationManager startUpdatingLocation]; //启动位置管理器
    }
    
    return nil;
}

- (void)fetchLocation:(Location)block {
    self._locBlock = [block copy];
    if ([CLLocationManager locationServicesEnabled]) {
        if (!locationManager) {
            locationManager = [[CLLocationManager alloc] init];//创建位置管理器
            locationManager.delegate = self; //设置代理
            locationManager.desiredAccuracy = kCLLocationAccuracyBest; //指定需要的精度级别
            locationManager.distanceFilter = 1; //
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [locationManager requestAlwaysAuthorization];
            }
        }
        if (!geocoder) {
            geocoder = [[CLGeocoder alloc] init];
            
        }
        [locationManager startUpdatingLocation]; //启动位置管理器
    }
}
- (void)fetchCity:(CLLocation *)location block:(LocationCityInfo)block {
    if (!geocoder) {
        geocoder = [[CLGeocoder alloc] init];
        
    }
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error)
         {
             NSLog(@"failed with error: %@", error);
             block(nil, nil);
             return;
         }
         if(placemarks.count > 0)
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSDictionary *dict = placemark.addressDictionary;
             NSString *strLoc = [NSString stringWithFormat:@"%@", [dict objectForKey:@"State"] ? [dict objectForKey:@"State"] : @"北京市"];
             if ([dict objectForKey:@"SubLocality"]) {
                 strLoc = [NSString stringWithFormat:@"%@%@", strLoc, [dict objectForKey:@"SubLocality"]];
             }
             if ([dict objectForKey:@"Street"]) {
                 strLoc = [NSString stringWithFormat:@"%@　%@", strLoc, [dict objectForKey:@"Street"]];
             } else if ([dict objectForKey:@"Thoroughfare"]) {
                 strLoc = [NSString stringWithFormat:@"%@ %@", strLoc, [dict objectForKey:@"Thoroughfare"]];
             }
             
             block(strLoc, [dict objectForKey:@"State"] ? [dict objectForKey:@"State"] : @"北京市");
//                          NSLog(@"%@", [dict objectForKey:@"State"]);
//                          NSLog(@"%@", [dict objectForKey:@"Street"]);
//                         NSLog(@"%@", [dict objectForKey:@"SubLocality"]);
//                          NSLog(@"%@", [dict objectForKey:@"SubThoroughfare"]);
//                          NSLog(@"%@", [dict objectForKey:@"Thoroughfare"]);
         }
     }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    self._locBlock(newLocation);
    self._locBlock = nil;
    
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    [locationManager release];
    locationManager = nil;
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    NSLog(@"%@", error);
    self._locBlock(nil);
    self._locBlock = nil;
    locationManager.delegate = nil;
    [locationManager release];
    locationManager = nil;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [locationManager requestWhenInUseAuthorization];
            }
            break;
        default:
            break;
    }
}

@end
