//
//  DAXUtils.m
//  DAX
//
//  Created by fengjia on 14-5-23.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import "DAXUtils.h"
@implementation DAXUtils

+ (id)sharedInstance {
    static DAXUtils *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DAXUtils alloc] init];
    });
    return instance;
}

- (void)showProgressHud:(UIView *)inView withMessage:(NSString *)message {
    [self hideProgressHud];
    if (!HUD) {
        HUD = [[MBProgressHUD alloc] initWithView:inView];
        HUD.labelText = message;
        HUD.dimBackground = YES;
        [inView addSubview:HUD];
        [HUD show:YES];
    }
}
- (void)hideProgressHud {
    if (HUD) {
        [HUD removeFromSuperview];
        [HUD release];
        HUD = nil;
    }
}
- (void)showProgressHudAndHideDelay:(NSTimeInterval)delay inView:(UIView *)inView withMessage:(NSString *)message {
    if (!HUD) {
        HUD = [[MBProgressHUD alloc] initWithView:inView];
        HUD.detailsLabelText = message;
        HUD.mode = MBProgressHUDModeText;
        [inView addSubview:HUD];
        [HUD show:YES];
        [self performSelector:@selector(hideDelayed) withObject:nil afterDelay:delay];
    }
}
- (void)hideDelayed {
    if (HUD) {
        [HUD hide:YES];
        [HUD removeFromSuperview];
        [HUD release];
        HUD = nil;
        
    }
}


- (void)clearAppCache {
    NSString *path = [[[NSHomeDirectory() stringByAppendingPathComponent:@"Library"] stringByAppendingPathComponent:@"Caches"] stringByAppendingPathComponent:@"ImageCache"];
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isDir = YES;
    if ([fm fileExistsAtPath:path isDirectory:&isDir]) {
        NSArray *arr = [fm subpathsAtPath:path];
        NSString* fileName;
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        for (int i = 0; i < [arr count]; i++) {
            if (i %20 == 0) {
                [pool drain];
                pool = [[NSAutoreleasePool alloc] init];
            }
            fileName = [arr objectAtIndex:i];
            NSString* fileAbsolutePath = [path stringByAppendingPathComponent:fileName];
            [fm removeItemAtPath:fileAbsolutePath error:nil];
        }
        [pool drain];
    }
}

- (NSString *)fetchAppCache {
    NSString *path = [[[NSHomeDirectory() stringByAppendingPathComponent:@"Library"] stringByAppendingPathComponent:@"Caches"] stringByAppendingPathComponent:@"ImageCache"];
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isDir = YES;
    if ([fm fileExistsAtPath:path isDirectory:&isDir])  {
        
        NSArray *arr = [fm subpathsAtPath:path];
        NSString* fileName;
        long long totalsize = 0;
        for (int i = 0; i < [arr count]; i++) {
            fileName = [arr objectAtIndex:i];
            NSString* fileAbsolutePath = [path stringByAppendingPathComponent:fileName];
            NSDictionary *fileSysAttributes = [fm attributesOfItemAtPath:fileAbsolutePath error:nil];
            NSNumber *filesize = [fileSysAttributes objectForKey:NSFileSize];
            totalsize += [filesize longValue];
        }
        return [NSString stringWithFormat:@"%.2fM", totalsize/1024.0/1024.0];
        
        
    }
    return nil;
}
@end
