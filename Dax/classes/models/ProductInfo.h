//
//  ProductInfo.h
//  Dax
//
//  Created by Jay_Ch on 15/3/27.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "JRFBaseObject.h"

@interface ProductInfo : JRFBaseObject
@property (nonatomic, copy) NSString *imageId;
@property (nonatomic, copy) NSString *mDescription;
@property (nonatomic, copy) NSNumber *mPrice;
@property (nonatomic, copy) NSString *mCategoryId;
@property (nonatomic, copy) NSDictionary *mProduct;
@property (nonatomic, copy) NSNumber *amount;
@end

