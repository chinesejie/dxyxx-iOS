//
//  TaskInfo.m
//  DAX
//
//  Created by Jay_Ch on 15/3/27.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "InboxInfo.h"

@implementation InboxInfo
- (id)initWithDictionary:(NSDictionary *)theDictionary {
    if (self = [super initWithDictionary:theDictionary]) {
        
        self.tId = [self safeStrValue:[theDictionary objectForKey:@"id"] replace:@""];
        self.title = [self safeStrValue:[theDictionary objectForKey:@"title"] replace:@""];
        self.content = [self safeStrValue:[theDictionary objectForKey:@"content"] replace:@""];
        NSString *dtime1 = [self safeStrValue:[theDictionary objectForKey:@"createTime"] replace:@""];
        self.creTime = [dtime1 substringToIndex:11];
        self.website = [self safeStrValue:[theDictionary objectForKey:@"website"] replace:@""];
         self.readd = [self safeStrValue:[theDictionary objectForKey:@"readd"] replace:@""];
    }
   // NSLog(@"@",self.images.description);
    return self;
}
@end
