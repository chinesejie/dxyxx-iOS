//
//  CommentInfo.h
//  Dax
//
//  Created by Jay_Ch on 15/6/1.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "JRFBaseObject.h"

@interface CommentInfo : JRFBaseObject
@property (strong, nonatomic) NSString *thumb;
@property (strong, nonatomic) NSString *user;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *images;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *commentId;
@end
