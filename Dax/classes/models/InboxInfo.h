//
//  TaskInfo.h
//  DAX
//
//  Created by Jay_Ch on 15/3/27.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "JRFBaseObject.h"
@interface InboxInfo : JRFBaseObject
@property (nonatomic, copy) NSString *tId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *creTime;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, copy) NSString *readd;
@end
