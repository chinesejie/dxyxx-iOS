//
//  CartViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/12.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CartViewController.h"
#import "FJPullTableView.h"
#import "ConfirmViewController.h"
#import "ListViewCell.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "CustomBadge.h"
#import "PurchaseViewController.h"
#import "LoginViewController.h"

@interface CartViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (retain, nonatomic) NSArray *itemname;
@property (retain, nonatomic) UIButton *mCart;
@property (retain, nonatomic) UIButton *mAddtocart;
@property (retain, nonatomic) UIView *mBottom;
//@property (nonatomic, retain) InboxInfo *Info;
- (void)addPullTableView;
@end

@implementation CartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"购物车";
    self.datasource = [NSMutableArray array];
    [self addPullTableView];
    self.automaticallyAdjustsScrollViewInsets = false;
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //定制自己的风格的  UIBarButtonItem
    UIBarButtonItem *clearButton= [[UIBarButtonItem alloc] initWithTitle:@"清空购物车" style:UIBarButtonItemStyleDone target:self action:@selector(removeCart:)];
    [self.navigationItem setRightBarButtonItem:clearButton];
    
    
    NSArray *list=self.navigationController.navigationBar.subviews;
    
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;

    
    [self initializeDefaultDataList];
    [self.pullTableView reloadData];
    self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[self.datasource count]];
    if([self.datasource count] == 0)
        self.navigationController.tabBarItem.badgeValue = nil;
    [self bottombar];
}

//buttom bar
-(void)bottombar {
    //底栏
    CGRect rect = CGRectMake(0,[[UIScreen mainScreen] bounds].size.height-95,[[UIScreen mainScreen] bounds].size.width,55);
    self.mBottom.backgroundColor = [UIColor whiteColor];
    UIView *view = [[UIView alloc]initWithFrame:rect];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    view.layer.borderWidth = 0.5f;
    view.layer.borderColor = [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor;
    //view.backgroundColor = [UIColor clearColor];
    
    //定义label
    UILabel * label1 = [[[UILabel alloc] init] autorelease];
    label1.frame = CGRectMake(16,15,51, 21);
    label1.font=[UIFont fontWithName:@"Arial" size:17];
    label1.text = @"合计：";
    label1.textColor = [UIColor blackColor];
    
    UILabel * label2 = [[[UILabel alloc] init] autorelease];
    label2.frame = CGRectMake(60,17,12, 21);
    label2.font=[UIFont fontWithName:@"Arial" size:14];
    label2.text = @"￥";
    label2.textColor = [UIColor redColor];
    
    float total = 0.0;
    for (int i=0; i<[self.datasource count]; i++) {
        DetailInfo *temp = (DetailInfo *)[self.datasource objectAtIndex:i];
        total += [temp.price floatValue]*[temp.amount intValue];
    }
    UILabel * label3 = [[[UILabel alloc] init] autorelease];
    label3.frame = CGRectMake(74,15,140, 21);
    label3.font=[UIFont fontWithName:@"Arial" size:22];
    label3.text = [NSString stringWithFormat:@"%@",@(total)];
    label3.textColor = [UIColor redColor];
    
    //    UILabel * label4 = [[[UILabel alloc] init] autorelease];
    //    label4.frame = CGRectMake(112,17,79, 21);
    //    label4.font=[UIFont fontWithName:@"Arial" size:14];
    //    label4.text = @"元";
    //    label4.textColor = [UIColor lightGrayColor];
    
    //定制自己的风格的  UIBarButtonItem
    CGRect frame_1= CGRectMake(view.frame.size.width-120,0, 120, view.frame.size.height);
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:frame_1];
    [btn setTitle:@" 立即结算" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font=[UIFont systemFontOfSize:19];
    [btn addTarget:self action:@selector(toConfirm:) forControlEvents:UIControlEventTouchUpInside];
    btn.backgroundColor = [UIColor orangeColor];
    //[btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [view addSubview:label1];
    [view addSubview:label2];
    [view addSubview:label3];
    //[view addSubview:label4];
    [view addSubview:btn];
    [self.view addSubview:view];
    
}


- (void)addPullTableView {
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20,  self.view.frame.size.width,  self.view.frame.size.height-115)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
}

#pragma mark - UITableViewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.datasource count];
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 116.0f;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifyCell = @"ListCell";
    ListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
    if (!cell) {
        cell = (ListViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"ListViewCell"
                                                              owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //cell.lbValue.hidden = YES;
        //cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    DetailInfo *detail = [self.datasource objectAtIndex:indexPath.row];
    NSString *picUrl = mSetimage;
    picUrl = [picUrl  stringByAppendingFormat:@"%@", detail.thumbId];
    [cell.mThumb setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
    cell.mTitle.text = detail.mDescription;
    cell.mDescription.text = detail.mSpecification;
    cell.mPrice.text = [NSString stringWithFormat:@"%@", @([detail.price floatValue])];
    cell.stepper.value = [detail.amount intValue];
    cell.stepper.hidden = FALSE;
    cell.stepper.valueChangedCallback = ^(PKYStepper *stepper, int count) {
        stepper.countLabel.text = [NSString stringWithFormat:@"%@", @(count)];
        [detail setValue:[NSNumber numberWithInteger:count] forKey:@"amount"];
        [self.datasource replaceObjectAtIndex:indexPath.row withObject:detail];
        [self addInfoWithName];
        [self bottombar];
    };
    [cell.stepper setup];
    //[numberFormatter release];
    //[cell configHomeTableViewCell];
    //config the cell
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PurchaseViewController *pdvc = [[PurchaseViewController alloc] initWithNibName:@"PurchaseViewController" bundle:nil];
    pdvc.mProuduct = (DetailInfo *)[self.datasource objectAtIndex:indexPath.row];
    pdvc.cId =  pdvc.mProuduct.cId;
    pdvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:pdvc animated:YES];
    //[self presentViewController:pdvc animated:YES completion:nil];
    // [UIView transitionFromView:self.view toView:pdvc.view duration:0.1f options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
    [pdvc release];
    
}

//单元格返回的编辑风格，包括删除 添加 和 默认  和不可编辑三种风格
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        //        获取选中删除行索引值
        NSInteger row = [indexPath row];
        //        通过获取的索引值删除数组中的值
        [self.datasource removeObjectAtIndex:row];
        [self addInfoWithName];
        //        删除单元格的某一行时，在用动画效果实现删除过程
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[self.datasource count]];
        if([self.datasource count] == 0)
            self.navigationController.tabBarItem.badgeValue = nil;
        [self bottombar];
    }
}

#pragma mark -  NSMutableArray、NSData互转
//NSMutableArray转成NSData

-(void)addInfoWithName{
    //save data
    NSData *cartInfo = [NSKeyedArchiver archivedDataWithRootObject:self.datasource ];
    [[NSUserDefaults standardUserDefaults] setObject:cartInfo forKey:@"cartInfo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// NSData转成NSMutableArray
-(void)initializeDefaultDataList{
    NSData *savedEncodedData = [[NSUserDefaults standardUserDefaults] objectForKey:@"cartInfo"];
    if(savedEncodedData == nil)
    {
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        self.datasource = temp;
    }
    else{
        self.datasource = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:savedEncodedData];
        //appDelegate().datasource = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:savedEncodedData];
        NSLog(@"datasource的信息！！%@",self.datasource);
    }
}




#pragma mark - 按钮功能

//立即结算
-(void) toConfirm:(id)sender {
    
    if (![self.datasource count]) {
        [WFHudView showMsg:@"请先选购" inView:nil];
    }
    else if (appDelegate().isLoginApp) {
        ConfirmViewController *pdvc = [[ConfirmViewController alloc] initWithNibName:@"ConfirmViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        pdvc.datasource = self.datasource;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    else{
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    
}

//清空购物车
-(void) removeCart:(id)sender {
    
    [self.datasource removeAllObjects];
    [self addInfoWithName];
    [self.pullTableView reloadData];
    self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[self.datasource count]];
    if([self.datasource count] == 0)
        self.navigationController.tabBarItem.badgeValue = nil;
    [self bottombar];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
