//
//  MyfruitViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/12.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "MyfruitViewController.h"
#import "MyfruitTopCell.h"
#import "MyfruitMiddleCell.h"
#import "MyfruitInfo.h"
#import "OrderListViewController.h"
#import "FJPullTableView.h"
#import "PurchaseViewController.h"
#import "EditViewController.h"
#import "LoginViewController.h"
#import "ScanViewController.h"
#import "UIButton+WebCache.h"
#import "SDImageCache.h"
#import "CouponListViewController.h"
#import "InboxViewController.h"

@interface MyfruitViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (retain, nonatomic) NSArray *itemname;
@property (nonatomic,retain) UISegmentedControl *segment;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL isLoadDone;
@end

@implementation MyfruitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addPullTableView];
    self.automaticallyAdjustsScrollViewInsets = false;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(appDelegate().isLoginApp && !self.isLoadDone)
        [self fetchInfo];
    if(!appDelegate().isLoginApp){
        [self.pullTableView reloadData];
        self.isLoadDone = false;
    }
    self.navigationController.navigationBar.hidden = TRUE;
        
}


- (void)addPullTableView {
    
    
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,0,  self.view.frame.size.width,  self.view.frame.size.height-45)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
    
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 1;
            break;
        default:
            return 4;
            break;
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return 238.0f;
        
    }
    else
        
        return 50.0f;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //    if(section == 1){
    //        return 51.0f;
    //
    //    }
    //    else
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 51.0f)] autorelease];
    UIImage *pic = [ UIImage imageNamed:@"foot.png"];
    UIImageView *imageView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 51.0f)];
    [imageView setImage:pic];
    [sectionView addSubview:imageView];
    return sectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *identifyCell = @"TopCell";
        MyfruitTopCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
        if (!cell) {
            cell = (MyfruitTopCell *)[[[NSBundle mainBundle] loadNibNamed:@"MyfruitTopCell"
                                                                    owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell.mAccount  addTarget:self action:@selector(toMyInfo:) forControlEvents:UIControlEventTouchUpInside];
        if (appDelegate().isLoginApp) {
            NSString *picUrl = mSetimage;
            picUrl = [picUrl  stringByAppendingFormat:@"%@?w=120&&p=0", [[NSUserDefaults standardUserDefaults] objectForKey:@"portrait"]];
            [cell.mAccount setBackgroundImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
            
            NSString *jifen = [NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"jiFen"] intValue]];
            cell.jifen.text = jifen;
            NSString *mMoney = [NSString stringWithFormat:@"%.2f",[[[NSUserDefaults standardUserDefaults] objectForKey:@"mMoney"] floatValue]];
            cell.mMoney.text = mMoney;
            NSString *mQuan =
            [NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] objectForKey:@"mQuan"] intValue]];
            cell.mQuan.text = mQuan;
            NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
            cell.Name.text = name;
            [cell.Name setNumberOfLines:1];
            [cell.Name sizeToFit];
        }
        
        return cell;
    }
    else{
        static NSString *identifyCell = @"MiddleCell";
        MyfruitMiddleCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
        if (!cell) {
            cell = (MyfruitMiddleCell *)[[[NSBundle mainBundle] loadNibNamed:@"MyfruitMiddleCell"
                                                                       owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.lbValue.hidden = YES;
            //cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        if (indexPath.row == 0) {
            [cell.myImage setImage:[UIImage imageNamed:@"my1"]];
            cell.myTitle.text = @"我的订单";
        }
//        else if (indexPath.row==1) {
//            [cell.myImage setImage:[UIImage imageNamed:@"share1"]];
//            cell.myTitle.text = @"我的分享";
//        }
        else if (indexPath.row == 1){
            [cell.myImage setImage:[UIImage imageNamed:@"group"]];
            cell.myTitle.text = @"我的代金券";
        }
        else if(indexPath.row == 2){
            [cell.myImage setImage:[UIImage imageNamed:@"home_qr"]];
            cell.myTitle.text = @"我的电子会员卡";
        }
        else {
                    [cell.myImage setImage:[UIImage imageNamed:@"inbox"]];
                    cell.myTitle.text = @"我的收件箱";
                }
        //[cell configHomeTableViewCell];
        //config the cell
        return cell;
    }
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     CATransform3D rotation;
     rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
     rotation.m34 = 1.0/ -600;
     
     cell.layer.shadowColor = [[UIColor blackColor]CGColor];
     cell.layer.shadowOffset = CGSizeMake(10, 10);
     cell.alpha = 0;
     cell.layer.transform = rotation;
     cell.layer.anchorPoint = CGPointMake(0, 0.5);
     
     
     [UIView beginAnimations:@"rotation" context:NULL];
     [UIView setAnimationDuration:0.8];
     cell.layer.transform = CATransform3DIdentity;
     cell.alpha = 1;
     cell.layer.shadowOffset = CGSizeMake(0, 0);
     [UIView commitAnimations];
     */
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (appDelegate().isLoginApp) {
        if (indexPath.row ==0 && indexPath.section == 1) {
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
            self.navigationItem.backBarButtonItem = backItem;
            backItem.title = @"返回";
            OrderListViewController *pdvc = [[OrderListViewController alloc] initWithNibName:@"OrderListViewController" bundle:nil];
            [self.navigationController pushViewController:pdvc animated:YES];
            [pdvc release];
        }
//        else if (indexPath.row ==1 && indexPath.section == 1) {
//            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
//            self.navigationItem.backBarButtonItem = backItem;
//            backItem.title = @"返回";
//            EditViewController *pdvc = [[EditViewController alloc] initWithNibName:@"EditViewController" bundle:nil];
//            [self.navigationController pushViewController:pdvc animated:YES];
//            [pdvc release];
//        }
        else if (indexPath.row ==3 && indexPath.section == 1) {
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
            self.navigationItem.backBarButtonItem = backItem;
            backItem.title = @"返回";
            InboxViewController *pdvc = [[InboxViewController alloc] initWithNibName:@"InboxViewController" bundle:nil];
            [self.navigationController pushViewController:pdvc animated:YES];
            [pdvc release];
        }
        else if (indexPath.row ==2 && indexPath.section == 1) {
            
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
            self.navigationItem.backBarButtonItem = backItem;
            backItem.title = @"返回";
            ScanViewController *pdvc = [[ScanViewController alloc] initWithNibName:@"ScanViewController" bundle:nil];
            pdvc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:pdvc animated:YES];
            [pdvc release];
        }
        else if (indexPath.row ==1 && indexPath.section == 1) {
            
            CouponListViewController *pdvc = [[CouponListViewController alloc] initWithNibName:@"CouponListViewController" bundle:nil];
            pdvc.hidesBottomBarWhenPushed = YES;
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
            self.navigationItem.backBarButtonItem = backItem;
            backItem.title = @"返回";
            [self.navigationController pushViewController:pdvc animated:YES];
            //[self presentViewController:pdvc animated:YES completion:nil];
            [pdvc release];
        }
        
        
    }else{
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    
    
}

-(void) didSelectcell{

    [self tableView:self.pullTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1]];
}

-(void) fetchInfo {
    
    //网络数据获取解析
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    //__block ConfirmViewController *myself = self;
    [[JRFHttpClient shareInstance] fetchNetworkData:mGetInfo param:params block:^(JRFResultCode resultCode, id resultObj) {
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                NSDictionary *temp  = [resultObj objectForKey:@"object"];
                [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"integral"] forKey:@"jiFen"];
                [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"balance"] forKey:@"mMoney"];
                [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"buffer"] forKey:@"mQuan"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.isLoadDone = true;
                [self.pullTableView reloadData];
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self fetchInfo];
                        }
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                        
                    }
                }];
                
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
        
    }];
    
    
}


#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidScroll:scrollView];
    }
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidEndDragging:scrollView];
    }
}


#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    if(appDelegate().isLoginApp)
        [self fetchInfo];
    self.isRefresh = YES;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
    
}



#pragma mark - button 功能
//登录页面
-(void) toMyInfo:(id)sender {
    if(!appDelegate().isLoginApp){
        LoginViewController *pdvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        pdvc.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        [self.navigationController pushViewController:pdvc animated:YES];
        [pdvc release];
    }
    else{
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        self.navigationItem.backBarButtonItem = backItem;
        backItem.title = @"返回";
        MyfruitInfo *pdvc = [[MyfruitInfo alloc] initWithNibName:@"MyfruitInfo" bundle:nil];
        [self.navigationController pushViewController:pdvc animated:YES];
        self.isLoadDone = false;
        [pdvc release];
    }
    //     [UIView transitionFromView:self.view toView:tabbar.view duration:0.1f options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
    
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
