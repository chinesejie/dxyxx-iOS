//
//  CouponTableViewCell.h
//  Dax
//
//  Created by Jay_Ch on 15/5/7.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *mMoney;
@property (retain, nonatomic) IBOutlet UIButton *mCode;
@property (retain, nonatomic) IBOutlet UIImageView *mImage;

@end
