//
//  PurchaseCollectionViewCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/18.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "PurchaseCollectionViewCell.h"

@implementation PurchaseCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
}


- (void)dealloc {
    [_img release];
    [_title release];
    [_detail release];
    [_price release];
    [super dealloc];
}
@end
