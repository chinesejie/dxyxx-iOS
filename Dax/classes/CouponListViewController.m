//
//  CouponListViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/5/6.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CouponListViewController.h"
#import "FJPullTableView.h"
#import "OrderListCell.h"
#import "OrderDetail.h"
#import "LoginViewController.h"
#import "ProductInfo.h"
#import "DetailInfo.h"
#import "CouponViewController.h"
#import "CouponTableViewCell.h"
#import "ListViewCell.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "ScanViewController.h"

@interface CouponListViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (retain, nonatomic) NSArray *itemname;
@property (retain, nonatomic) NSArray *navitemname;
@property (nonatomic,retain) UISegmentedControl *segment;
@property (nonatomic,retain) UISegmentedControl *navsegment;
@property (nonatomic, retain) NSString *url;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL isCoupon;
@property int offset;
- (void)fetchTaskLists;
- (void)fetchTaskListsFinished:(JRFResultCode)code result:(NSArray *)tasks;
- (void)addPullTableView;


@end

@implementation CouponListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datasource = [NSMutableArray array];
    //分页实现
    
    self.itemname = @[@"已支付", @"已消费"];
    self.segment = [[UISegmentedControl alloc]initWithItems:self.itemname];
    self.segment.frame = CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20, [[UIScreen mainScreen] bounds].size.width, 40);
    [self.segment addTarget:self action:@selector(selected:) forControlEvents:UIControlEventValueChanged];
    self.segment.selectedSegmentIndex = 0;
    [self.segment removeFromSuperview];
    [self.view addSubview:self.segment ];
    [self setSegmentStyle];   
    self.automaticallyAdjustsScrollViewInsets = false;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
     self.navigationItem.title = @"代金券详情";
    self.navigationController.navigationBar.titleTextAttributes = dict;

       if (!self.isLoadTaskDone){
        self.offset = 1;
        [self fetchTaskLists];
    }
}

- (void)fetchTaskLists{
    long propId = self.segment.selectedSegmentIndex+2;;
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"currentPage": [NSNumber numberWithInt:self.offset],
                           @"pageSize": @5,
                           @"propId":[NSNumber numberWithLong:propId]};
    [params addGetParams:dict];
    __block CouponListViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mGetTickets param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultCode == JRF_RESULT_OK) {
            [myself fetchTaskListsFinished:resultCode result:resultObj];
        } else {
             [WFHudView showMsg:resultObj inView:nil];
        }
    }];
}
- (void)fetchTaskListsFinished:(JRFResultCode)code result:(id)tasks {
    if (tasks && [tasks isKindOfClass:[NSDictionary class]]) {
        if ([[[tasks objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
            self.isLoadTaskDone = YES;
            NSDictionary *temp  = [tasks objectForKey:@"object"];
            NSLog(@"%@", tasks);
            if (!tasks) {
                return;
            }
            if (self.isRefresh || self.offset ==1) {
                [self.datasource removeAllObjects];
            }
            int count = (int)self.datasource.count;
            NSArray *orderlist = [[NSArray alloc] init];
            orderlist = [temp objectForKey:@"recordList"];
            [self.datasource addObjectsFromArray:orderlist];
            if (count <= [self.datasource count]) {
                self.offset++;
            }
            if (!self.pullTableView) {
                [self addPullTableView];
            }
            [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
        }
        else if([[[tasks objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
            NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
            NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
            JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
            NSString *reqType = mLoginApp;
            NSDictionary *dict1 = @{@"phone" : phoneNum,
                                    @"password" : passwordOld};
            [params addPostParams:dict1];
            [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                [[DAXUtils sharedInstance] hideProgressHud];
                if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        [self fetchTaskLists];
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                        
                    }
                }
            }];
            
        }
        else   {
            NSString *msg = [tasks objectForKey:@"info"];
           [WFHudView showMsg:msg inView:nil];
        }
    }
}


-(void)selected:(id)sender{
    UISegmentedControl* control = (UISegmentedControl*)sender;
    switch (control.selectedSegmentIndex) {
        case 0:
            self.offset = 1;
            [self fetchTaskLists];
            break;
        default:
            self.offset = 1;
            [self fetchTaskLists];
            break;
    }
}


- (void)addPullTableView {
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+60,  self.view.frame.size.width,  self.view.frame.size.height-105)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
}



- (void)setSegmentStyle {
    [self.segment setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    self.segment.tintColor = [UIColor clearColor];
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor lightGrayColor],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateNormal];
    
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor colorWithRed:1 green:.77 blue:0 alpha:1],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateSelected];
    
    [self.segment setDividerImage:[UIImage imageNamed:@"divider.png"]
              forLeftSegmentState:UIControlStateNormal
                rightSegmentState:UIControlStateNormal
                       barMetrics:UIBarMetricsDefault];
    
}


#pragma mark - UITableViewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.datasource count];
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        return 102.0f;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        static NSString *identifyCell = @"OrderListCell";
        CouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
        if (!cell) {
            
            cell = (CouponTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"CouponTableViewCell"
                                                                         owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSDictionary *temp1 = [self.datasource objectAtIndex:indexPath.row];
        if (indexPath.row % 4 == 0 )
            cell.mImage.image = [UIImage imageNamed:@"coupon_r"];
        else if (indexPath.row % 4 == 1)
            cell.mImage.image = [UIImage imageNamed:@"coupon_b"];
        else if (indexPath.row % 4 == 2)
            cell.mImage.image = [UIImage imageNamed:@"coupon_y"];
        else
            cell.mImage.image = [UIImage imageNamed:@"coupon_g"];
        cell.mMoney.text = [NSString stringWithFormat:@"%.2f",[[temp1 objectForKey:@"money"] floatValue]];
        cell.mCode.tag = indexPath.row;
        [cell.mCode addTarget:self action:@selector(toCode:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *temp1 = [self.datasource objectAtIndex:indexPath.row];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    ScanViewController *pdvc = [[ScanViewController alloc] initWithNibName:@"ScanViewController" bundle:nil];
    pdvc.uuid = [temp1 objectForKey:@"uuid"];
    pdvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
}



#pragma mark   ==============时间戳转时间的方法==============
- (NSString *)transformTodate1:(NSString *) mTime
{
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MM月dd日"];
    
    //设置时区,这个对于时间的处理有时很重要
    //例如你在国内发布信息,用户在国外的另一个时区,你想让用户看到正确的发布时间就得注意时区设置,时间的换算.
    //例如你发布的时间为2010-01-26 17:40:50,那么在英国爱尔兰那边用户看到的时间应该是多少呢?
    //他们与我们有7个小时的时差,所以他们那还没到这个时间呢...那就是把未来的事做了
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    // 时间戳转时间的方法
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[mTime longLongValue]/1000 ];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}

- (NSString *)transformTodate2:(NSString *) mTime
{
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    //设置时区,这个对于时间的处理有时很重要
    //例如你在国内发布信息,用户在国外的另一个时区,你想让用户看到正确的发布时间就得注意时区设置,时间的换算.
    //例如你发布的时间为2010-01-26 17:40:50,那么在英国爱尔兰那边用户看到的时间应该是多少呢?
    //他们与我们有7个小时的时差,所以他们那还没到这个时间呢...那就是把未来的事做了
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    // 时间戳转时间的方法
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[mTime longLongValue]/1000 ];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}



#pragma mark - 按钮功能



//条形码
-(void) toCode:(id)sender {
    
    UIButton *temp = (UIButton *)sender;
    NSDictionary *temp1 = [self.datasource objectAtIndex:temp.tag];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    ScanViewController *pdvc = [[ScanViewController alloc] initWithNibName:@"ScanViewController" bundle:nil];
    pdvc.uuid = [temp1 objectForKey:@"uuid"];
    pdvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
    
}



#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidScroll:scrollView];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidEndDragging:scrollView];
    }
}

#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    
    self.isRefresh = YES;
    self.offset = 1;
    [self fetchTaskLists];
    
    
}
- (void)beginToLoadMoreData:(FJPullTableView *)tableView {
    self.isRefresh = NO;
    [self fetchTaskLists];
    
    
}



@end
