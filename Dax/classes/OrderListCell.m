//
//  OrderListCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/22.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "OrderListCell.h"

@implementation OrderListCell

- (void)awakeFromNib {
    // Initialization code
    self.borderview.layer.borderWidth = 0.5f;
    self.borderview.layer.borderColor = [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_borderview release];
    [_time release];
    [_date release];
    [_money release];
    [_checkOrder release];
    [_takeGoods release];
    [_payMoney release];
    [_mComment release];
    [_orderNo release];
    [_orderName release];
    [super dealloc];
}
@end
