//
//  UploadImage.m
//  Dax
//
//  Created by Jay_Ch on 15/5/30.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "UploadImage.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"

@implementation UploadImage

+ (void)uploadImageWithImage:(UIImage * ) image success:(void (^)(NSString * md5))success fail:(void (^)())fail
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:mUpload parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         NSData        * data;
         if (UIImageJPEGRepresentation(image, 0.7) != nil)
         {
             data = UIImageJPEGRepresentation(image, 0.7);
         }
         else
         {
             data = UIImagePNGRepresentation(image);
         }
         
         [formData appendPartWithFormData:data name:@"userfile"];
     }
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (success)
         {
             NSString  *aString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             NSInteger  index=[aString rangeOfString:@":"].location;
             NSRange    range;
             range.location=index+2;
             range.length=[aString length]-range.location-2;
             NSString  *md5=[aString substringWithRange:range];
             success(md5);
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if (fail)
         {
             fail();
         }
     }];
}


+(void) uploadImagesWithImages:(NSArray *)images success:(void (^)(NSArray *))success fail:(void (^)())fail
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:mUpload parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         for (UIImage * image in images)
         {
            
             NSData  * data;
             if (UIImageJPEGRepresentation(image, 0.7) != nil)
             {
                 data = UIImageJPEGRepresentation(image, 0.7);
             }
             else
             {
                 data = UIImagePNGRepresentation(image);
             }
             [formData appendPartWithFormData:data name:@"userfile"];
         }
     }
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if (success)
         {
             int count = 0;
             NSString  *aString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             NSInteger  index=[aString rangeOfString:@":"].location;
             NSRange    range;
             range.location=index+2;
             range.length=[aString length]-range.location-2;
             NSString *value=[aString substringWithRange:range];
             NSArray  *values =[value componentsSeparatedByString:@"\""];
             NSMutableArray * md5s=[NSMutableArray array];
             for (NSString * string in values)
             {
                 if (![string isEqualToString:@""])
                 {
                     UIImage *temp = [images objectAtIndex:count];
                     NSString *width = [NSString stringWithFormat:@"%d",(int)temp.size.width/2];
                      NSString *height = [NSString stringWithFormat:@"%d",(int)temp.size.height/2];
                    string =  [string  stringByAppendingFormat:@"_%@_%@", width,height];
                     [md5s addObject:string];
                     count++;
                 }
             }
             success(md5s);
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if (fail)
         {
             fail();
         }
     }];
}

@end
