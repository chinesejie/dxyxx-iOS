//
//  TopTableViewCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/12.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

@interface TopTableViewCell : UITableViewCell <SDCycleScrollViewDelegate>@property (nonatomic, retain) NSMutableArray *advertArray;
@property (retain, nonatomic) IBOutlet UIPageControl *pageCtrl;
@property (retain, nonatomic) IBOutlet UIButton *mOrder;
@property (retain, nonatomic) IBOutlet UIView *bg;
@property (retain, nonatomic) IBOutlet UIButton *mKaiTuan;
@property (retain, nonatomic) IBOutlet UIButton *mCoupon;
@property (retain, nonatomic) IBOutlet UIButton *mCouponlist;

- (void)configTopTableViewCell;
@end
