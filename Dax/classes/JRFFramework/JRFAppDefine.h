//
//  JRFAppDefine.h
//  DAX
//
//  Created by feng jia on 14-5-10.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#ifndef DAX_JRFAppDefine_h
#define DAX_JRFAppDefine_h

#if __has_feature(objc_arc)
#define mRelease(obj)   obj = nil;
#define mRetainProperty   @property (nonatomic, strong)
#else
#define mRelease(obj)   [obj release]; obj = nil;
#define mRetainProperty   @property (nonatomic, retain)
#endif

#endif
