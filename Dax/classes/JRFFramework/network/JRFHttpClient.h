//
//  JRFHttpClient.h
//  DAX
//
//  Created by feng jia on 14-5-10.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JRFHttpRequest.h"

/**
 *  作为网络请求时传递的参数对象
 */
@interface JRFParams : NSObject

//parserTag来识别是哪个parser服务下的请求
@property JRFParserTag parserTag;
//组织的get请求参数
@property (nonatomic, retain) NSDictionary *paramsGet;
//组织的post请求参数
@property (nonatomic, retain) NSDictionary *paramsPost;

- (id)initWithParseTag:(JRFParserTag)tag;
- (void)addGetParams:(NSDictionary *)params;
- (void)addPostParams:(NSDictionary *)params;

@end


/**
 *  暴漏给上层程序的类，用于获取网络请求和取消一个网络请求
 */
@interface JRFHttpClient : NSObject
+ (id)shareInstance;
/**
 *  发送网络请求通过block回调方式获取到状态码resultcode和最终结果result
 *
 *  @param reqType 用来组装请求的url； 用来标识唯一的网络请求
 *  @param param   一个参数对象包括解析时用到的tag、get请求参数以及post请求参数
 *  @param block   回调状态码以及结果
 */
- (void)fetchNetworkData:(NSString *)reqType param:(JRFParams *)param block:(CompleteBlock)block;

/**
 *   根据指定的请求type取消一个网络请求
 *
 *  @param reqType 用来标识唯一的网络请求
 */
- (void)cancelHttpRequest:(NSString *)reqType;

@end
