//
//  JRFDataParser.h
//  DAX
//
//  Created by feng jia on 14-5-10.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JRFHttpReqDefine.h"
@interface JRFDataParser : NSObject
/**
 *  根据请求的类型reqType（识别是哪个请求) 把请求返回的json数据
 *  解析成服务层需要的类型数据（NSDictionary/NSArray/NSString/Model等）
 *
 *  @param jsonValue  请求返回的json数据
 *  @param reqType    请求类型来识别是哪个请求
 *  @param resultCode 作为回调返回
 *
 *  @return 服务层需要的类型数据
 */
+ (id)dataParser:(id)jsonValue parseTag:(JRFParserTag)parserTag type:(NSString *)reqType resultCode:(JRFResultCode *)resultCode;
@end
