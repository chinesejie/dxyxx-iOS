//
//  JRFHttpRequest.h
//  DAX
//
//  Created by feng jia on 14-5-10.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JRFHttpReqDefine.h"

//用于数据回调的block， resultCode请求返回码， resultObj请求返回数据
typedef void(^CompleteBlock)(JRFResultCode resultCode, id resultObj);

/**
 *  发送get、post请求，通过CompleteBlock回调数据到上层
 */
@interface JRFHttpRequest : NSObject

@property (nonatomic, retain) NSDictionary *userInfo;
@property (nonatomic, copy) CompleteBlock completeBlock;
/**
 *  发送get请求
 *
 *  @param url      请求的url
 *  @param params   请求中的参数
 *  @param theBlock 回调的block
 */
- (void)getRequest:(NSString *)url params:(NSDictionary *)params block:(CompleteBlock)theBlock;
/**
 *  发送post请求
 *
 *  @param url      请求的url
 *  @param params   请求中的参数，用于构建body
 *  @param theBlock 回调的block
 */
- (void)postRequest:(NSString *)url params:(NSDictionary *)params block:(CompleteBlock)theBlock;

- (void)cancelRequest;

@end
