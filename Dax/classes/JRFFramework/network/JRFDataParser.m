//
//  JRFDataParser.m
//  DAX
//
//  Created by feng jia on 14-5-10.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import "JRFDataParser.h"

@implementation JRFDataParser
+ (id)dataParser:(id)jsonValue
        parseTag:(JRFParserTag)parserTag
            type:(NSString *)reqType
      resultCode:(JRFResultCode *)resultCode {
    
    id result = nil;
    
    NSString *parserName = [mDataParseClassDictionary objectForKey:[NSString stringWithFormat:@"%lu", (unsigned long)parserTag]];
    if (parserName) {
        Class class = NSClassFromString(parserName);
        if (class) {
            JRFResultCode code = JRF_RESULT_OK;
            
            id (*fun)(Class, SEL, id, id, JRFResultCode*)  = (id (*)(Class, SEL, id, id, JRFResultCode*))objc_msgSend;
            id result = fun(class, @selector(parserData:reqType:resultCode:), jsonValue, reqType, &code);
            
            //result = objc_msgSend(class, @selector(parserData:reqType:resultCode:), jsonValue, reqType, &code);
            *resultCode = code;
            return result;
        } else {
            NSLog(@"请检查 JRFHttpConfig.plist 是否配置了该parser服务！！");
        }
    } else {
        NSLog(@"请检查 JRFHttpReqDefine.h 是否配置了该parserTag服务！！");
    }
    *resultCode = JRF_RESULT_ERROR;
    return result;
}
@end
