//
//  JRFHttpReqDefine.h
//  DAX
//
//  Created by feng jia on 14-5-10.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#ifndef DAX_JRFHttpReqDefine_h
#define DAX_JRFHttpReqDefine_h

#import "JRFAppDefine.h"

typedef NS_ENUM(NSUInteger, JRFResultCode) {
    JRF_RESULT_OK = 2000,  //正确返回
    JRF_RESULT_UNKNOW,   //未知错误
    JRF_RESULT_ERROR,
    
    JRF_JSON_ERROR,  //json格式解析错误
    
    
    
    JRF_CONN_TIMEOUT = 5000,  //连接超时
    JRF_NOT_CONN,   //连接异常
    JRF_CONN_BADURL, //连接url错误
    JRF_NORMAL_CANCEL  //请求正常被cancel
};


#define APPCONFIG_PATH   [[NSBundle mainBundle] pathForResource:@"JRFHttpReqConfig" ofType:@"plist"]

#define KXF_BASEURL      [[NSDictionary dictionaryWithContentsOfFile:APPCONFIG_PATH] objectForKey:@"JRF_BASEURL"]

#define mDataParseClassDictionary [[NSDictionary dictionaryWithContentsOfFile:APPCONFIG_PATH] objectForKey:@"DataService"]

typedef NS_ENUM(NSUInteger, JRFParserTag) {
    JRF_PARSERTAG_  = 1000
};
#define mte @"[{"amount":1,"id":1,"price":23.0,"productId":1}]"
#define mTopbanner         @"https://rs88881234.com/chinesejie/topbanner/list"   //轮转图
#define mHomepagebanner    @"https://rs88881234.com/chinesejie/homepagebanner/listActive" //主界面列表
#define mInboxlist         @"https://rs88881234.com/chinesejie/user_message/get" //收件箱列表
#define mUploadtoserver     @"https://rs88881234.com/chinesejie/user_message/update" //反馈给服务器
#define mCategory          @"category/list" //类别列表
#define mListbycid         @"product/list?" //categoryId= 得到某一种类的所有的产品：
#define mGetTickets        @"order/getTickets?" //currentPage=1&pageSize=5
#define mGetGroupOrders    @"gorder/get?" //currentPage=1&pageSize=5
#define mLoginApp          @"login?"
#define mCheck             @"user/verify"
#define mTicketadd          @"order/addTicket?"//代金券订单
#define mOrderadd          @"order/add?" //提交订单
#define mGroupadd          @"gorder/add?" //提交团购订单
#define mGroupInstanceadd  @"gorder/attend?" //提交参团团购订单
#define mOrderCancel       @"order/cancelOrder?"//
#define mGorderCancel       @"gorder/cancelOrder?"//
#define mGorderTZhang       @"gorder/findById?"//团长信息
#define mGorderTYuan       @"gorder/groupinstance/get?"//团长信息

#define mWechatpay         @"tenpay/iosprepay?" //微信支付
#define mOrderfind         @"order/findOrdersByUser?" //订单查询  propId=1&currentPage=1&pageSize=5
#define mPassword          @"mob/getback?"//重置密码 phone=15201967268&password=12223&code=9947&type=ios
#define mZhuce             @"mob/smsregister?"//注册密码 phone=15201967268&password=12223&code=9947&type=ios
#define mproductDetail     @"productDetail/get?" //商品详情 productId=xxx
#define mgroupDetail       @"group/detailsByGid?" //开团订单详情 groupId=xxx
#define mgroupInstance     @"groupinstance/check?" //参团订单详情 groupinstanceId=
#define mCodeView          @"phones/SEND_PHONE_FOR_SCAN" //条形码
#define mTuan              @"https://rs88881234.com/chinesejie/html/group/group-index.html#/list" 
#define mSetimage          @"http://rs88881234.com:4869/"
#define mGetInfo            @"auxiliary/get"
#define mLogOut            @"user/logout"
#define mUpload          @"http://rs88881234.com/zimg/upload"//图片上传
#define mInfomodify          @"https://rs88881234.com/chinesejie/user/modify"//资料修改上传
#define mNewComment          @"comment/list?"//最新评论列表
#define mineComment          @"comment/minelist?"//个人评论列表
#define mPidComment      @"comment/listByPid?"//商品评论pid=xxx&current=1&count=5
#define mOrderPingfen         @"order_comment/add?"//订单评分提交
#define mOrderfen         @"order_comment/byUuid?"//订单评分
#define mGetTuanyuan         @"groupinstance/get?"//获取团员
#define mStoreList         @"store/list"//订单评分
#define mAddComment         @"https://rs88881234.com/chinesejie/comment/add"//增加评论
#define mAddReplys        @"https://rs88881234.com/chinesejie/comment/reply"//增加回复
#define mGetReplys       @"https://rs88881234.com/chinesejie/comment/getReplys"//回复列表
#define mSendToken       @"https://rs88881234.com/chinesejie/appleid/add"//发送token
#endif
