//
//  JRFHttpClient.m
//  DAX
//
//  Created by feng jia on 14-5-10.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import "JRFHttpClient.h"
#import "JRFDataParser.h"
@implementation JRFParams

- (void)dealloc {
    self.paramsGet = nil;
    self.paramsPost = nil;
#if __has_feature(objc_arc)
#else
    [super dealloc];
#endif
}
- (id)initWithParseTag:(JRFParserTag)tag {
    if (self = [super init]) {
        self.parserTag = tag;
        self.paramsGet = [NSDictionary dictionary];
    }
    return self;
}

- (void)addGetParams:(NSDictionary *)params {
    self.paramsGet = params;
}

- (void)addPostParams:(NSDictionary *)params {
    self.paramsPost = params;
}

@end

@interface JRFHttpClient()

mRetainProperty NSMutableArray *theRequestQueue;

- (void)sendRequest:(NSString *)url
             method:(NSString *)method
             params:(NSDictionary *)params
           userInfo:(NSDictionary *)userInfo
              block:(CompleteBlock)block;

@end
@implementation JRFHttpClient

+ (id)shareInstance {
    static JRFHttpClient *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[JRFHttpClient alloc] init];
    });
    return instance;
}

- (void)fetchNetworkData:(NSString *)reqType param:(JRFParams *)param block:(CompleteBlock)block {
    NSMutableString *url = [NSMutableString stringWithFormat:@"%@%@", KXF_BASEURL, reqType];
    if ([reqType isEqualToString:mUpload]) {
        url = [NSMutableString stringWithFormat:@"%@", reqType];

    }
    NSDictionary *params = param.paramsPost;
    NSString *method = @"POST";
    if (!params) {
        method = @"GET";
        params = param.paramsGet;
    }
    
    NSDictionary *userInfo = @{@"reqType": reqType};
    [self sendRequest:url method:method params:params userInfo:userInfo block:^(JRFResultCode resultCode, id resultObj) {
        if (resultCode == JRF_RESULT_OK) {
            JRFResultCode rCode = resultCode;
            id result = [JRFDataParser dataParser:resultObj parseTag:param.parserTag type:reqType resultCode:&rCode];
            block(rCode, result);
        } else {
            block(resultCode, resultObj);
        }
    }];
}

- (void)cancelHttpRequest:(NSString *)reqType {
    if (!reqType) {
        return;
    }
    for (JRFHttpRequest *request in self.theRequestQueue) {
        if ([reqType isEqualToString:[request.userInfo objectForKey:@"reqType"]]) {
            [request cancelRequest];
        }
    }
}

#define Private Methods

- (void)sendRequest:(NSString *)url
             method:(NSString *)method
             params:(NSDictionary *)params
           userInfo:(NSDictionary *)userInfo
              block:(CompleteBlock)block {
    if (!url) {
        block(JRF_RESULT_ERROR, @"请求的url不能为空");
    }
    if (!method) {
        block(JRF_RESULT_ERROR, @"需要定义请求方式");
    }
    if (![method isKindOfClass:[NSString class]]) {
        block(JRF_RESULT_ERROR, @"请求方式类型错误");
    }
    
    __block JRFHttpRequest *request = [[JRFHttpRequest alloc] init];
    __block JRFHttpClient *myself = self;
    request.userInfo = userInfo;
    [self.theRequestQueue addObject:request];
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        [request postRequest:url params:params block:^(JRFResultCode resultCode, id resultObj) {
            if (block) {
                if (resultCode == JRF_RESULT_OK) {
                    [myself convertJsonValue:resultObj block:block];
                } else if (resultCode == JRF_NORMAL_CANCEL) {
                    //正常cancel不做任何处理
                } else {
                    block(resultCode, resultObj);
                }
            }
            [myself.theRequestQueue removeObject:request];
            mRelease(request);
        }];
    } else if ([[method uppercaseString] isEqualToString:@"GET"]) {
        [request getRequest:url params:params block:^(JRFResultCode resultCode, id resultObj) {
            if (block) {
                if (resultCode == JRF_RESULT_OK) {
                    [myself convertJsonValue:resultObj block:block];
                } else if (resultCode == JRF_NORMAL_CANCEL) {
                    //正常cancel不做任何处理
                } else {
                    block(resultCode, resultObj);
                }
            }
            [myself.theRequestQueue removeObject:request];
            mRelease(request);
        }];
    } else {
        block(JRF_RESULT_ERROR, @"请求方式类型错误");
        mRelease(request);
    }
}
- (void)convertJsonValue:(id)resultObj block:(CompleteBlock)block {
    NSLog(@"%@", resultObj);
    NSError *error = nil;
//    NSString * responseString = [resultObj stringByReplacingOccurrencesOfString : @"\r\n" withString : @"" ];
//    responseString = [responseString stringByReplacingOccurrencesOfString : @"\n" withString : @"" ];
//    responseString = [responseString stringByReplacingOccurrencesOfString : @"\t" withString : @"" ];
    NSData *data = [resultObj dataUsingEncoding:NSUTF8StringEncoding];
    id result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    NSDictionary *dic = (NSDictionary *)result;
    NSLog(@"%@",dic);
    if (error ) {
        NSLog(@"jsonObject格式错误 %@", error.description);
        block(JRF_JSON_ERROR, @"jsonObject格式错误");
    }
    else {
        block(JRF_RESULT_OK, result);
    }
}

@end
