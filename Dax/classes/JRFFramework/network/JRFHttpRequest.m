//
//  JRFHttpRequest.m
//  DAX
//
//  Created by feng jia on 14-5-10.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import "JRFHttpRequest.h"

#define INTERVAL_TIMEOUT     30
@interface JRFHttpRequest()
@property (nonatomic, retain) NSMutableString *strUrl;
@property (nonatomic, retain) NSMutableURLRequest *request;
@property (nonatomic, retain) NSURLConnection *conn;
@property (nonatomic, retain) NSMutableData *content;

- (void)sendRequest;
- (void)buildGetParams:(NSDictionary *)params;
- (void)buildPostBody:(NSDictionary *)params;

@end

@implementation JRFHttpRequest

- (void)dealloc {
    NSLog(@"dealloc");
    self.completeBlock = nil;
    self.strUrl = nil;
    self.request = nil;
    self.conn = nil;
    self.content = nil;
    self.userInfo = nil;
    [super dealloc];
}
- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)getRequest:(NSString *)url params:(NSDictionary *)params block:(CompleteBlock)theBlock {
    self.completeBlock = [theBlock copy];
    self.strUrl = [NSMutableString stringWithString:url];
    
    [self buildGetParams:params];
    [self sendRequest];
}

- (void)postRequest:(NSString *)url params:(NSDictionary *)params block:(CompleteBlock)theBlock {
    self.completeBlock = [theBlock copy];
    self.strUrl = [NSMutableString stringWithString:url];
    
    [self buildPostBody:params];
    [self sendRequest];
    
}

- (void)sendRequest {
    NSURLConnection *connection = [[[NSURLConnection alloc] initWithRequest:self.request delegate:self startImmediately:YES] autorelease];
    self.conn = connection;
    [self.conn start];
}

- (void)cancelRequest {
    if (self.conn) {
        [self.conn cancel];
        self.conn = nil;
        self.completeBlock(JRF_NORMAL_CANCEL, @"取消了这个请求");
    }
    
}

- (void)buildGetParams:(NSDictionary *)params {
    
    //设置get参数形式 key1=value1&key2=value2&key3=value3 并将其拼接到url后面作为完整的url发送请求
    for (int i =0; i < [[params allKeys] count]; i++) {
        NSString *key = [[params allKeys] objectAtIndex:i];
        if (i == [[params allKeys] count] - 1) {
            [self.strUrl appendFormat:@"%@=%@", key, [params objectForKey:key]];
        } else {
            [self.strUrl appendFormat:@"%@=%@&", key, [params objectForKey:key]];
        }
    }
    [self convertURL:self.strUrl];
    NSLog(@"%@", self.strUrl);
    NSMutableURLRequest *req = [[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.strUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:INTERVAL_TIMEOUT] autorelease];
    self.request = req;
}

- (void)buildPostBody:(NSDictionary *)params {
    NSLog(@"%@", self.strUrl);
    NSMutableURLRequest *req = [[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.strUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:INTERVAL_TIMEOUT] autorelease];
    self.request = req;
    
    [self.request setHTTPMethod:@"POST"];
    if([self.strUrl isEqualToString:mUpload])
    {
        NSString *TWITTERFON_FORM_BOUNDARY = @"AaB03x";
         NSString *content=[[NSString alloc]initWithFormat:@"multipart/form-data; boundary=%@",TWITTERFON_FORM_BOUNDARY];
        [self.request setValue:content forHTTPHeaderField:@"Content-Type"];
    }
    else
        [self.request setValue:@"application/x-www-form-urlencoded;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableArray *arrParams = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSString *key in [params allKeys]) {
        NSString *paramString = [NSString stringWithFormat:@"%@=%@", key, [params objectForKey:key]];
        [arrParams addObject:paramString];
    }
    //设置post参数形式 key1=value1&key2=value2&key3=value3 将其转成NSData然后传给httpBody
    NSString *paramsContent = [arrParams componentsJoinedByString:@"&"];
    if (paramsContent.length > 0) {
        NSData *bodyData = [NSData dataWithBytes:[paramsContent UTF8String] length:paramsContent.length];
        [self.request setHTTPBody:bodyData];
        [self.request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)paramsContent.length] forHTTPHeaderField:@"Content-Length"];
    }
    [arrParams release];
}
- (void)convertURL:(NSString *)url {
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSData *data = [url dataUsingEncoding:NSUTF8StringEncoding];
    self.strUrl = [[[NSMutableString alloc] initWithData:data encoding:enc] autorelease];
}

#pragma mark - NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (!self.content) {
        self.content = [NSMutableData data];
    }
    [self.content setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if (self.content) {
        [self.content appendData:data];
    }
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"--------");
    if (self.completeBlock) {
        NSString *respString = [[NSString alloc] initWithData:self.content encoding:NSUTF8StringEncoding];
        self.completeBlock(JRF_RESULT_OK, respString);
        self.completeBlock = nil;
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"---=========%@  %ld", error, (long)error.code);
    if (self.completeBlock) {
        if (error.code == 2) {
            self.completeBlock(JRF_CONN_TIMEOUT, @"连接超时，请稍候再试");
        } else if (error.code == 5) {
            self.completeBlock(JRF_CONN_BADURL, @"请检查请求的URL是否正确");
        } else {
            self.completeBlock(JRF_NOT_CONN, @"请检查当前网络是否正常");
        }
        self.completeBlock = nil;
    }
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    static CFArrayRef certs;
    if (!certs) {
        NSData  *certData =[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"server" ofType:@"cer"]];

        SecCertificateRef rootcert =SecCertificateCreateWithData(kCFAllocatorDefault,CFBridgingRetain(certData));
        const void *array[1] = { rootcert };
        certs = CFArrayCreate(NULL, array, 1, &kCFTypeArrayCallBacks);
        CFRelease(rootcert);    // for completeness, really does not matter
    }
    
    SecTrustRef trust = [[challenge protectionSpace] serverTrust];
    int err;
    SecTrustResultType trustResult = 0;
    err = SecTrustSetAnchorCertificates(trust, certs);
    if (err == noErr) {
        err = SecTrustEvaluate(trust,&trustResult);
    }
    CFRelease(trust);
    BOOL trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed)||(trustResult == kSecTrustResultConfirm) || (trustResult == kSecTrustResultUnspecified));
    
    if (trusted) {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    }else{
        [challenge.sender cancelAuthenticationChallenge:challenge];
    }
}


@end
