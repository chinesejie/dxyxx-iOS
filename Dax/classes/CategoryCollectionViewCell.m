//
//  CategoryCollectionViewCell.m
//  Dax
//
//  Created by Jay_Ch on 15/3/14.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "CategoryCollectionViewCell.h"

@implementation CategoryCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, CGRectGetWidth(self.frame)-10, CGRectGetWidth(self.frame)-10)];
        [self addSubview:self.imgView];
        self.text = [[UILabel alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(self.imgView.frame), CGRectGetWidth(self.frame)-10, 20) ];
        self.text.textColor = [UIColor darkGrayColor];
        self.text.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.text];
        
    }
    return self;
}


@end
