//
//  ListViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/15.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "ListViewController.h"
#import "FJPullTableView.h"
#import "HomeTableViewCell.h"
#import "ListViewCell.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "PurchaseViewController.h"

@interface ListViewController ()<FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (retain, nonatomic) NSArray *itemname;
@property (nonatomic,retain) UISegmentedControl *segment;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL isLoadTaskDone;
@property BOOL isHighprice;
@property int offset;
@property int count;
- (void)fetchTaskLists:(NSString *)order;
- (void)fetchTaskListsFinished:(JRFResultCode)code result:(NSArray *)tasks;
- (void)addPullTableView;
@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datasource = [NSMutableArray array];
    
    //分页实现
    self.itemname = @[@"新品", @"热卖", @"价格"];
    self.segment = [[UISegmentedControl alloc]initWithItems:self.itemname];
    self.segment.frame = CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20, [[UIScreen mainScreen] bounds].size.width, 40);
    
    [self.segment addTarget:self action:@selector(selected:) forControlEvents:UIControlEventValueChanged];
    self.segment.selectedSegmentIndex = 0;
    [self.view addSubview:self.segment ];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    //[self addPullTableView];
    // Do any additional setup after loading the view from its nib.
}

- (void)dealloc {
    [_segment release];
    self.itemname = nil;
    [super dealloc];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.segment setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    self.segment.tintColor = [UIColor clearColor];
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor lightGrayColor],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateNormal];
    
    [self.segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor colorWithRed:1 green:.77 blue:0 alpha:1],UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                          [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0], UITextAttributeFont, nil] forState:UIControlStateSelected];
    
    [self.segment setDividerImage:[UIImage imageNamed:@"divider.png"]
              forLeftSegmentState:UIControlStateNormal
                rightSegmentState:UIControlStateNormal
                       barMetrics:UIBarMetricsDefault];
    
    self.navigationItem.title = self.mTitle;
    if (!self.isLoadTaskDone) {
        self.offset = 1;
        [self fetchTaskLists:@"newest"];
    }
    NSArray *list=self.navigationController.navigationBar.subviews;
    
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
}

- (void)fetchTaskLists:(NSString *)order{
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSDictionary *dict = @{@"categoryId": self.cId,
                           @"current": [NSNumber numberWithInt:self.offset],
                           @"count": @5,
                           @"order":order};
    [params addGetParams:dict];
    __block ListViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mListbycid param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultCode == JRF_RESULT_OK) {
            [myself fetchTaskListsFinished:resultCode result:resultObj];
        } else {
            [WFHudView showMsg:resultObj inView:nil];
        }
    }];
}
- (void)fetchTaskListsFinished:(JRFResultCode)code result:(NSMutableArray *)tasks {
    NSLog(@"%@", tasks);
    if (!tasks) {
        return;
    }
    self.isLoadTaskDone = YES;
    
    if (self.isRefresh || self.offset ==1) {
        [self.datasource removeAllObjects];
    }
    int count = (int)self.datasource.count;
    [self.datasource addObjectsFromArray:tasks];
    if (count <= [self.datasource count]) {
        self.offset++;
    }
    if (!self.pullTableView) {
        [self addPullTableView];
    }
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
}


-(void)selected:(id)sender{
    UISegmentedControl* control = (UISegmentedControl*)sender;
    switch (control.selectedSegmentIndex) {
        case 0:
            self.offset = 1;
            [self fetchTaskLists:@"newest"];
            break;
        case 1:
            self.offset = 1;
            [self fetchTaskLists:@"hot"];
            break;
        default:
            self.offset = 1;
            if (!self.isHighprice) {
                [self fetchTaskLists:@"lowerprice"];
                self.isHighprice = TRUE;
            }
            else{
                [self fetchTaskLists:@"highprice"];
                self.isHighprice = FALSE;
            }
            break;
    }
}

- (void)addPullTableView {
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+60,  self.view.frame.size.width,  self.view.frame.size.height-150)]  autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
}

#pragma mark - UITableViewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.datasource count];
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 116.0f;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifyCell = @"ListCell";
    ListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
    if (!cell) {
        cell = (ListViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"ListViewCell"
                                                              owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    DetailInfo *detail = [self.datasource objectAtIndex:indexPath.row];
    NSString *picUrl = mSetimage;
    picUrl = [picUrl  stringByAppendingFormat:@"%@", detail.thumbId];
    [cell.mThumb setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
    cell.mycart.hidden = FALSE;
    cell.mTitle.text = detail.mDescription;
    cell.mDescription.text = detail.mSpecification;
    cell.mPrice.text = [NSString stringWithFormat:@"%@", @([detail.price floatValue])];;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     CATransform3D rotation;
     rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
     rotation.m34 = 1.0/ -600;
     
     cell.layer.shadowColor = [[UIColor blackColor]CGColor];
     cell.layer.shadowOffset = CGSizeMake(10, 10);
     cell.alpha = 0;
     cell.layer.transform = rotation;
     cell.layer.anchorPoint = CGPointMake(0, 0.5);
     
     
     [UIView beginAnimations:@"rotation" context:NULL];
     [UIView setAnimationDuration:0.8];
     cell.layer.transform = CATransform3DIdentity;
     cell.alpha = 1;
     cell.layer.shadowOffset = CGSizeMake(0, 0);
     [UIView commitAnimations];
     */
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PurchaseViewController *pdvc = [[PurchaseViewController alloc] initWithNibName:@"PurchaseViewController" bundle:nil];
    pdvc.mProuduct = (DetailInfo *)[self.datasource objectAtIndex:indexPath.row];
    pdvc.cId = self.cId;
    pdvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:pdvc animated:YES];
    //[self presentViewController:pdvc animated:YES completion:nil];
    // [UIView transitionFromView:self.view toView:pdvc.view duration:0.1f options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
    [pdvc release];
    
    
}



#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidScroll:scrollView];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidEndDragging:scrollView];
    }
}


#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    
    self.isRefresh = YES;
    self.offset = 1;
    if(self.segment.selectedSegmentIndex == 0)
        [self fetchTaskLists:@"newest"];
    else if(self.segment.selectedSegmentIndex == 1){
        [self fetchTaskLists:@"hot"];
    }
    else {
        if (!self.isHighprice) {
            [self fetchTaskLists:@"lowerprice"];
        }
        else{
            [self fetchTaskLists:@"highprice"];
        }
    }
    
}
- (void)beginToLoadMoreData:(FJPullTableView *)tableView {
    self.isRefresh = NO;
    
    if(self.segment.selectedSegmentIndex == 0)
        [self fetchTaskLists:@"newest"];
    else if(self.segment.selectedSegmentIndex == 1){
        [self fetchTaskLists:@"hot"];
    }
    else {
        if (!self.isHighprice) {
            [self fetchTaskLists:@"lowerprice"];
        }
        else{
            [self fetchTaskLists:@"highprice"];
        }
    }
}

#pragma mark - 按钮功能

////加入购物车功能
//-(void) addTocart:(id)sender {
//    //获取购买数量&&同步数据
//    TopPurhcaseCell *cell = (TopPurhcaseCell *)[self.pullTableView cellForRowAtIndexPath:self.indexPath ];
//    self.count = cell.stepper.value;
//    NSLog(@"%d",self.count);
//    self.mProuduct.amount =  [NSNumber numberWithInteger:1];
//    if ([self.datasource count]==0) {
//        [self.mProuduct setValue:self.mProuduct.amount forKey:@"amount"];
//        [self.datasource insertObject:self.mProuduct atIndex:0];
//        [self addInfoWithName];
//    }
//    else{
//        for (int i=0; i< [self.datasource count]; i++) {
//            DetailInfo *temp = (DetailInfo *)[self.datasource objectAtIndex:i];
//            if ([temp.productId intValue]== [self.mProuduct.productId intValue] ) {
//                int count = [temp.amount floatValue] + [self.mProuduct.amount intValue];
//                temp.amount =  [NSNumber numberWithInteger:count];
//                [temp setValue: temp.amount forKey:@"amount"];
//                [self.datasource replaceObjectAtIndex:i withObject:temp];
//                self.isRepeat = TRUE;
//            }
//        }
//        if (!self.isRepeat) {
//            [self.mProuduct setValue:self.mProuduct.amount forKey:@"amount"];
//            [self.datasource insertObject:self.mProuduct atIndex:0];
//        }
//        [self addInfoWithName];
//    }
//
//    NSLog(@"datasource的信息 %lu",(unsigned long)[self.datasource count]);
//    DetailInfo *t1 = (DetailInfo *) [self.datasource objectAtIndex:0];
//    NSLog(@"该物品在购物车内的数量：%@",t1.amount);
//
//    //设置badge
//    NSString *temp1 = [NSString stringWithFormat:@"%lu",(unsigned long)[self.datasource count]];
//    CustomBadge *badge = [CustomBadge customBadgeWithString:temp1];
//    [badge setFrame:CGRectMake(24,0, 20,20)];
//    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
//    [animation setFromValue:[NSNumber numberWithFloat:1.5]];
//    [animation setToValue:[NSNumber numberWithFloat:1]];
//    [animation setDuration:0.2];
//    [animation setTimingFunction:[CAMediaTimingFunction functionWithControlPoints:.4 :1.3 :1 :1]];
//    [badge.layer addAnimation:animation forKey:@"bounceAnimation"];
//    [self.mCart addSubview:badge];
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
