//
//  DXDataParser.h
//  DAX
//
//  Created by fengjia on 14-5-22.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DXDataParser : NSObject


+ (id)parserData:(id)jsonValue reqType:(NSString *)reqType resultCode:(int *)resultCode;
@end
