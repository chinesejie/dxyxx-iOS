//
//  PurchaseCollectionViewCell.h
//  Dax
//
//  Created by Jay_Ch on 15/3/18.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PurchaseCollectionViewCell : UICollectionViewCell
@property (retain, nonatomic) IBOutlet UIImageView *img;
@property (retain, nonatomic) IBOutlet UILabel *title;
@property (retain, nonatomic) IBOutlet UILabel *detail;
@property (retain, nonatomic) IBOutlet UILabel *price;

@end
