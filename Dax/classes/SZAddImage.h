//
//  SZAddImage.h
//  Dax
//
//  Created by Jay_Ch on 15/4/20.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZAddImage : UIView
 @property (nonatomic, strong) NSMutableArray *images;
 @property  NSInteger count;
@end
