//
//  AppDelegate.m
//  Dax
//
//  Created by Jay_Ch on 15/3/11.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "AppDelegate.h"
#import "sys/utsname.h"
#import "TabBarViewController.h"
#import "WFHudView.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "NavigationController_3.h"
#import "MyfruitViewController.h"
#define appKey @"6cb9f8457c88"
#define mappSecret @"e6f8a15267c0151353a66130dd97f07a"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize isLoginApp;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //tabbarController初始化
    //[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:.29 green:0.46 blue:.26 alpha:1]];
    [self initializeDefaultDataList];
    [NSThread sleepForTimeInterval:3.0];

    NSString* lastLoginTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastLoginTime"];
    NSLog(@"%@",lastLoginTime);
    if (lastLoginTime != nil)
    {
        NSTimeInterval lastTime = [lastLoginTime doubleValue];
        NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970];
        if (nowTime-lastTime<3600*24*14 && [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"] !=nil )
        {
            self.isLoginApp = true;
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"jiFen"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mMoney"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mQuan"];
            //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"phone"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"portrait"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"name"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.isLoginApp = false;
        }
    }

    //注册推送服务
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
    {
        UIUserNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:myTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        //消息推送支持的类型
        UIRemoteNotificationType types = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        //注册消息推送
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: types];
    }

    
    
    
    //向微信注册
    [WXApi registerApp:APP_ID withDescription:@"大象与猩猩"];
    [SMS_SDK registerApp:appKey withSecret:mappSecret];
    
    [ShareSDK registerApp:@"5d0aeb031028"];//ShareSDK的AppKey
    
    //添加新浪微博应用
    [ShareSDK connectSinaWeiboWithAppKey:@"2546389459"
                               appSecret:@"ec42bdc09537069280285a32a0ffd3ab"
                             redirectUri:@"http://rs88881234.com"];
    //当使用新浪微博客户端分享的时候需要按照下面的方法来初始化新浪的平台
    [ShareSDK  connectSinaWeiboWithAppKey:@"2546389459"
                                appSecret:@"ec42bdc09537069280285a32a0ffd3ab"
                              redirectUri:@"http://rs88881234.com"
                              weiboSDKCls:[WeiboSDK class]];
    
    
    //添加QQ空间应用
    [ShareSDK connectQZoneWithAppKey:@"1103995166"
                           appSecret:@"td75sGoPtUHRH65r"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    
    //添加QQ应用  
    [ShareSDK connectQQWithQZoneAppKey:@"1103995166"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    //添加微信应用 注册网址 http://open.weixin.qq.com
    [ShareSDK connectWeChatWithAppId:@"wxa207119c4b14badc"
                           wechatCls:[WXApi class]];
    //微信登陆的时候需要初始化
    [ShareSDK connectWeChatWithAppId:@"wxa207119c4b14badc"
                           appSecret:@"2e8e35c078c2ff730e40879d4982a719"
                           wechatCls:[WXApi class]];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self initializeDefaultDataList];
    if ([self.datasource count] != 0) {
        TabBarViewController *matabbar = (TabBarViewController *)self.window.rootViewController;
       [[[matabbar.viewControllers objectAtIndex:3] tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%lu",(unsigned long)[self.datasource count]]];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}


//回调通知函数
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    NSLog(@"%@",url.host);
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                                             NSLog(@"result = %@",resultDic);
            NSString *resultStr = resultDic[@"result"];

        }];
        
    }
    else if ([url.host isEqualToString:@"pay"])
        return  [WXApi handleOpenURL:url delegate:self];
    else
        return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:self];
    return YES;
}


-(void) onResp:(BaseResp*)resp
{
    NSString *strMsg = [NSString stringWithFormat:@"errcode:%d", resp.errCode];
    NSString *strTitle = nil;
    
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        strTitle = [NSString stringWithFormat:@"提示"];
        
        switch (resp.errCode) {
            case WXSuccess:
                strMsg = @"分享成功！";
                break;
                
            default:
                strMsg = @"分享失败！";
                NSLog(@"错误，retcode = %d, retstr = %@", resp.errCode,resp.errStr);
                break;
        }

    }
    if([resp isKindOfClass:[PayResp class]]){
        //支付返回结果，实际支付结果需要去微信服务器端查询
        strTitle = [NSString stringWithFormat:@"支付结果"];
        
        switch (resp.errCode) {
            case WXSuccess:
                //[self presentViewController:mTabBar animated:NO completion:nil];
                strMsg = @"支付成功";
                NSLog(@"支付成功－PaySuccess，retcode = %d", resp.errCode);
                break;
                
            default:
                strMsg = @"支付失败,请重新支付";
                NSLog(@"错误，retcode = %d, retstr = %@", resp.errCode,resp.errStr);
                break;
        }
        if(resp.errCode == WXSuccess){
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            TabBarViewController *mTabBar = [storyBoard instantiateViewControllerWithIdentifier:@"idTabBar"];
            mTabBar.selectedIndex = 0;
            self.window.rootViewController = mTabBar;
        }
        
    }
  [WFHudView showMsg:strMsg inView:nil];

}

+ (NSString*)deviceString
{
    // 需要#import "sys/utsname.h"
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceString isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([deviceString isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([deviceString isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([deviceString isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([deviceString isEqualToString:@"iPhone4,1"])    return @"iPhone 4";
    if ([deviceString isEqualToString:@"iPhone5,2"])    return @"iPhone 5";
    if ([deviceString isEqualToString:@"iPhone3,2"])    return @"iPhone 4";
    if ([deviceString isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([deviceString isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([deviceString isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([deviceString isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([deviceString isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([deviceString isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([deviceString isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([deviceString isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([deviceString isEqualToString:@"i386"])         return @"Simulator";
    if ([deviceString isEqualToString:@"x86_64"])       return @"iPhone 4";
    NSLog(@"NOTE: Unknown device type: %@", deviceString);
    return deviceString;
}


// NSData转成NSMutableArray
-(void)initializeDefaultDataList{
    self.datasource = [[NSMutableArray alloc] init];
    NSData *savedEncodedData = [[NSUserDefaults standardUserDefaults] objectForKey:@"cartInfo"];
    if(savedEncodedData == nil)
    {
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        self.datasource = temp;
    }
    else{
        self.datasource = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:savedEncodedData];
        //appDelegate().datasource = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:savedEncodedData];
        NSLog(@"datasource的信息！！%@",self.datasource);
    }
}




#pragma mark - 消息推送 网络
//add by lxl
//获取 deviceToken 成功
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //这里进行的操作，是将Device Token发送到自己公司的服务端,因为deviceToken 可能会改变,所以需要在本地保存,当发现不一致的时候,同步到自己的服务器
    NSString *token = [NSString stringWithFormat:@"%@", deviceToken];
    
    NSLog(@"My APNS token is:%@", token);
    NSCharacterSet* set = [NSCharacterSet characterSetWithCharactersInString:@"<> "];
    
    //NSString *deviceNSToken = [NSString stringWithUTF8String:token];
    token = [token stringByTrimmingCharactersInSet:set];//坑啊,只能去除两端的空格
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];//删除中间空格
    //NotificationManager::setAPNSDeviceToken([token UTF8String]);
    self.token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    if(token!=self.token){
        self.token = token ;
        [[NSUserDefaults standardUserDefaults] setObject:self.token forKey:@"token"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    if(appDelegate().isLoginApp){
        //[self sendProviderDeviceToken:self.token];
        [(AppDelegate*)[UIApplication sharedApplication].delegate sendProviderDeviceToken:appDelegate().token];
    }
    
    
}

//注册消息推送失败
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSString *error_str = [NSString stringWithFormat: @"%@", error];
    NSLog(@"Failed to get token, error:%@", error_str);
}
/*
 0  command
 2  token length
 32 device token
 2  payload length
 34 notifications content 使用json 最长 256 byte
 
 {
 "aps":
 {
 "alert" : "You got a new message!",
 "badge" : 5,
 "sound" : "beep.wav"
 },
 "acme1" : "bar",
 "acme2" : 42
 }
 */
//处理收到的消息推送 当用户在游戏的时候才会调用
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    //获取支持那些类型的推动
    self.badgecount = [[[userInfo objectForKey:@"aps"] objectForKey:@"badge" ] intValue];
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:self.badgecount];
    //UIRemoteNotificationType enabledTypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if (application.applicationState != UIApplicationStateActive) {
        TabBarViewController *matabbar = (TabBarViewController *)self.window.rootViewController;
        NavigationController_3 *nav = (NavigationController_3 *)matabbar.viewControllers[2];
        MyfruitViewController *pdvc = (MyfruitViewController *)nav.childViewControllers[0];
        matabbar.selectedIndex = 2;
        [pdvc didSelectcell];
    }
    else {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.userInfo = userInfo;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        localNotification.fireDate = [NSDate date];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        self.FLAG = FALSE ;
    }
    completionHandler(UIBackgroundFetchResultNewData);
    
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    if (self.FLAG) {
        TabBarViewController *matabbar = (TabBarViewController *)self.window.rootViewController;
        NavigationController_3 *nav = (NavigationController_3 *)matabbar.viewControllers[2];
        MyfruitViewController *pdvc = (MyfruitViewController *)nav.childViewControllers[0];
        matabbar.selectedIndex = 2;
        [pdvc didSelectcell];
    }
    self.FLAG =TRUE;
    
}

//发送token
- (void)sendProviderDeviceToken: (NSString *)deviceTokenString
{
    NSDictionary *dict =  @{@"token":deviceTokenString};
    //获取网络数据
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    //异步访问网络，回调是主线程，更新UI
    [manager POST:mSendToken parameters:dict success:^(AFHTTPRequestOperation *operation, id resultObj) {
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                NSLog(@"发送成功");
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        appDelegate().isLoginApp =  true;
                        [(AppDelegate*)[UIApplication sharedApplication].delegate sendProviderDeviceToken:appDelegate().token];
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                        return ;
                        
                    }
                }];
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[DAXUtils sharedInstance] hideProgressHud];
        [WFHudView showMsg:@"网络好像出了点问题" inView:nil];
        return ;
    }];
    
}
@end
