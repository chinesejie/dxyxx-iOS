//
//  TabBarViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/3/11.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "TabBarViewController.h"
#import "HomeViewController.h"
@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0f],NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:.77 blue:0 alpha:1]} forState:UIControlStateSelected];
    self.tabBar.tintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
//    if ([appDelegate().datasource count] != 0) {
//         [[[self.viewControllers objectAtIndex:3] tabBarItem]setBadgeValue:[NSString stringWithFormat:@"%lu",(unsigned long)[appDelegate().datasource count]]];
//    }
//    
        // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
