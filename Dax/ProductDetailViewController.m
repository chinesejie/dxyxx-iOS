//
//  ProductDetailViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/4/25.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
@interface ProductDetailViewController ()
@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   //self.datasource = [NSMutableArray array];
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    NSString *reqType = mproductDetail;
    NSDictionary *dict = @{@"productId" : self.productId};
    [params addGetParams:dict];
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载..."];
    [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultCode == JRF_RESULT_OK && [resultObj isKindOfClass:[NSDictionary class]]) {
            NSData *data = [[resultObj objectForKey:@"details"] dataUsingEncoding:NSUTF8StringEncoding];
            id result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            NSDictionary *dic = (NSDictionary *)result;
            self.datasource = dic;
            [self setMyView];
        }
        
    }];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
   }

- (void) setMyView {
        float nextY = 0;
        for (NSDictionary *tempDict in self.datasource) {
        NSString *picUrl = mSetimage;
        NSString *detail = [tempDict objectForKey:@"detail"] ;
        NSArray *temp = [[tempDict objectForKey:@"image"] componentsSeparatedByString: @"_"];
        NSString *tempUrl = [temp objectAtIndex: 0];
        float height = [[temp objectAtIndex: 2] floatValue];
        float width = [[temp objectAtIndex: 1] floatValue];
        picUrl = [picUrl  stringByAppendingFormat:@"%@%@%f", tempUrl, @"?w=",[[UIScreen mainScreen] bounds].size.width];
        UIImageView *igv = [[[UIImageView alloc] initWithFrame:
                             CGRectMake(0.01*[[UIScreen mainScreen] bounds].size.width, nextY, 0.98*[[UIScreen mainScreen] bounds].size.width, (height/width)*0.98*[[UIScreen mainScreen] bounds].size.width)] autorelease];
        [igv setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];;
        [self.detailScrollview insertSubview:igv atIndex:0];
        nextY = CGRectGetMaxY(igv.frame) + 10;
        //[igv release];
        
        if (detail && detail.length != 0) {
        CGSize size = [detail sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(0.98*[[UIScreen mainScreen] bounds].size.width, 2000)];
        UILabel *lb = [[[UILabel alloc] initWithFrame:CGRectMake(0.01*[[UIScreen mainScreen] bounds].size.width, nextY, size.width, size.height)] autorelease];
        lb.font = [UIFont systemFontOfSize:14];
        lb.textColor = [UIColor darkGrayColor];
        lb.text = detail;
        nextY = CGRectGetMaxY(lb.frame) + 10;
        [self.detailScrollview insertSubview:lb atIndex:0];
            }
    }
    self.detailScrollview.showsVerticalScrollIndicator = FALSE;
    self.detailScrollview.hidden = NO;
    if (nextY > CGRectGetHeight(self.detailScrollview.frame) ) {
        self.detailScrollview.contentSize = CGSizeMake(CGRectGetWidth(self.detailScrollview.frame), nextY );
    }

 
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(UIButton *)sender {
    [[JRFHttpClient shareInstance] cancelHttpRequest:mproductDetail];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)dealloc {
    [_detailScrollview release];
    [super dealloc];
}
@end
