//
//  ProductCommentViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/6/19.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCommentViewController : UIViewController
@property (nonatomic, copy) NSString *productId;
@end
