//
//  MyfruitInfo.h
//  Dax
//
//  Created by Jay_Ch on 15/5/29.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyfruitInfo: UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,retain) NSString *username;
@property(nonatomic,retain) NSString *sex;
@property (retain, nonatomic) IBOutlet UIButton *mLogout;
@property(nonatomic,retain) NSString *birthday;
@end

