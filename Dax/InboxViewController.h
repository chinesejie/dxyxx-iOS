//
//  InboxViewController.h
//  DAX
//
//  Created by Jay_Ch on 15/1/25.
//  Copyright (c) 2015年 feng jia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNBlurModalView.h"
#import <QuartzCore/QuartzCore.h>

@interface InboxViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


@end
