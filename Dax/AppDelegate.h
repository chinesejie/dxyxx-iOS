//
//  AppDelegate.h
//  Dax
//
//  Created by Jay_Ch on 15/3/11.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AlipaySDK/AlipaySDK.h>
#import <SMS_SDK/SMS_SDK.h>
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import "WeiboSDK.h"
#import <TencentOpenAPI/QQApi.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <QZoneConnection/ISSQZoneApp.h>
// 账号帐户资料
//更改商户把相关参数后可测试

#define APP_ID          @"wxa207119c4b14badc"               //APPID
//支付结果回调页面
#define NOTIFY_URL      @"http://wxpay.weixin.qq.com/pub_v2/pay/notify.v2.php"
//获取服务器端支付数据地址（商户自定义）
#define SP_URL          @"http://wxpay.weixin.qq.com/pub_v2/app/app_pay.php"


@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSMutableArray *datasource;
@property (nonatomic, retain) NSString *token;
@property BOOL FLAG;
@property BOOL FLAG2;
@property BOOL isLoginApp;
@property (nonatomic, assign) int badgecount;
+ (NSString*)deviceString;
@end

