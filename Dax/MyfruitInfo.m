//
//  MyfruitInfo.m
//  Dax
//
//  Created by Jay_Ch on 15/5/29.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "MyfruitInfo.h"
#import "infoCell.h"
#import "UIButton+WebCache.h"
#import "SDImageCache.h"
#import "CorePhotoPickerVCManager.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "UploadImage.h"
#import "CoreActionSheetPicker.h"

@interface MyfruitInfo ()<UITextFieldDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
@property (nonatomic,strong) CorePhotoPickerVCManager *manager;
@property (nonatomic, retain) NSIndexPath *indexPath;
@property (nonatomic, retain) UITextField *mText;
@property (nonatomic, retain) NSString *md5;
@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;
@property (nonatomic, assign) UIDatePickerMode datePickerMode;
@property (nonatomic, strong) NSArray *sexs;
@property (nonatomic,assign) NSInteger selectedIndex;
@end

@implementation MyfruitInfo

- (void)viewDidLoad
{
    self.sexs = @[@"保密", @"帅哥", @"美女"];
    [self.mLogout addTarget:self action:@selector(toLogout:) forControlEvents:UIControlEventTouchUpInside];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.mLogout.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.mLogout.layer.cornerRadius = 5.0;
    self.mLogout.layer.borderWidth = 1.0;
    self.mLogout.layer.borderColor =[UIColor colorWithRed:1 green:.77 blue:0 alpha:1].CGColor;
    
    
    
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    self.navigationItem.title = @"个人信息";
    UIBarButtonItem *backButton= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(toBack:)];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark Table View Data Source Methods
//定义分组数
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
//定义分组行数
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0)
        return 2;
    else
        return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && indexPath.row == 0)
        return 60.0f;
    else
        return 45.0f;
}
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section]==0)
    {
        if([indexPath row]==0)
        {
            static NSString *modifyinfoTableIdentifier=@"LookInfoModelCell";
            infoCell *cell= (infoCell *)[tableView dequeueReusableCellWithIdentifier:modifyinfoTableIdentifier];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"infoCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            NSString *picUrl = mSetimage;
            picUrl = [picUrl  stringByAppendingFormat:@"%@?w=120&&p=0", [[NSUserDefaults standardUserDefaults] objectForKey:@"portrait"]];
            [cell.mPhoto addTarget:self action:@selector(toSetphoto:) forControlEvents:UIControlEventTouchUpInside];
            [cell.mPhoto setBackgroundImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:[UIImage imageNamed:@"loading.png"] options:SDWebImageRetryFailed];
            cell.titilelabel.text=@"头像";
            cell.mPhoto.layer.cornerRadius = 19.0;
            cell.mPhoto.clipsToBounds = TRUE;
            //cell.mPhoto.layer.borderWidth = 2.0;
            cell.mPhoto.hidden = false;
            cell.contentlabel.hidden = true;
            self.md5 = [[NSUserDefaults standardUserDefaults] objectForKey:@"portrait"];
            return cell;
        }
        else
        {
            static NSString *modifyinfoTableIdentifier=@"LookInfoModelCell";
            infoCell *cell= (infoCell *)[tableView dequeueReusableCellWithIdentifier:modifyinfoTableIdentifier];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"infoCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.contentlabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
            cell.titilelabel.text= @"昵称";
            self.username = cell.contentlabel.text;
            return cell;
        }
    }
    else
    {
        if([indexPath row]==0)
        {
            static NSString *modifyinfoTableIdentifier=@"LookInfoModelCell";
            infoCell *cell= (infoCell *)[tableView dequeueReusableCellWithIdentifier:modifyinfoTableIdentifier];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"infoCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            self.selectedIndex = [[[NSUserDefaults standardUserDefaults] objectForKey:@"sex"] integerValue];
            cell.contentlabel.text= (self.sexs)[[[[NSUserDefaults standardUserDefaults] objectForKey:@"sex"] integerValue]];
            cell.titilelabel.text= @"性别";
            self.sex = cell.contentlabel.text;
            return cell;
        }
        else
        {
            static NSString *modifyinfoTableIdentifier=@"LookInfoModelCell";
            infoCell *cell= (infoCell *)[tableView dequeueReusableCellWithIdentifier:modifyinfoTableIdentifier];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"infoCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.contentlabel.text= [[NSUserDefaults standardUserDefaults] objectForKey:@"birthday"];
            cell.titilelabel.text=@"生日";
            self.birthday = cell.contentlabel.text;
            return cell;
        }
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    infoCell *cell = (infoCell *)[tableView cellForRowAtIndexPath:indexPath ];
    cell.contentlabel.returnKeyType = UIReturnKeyDone;
    UIBarButtonItem *saveButton= [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(toSave:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    if (!cell.contentlabel.hidden && indexPath.section == 0) {
        cell.contentlabel.enabled = true;
        cell.contentlabel.delegate = self;
        self.mText =  cell.contentlabel;
        self.username = cell.contentlabel.text;
        [cell.contentlabel becomeFirstResponder];
    }
    else if(indexPath.section == 1 && indexPath.row == 1){
        [self.mText resignFirstResponder];
        UITextField *sender  = cell.contentlabel;
        ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"选择日期" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dataWasSelected:element:) origin:sender];
        
        [datePicker showActionSheetPicker];
    }
    else if(indexPath.section == 1 && indexPath.row == 0){
        [self.mText resignFirstResponder];
        UITextField *sender  = cell.contentlabel;
        [ActionSheetStringPicker showPickerWithTitle:@"请选择" rows:self.sexs initialSelection:self.selectedIndex target:self successAction:@selector(sexWasSelected:element:) cancelAction:nil origin:sender];
    }
    else {
        [self.mText resignFirstResponder];
        UIActionSheet *sheet=[[UIActionSheet alloc] initWithTitle:@"请选取" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"拍摄" otherButtonTitles:@"照片库", nil];
        [sheet showInView:self.view];
        
    }
    
}


- (void)sexWasSelected:(NSNumber *)selectedIndex element:(id)element {
    self.selectedIndex = [selectedIndex intValue];
    [element setText: (self.sexs)[(NSUInteger) self.selectedIndex]];
    self.sex = (self.sexs)[(NSUInteger) self.selectedIndex];
}

-(void)dataWasSelected:(NSDate *)selectedTime element:(id)element {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    [element setText:[dateFormatter stringFromDate:selectedTime]];
    self.birthday = [dateFormatter stringFromDate:selectedTime];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    CorePhotoPickerVCMangerType type=0;
    
    
    if(buttonIndex==0)
        type=CorePhotoPickerVCMangerTypeCamera;
    if(buttonIndex==1)
        type=CorePhotoPickerVCMangerTypeSinglePhoto;
    if(buttonIndex==2)
        return;
    
    
    CorePhotoPickerVCManager *manager=[CorePhotoPickerVCManager sharedCorePhotoPickerVCManager];
    
    //设置类型
    manager.pickerVCManagerType=type;
    
    //最多可选5张
    manager.maxSelectedPhotoNumber=5;
    
    //错误处理
    if(manager.unavailableType!=CorePhotoPickerUnavailableTypeNone){
        NSLog(@"设备不可用");
        return;
    }
    
    UIViewController *pickerVC=manager.imagePickerController;
    
    //选取结束
    manager.finishPickingMedia=^(NSArray *medias){
        
        [medias enumerateObjectsUsingBlock:^(CorePhoto *photo, NSUInteger idx, BOOL *stop) {
            NSLog(@"%@",photo.editedImage);
            self.indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            infoCell *cell = (infoCell *)[self.tableview cellForRowAtIndexPath:self.indexPath ];
            [UploadImage uploadImageWithImage:photo.editedImage success:^(NSString *md5) {
                self.md5= md5;
                [cell.mPhoto setBackgroundImage:photo.editedImage forState:UIControlStateNormal];
            } fail:^{
                mAlertView(@"提示", @"上传失败，请重新选择");
            }];
            
        }];
    };
    
    [self presentViewController:pickerVC animated:YES completion:nil];
    
    
}


//点击done 按钮 去掉键盘

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    UIBarButtonItem *saveButton= [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(toSave:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   
    [textField resignFirstResponder];
    return YES;
}
//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self resignFirstResponder];
}


#pragma mark - 按钮功能


-(void) toSetphoto:(id)sender {
    UIBarButtonItem *saveButton= [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(toSave:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    UIActionSheet *sheet=[[UIActionSheet alloc] initWithTitle:@"请选取" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"拍摄" otherButtonTitles:@"照片库", nil];
    [sheet showInView:self.view];
}

//注销
-(void) toLogout:(id)sender {
    //网络数据获取解析
    JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
    //__block ConfirmViewController *myself = self;
    [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在加载"];
    [[JRFHttpClient shareInstance] fetchNetworkData:mLogOut param:params block:^(JRFResultCode resultCode, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"jiFen"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mMoney"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mQuan"];
                //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"phone"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"portrait"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"name"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastLoginTime"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                appDelegate().isLoginApp =  false;
                [self.navigationController popViewControllerAnimated:YES];
                
                
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self toLogout:sender];
                        }
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                    }
                }];
                
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
        
    }];}

//返回
-(void) toBack:(id)sender {
    if (self.navigationItem.rightBarButtonItem != nil) {
        [self.mText resignFirstResponder];
        UIAlertView *malert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"信息未保存，确认退出吗？" delegate:self
                                               cancelButtonTitle:@"确定"
                                               otherButtonTitles:@"取消", nil];
        [malert show];
        //mAlertView(@"提示", @"信息未保存，确认退出吗");
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex==1)
        return;
    if(buttonIndex==0)
        [self.navigationController popViewControllerAnimated:YES];
}

-(void) toSave:(id)sender {
    [self.mText resignFirstResponder];
    if(!([self.mText.text isEqualToString:@""]) && self.mText != nil){
     self.username = self.mText.text;
     }
    else if(self.mText != nil){
        mAlertView(@"提示", @"昵称不能为空");
        return;
    }
    NSDictionary *dict = @{@"sex" : [NSNumber numberWithInteger:self.selectedIndex],
                           @"image": self.md5,
                           @"username" : self.username,
                           @"birthday" : self.birthday};

    //获取网络数据
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    //异步访问网络，回调是主线程，更新UI
    [manager POST:mInfomodify parameters:dict success:^(AFHTTPRequestOperation *operation, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                self.navigationItem.rightBarButtonItem = nil;
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat: @"%ld",(long)self.selectedIndex]  forKey:@"sex"];
                [[NSUserDefaults standardUserDefaults] setObject:self.birthday  forKey:@"birthday"];
                [[NSUserDefaults standardUserDefaults] setObject:self.username  forKey:@"username"];
                [[NSUserDefaults standardUserDefaults] setObject:self.md5  forKey:@"portrait"];
                
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]){
                        if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                            appDelegate().isLoginApp =  true;
                            [self toSave:sender];
                        }
                    }
                    else {
                        [self.tableview reloadData];
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                        
                    }
                }];
                
            }
            else   {
                NSString *msg = [resultObj objectForKey:@"info"];
                [WFHudView showMsg:msg inView:nil];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[DAXUtils sharedInstance] hideProgressHud];
         [WFHudView showMsg:@"网络好像出了点问题" inView:nil];
    }];

        
        
    
    
}


- (void)dealloc {
    [_tableview release];
    [_mLogout release];
    [super dealloc];
}
@end
