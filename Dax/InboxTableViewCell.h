//
//  AccountTableViewCell.h
//  DAX
//
//  Created by fengjia on 14-5-26.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class InboxInfo;
@interface InboxTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *leftIconIgv;
@property (retain, nonatomic) IBOutlet UILabel *lbtitle;
@property (retain, nonatomic) IBOutlet UILabel *lbdetail;
- (void)configInboxCell:(InboxInfo *)InboxInfo ;

@end

