//
//  SetPasswordViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/4/22.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetPasswordViewController : UIViewController
@property (retain, nonatomic) IBOutlet UITextField *tfConfirmPassword;
@property (retain, nonatomic) IBOutlet UITextField *tfPassword;
@property (retain, nonatomic) IBOutlet UITextField *tfPhone;
@property (retain, nonatomic) IBOutlet UITextField *tfVerify;
@property (retain, nonatomic) IBOutlet UIButton *mModifyPassword;
@property (retain, nonatomic) IBOutlet UIButton *mFetchVerify;
@property BOOL isRegister;
@end
