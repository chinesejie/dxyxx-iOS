//
//  AccountTableViewCell.m
//  DAX
//
//  Created by fengjia on 14-5-26.
//  Copyright (c) 2014年 feng jia. All rights reserved.
//

#import "InboxTableViewCell.h"
#import "InboxInfo.h"
#import<CoreText/CoreText.h>
#import "UIImageView+WebCache.h"
@interface InboxTableViewCell ()

@end

@implementation InboxTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configInboxCell:(InboxInfo *)InboxInfo {

    
    NSLog(@"content的信息！！%@",InboxInfo.content);
    self.lbtitle.text = InboxInfo.title;
    NSLog(@"text的信息！！%@",self.lbtitle.text);
    /*
    CGRect frame = self.lbtitle.frame;
    CGSize size = [InboxInfo.title sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(170, 36)];
    frame.size.width = size.width;
    frame.size.height = size.height;
    self.lbtitle.frame = frame;
    */
    //    [self.lbTitle sizeToFit];
    NSString *temp = InboxInfo.content;
    self.lbdetail.text  = [temp  stringByAppendingFormat:@"   %@", InboxInfo.website];
    
}

// 自绘分割线
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, rect);
    
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:0xE2/255.0f green:0xE2/255.0f blue:0xE2/255.0f alpha:1].CGColor);
    CGContextStrokeRect(context, CGRectMake(0,rect.size.height, rect.size.width,0.1));
}


- (void)dealloc {
    [_leftIconIgv release];
    [_lbtitle release];
    [_lbdetail release];
    [super dealloc];
}
@end
