//
//  LoginViewController.m
//  Dax
//
//  Created by Jay_Ch on 15/4/18.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import "LoginViewController.h"
#import "SetPasswordViewController.h"
#import "TabBarViewController.h"
#import "SecurityUtil.h"
@interface LoginViewController ()<UITextFieldDelegate>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tfPhone.delegate = self;
    self.tfPassword.delegate = self;
    self.tfPhone.keyboardType = UIKeyboardTypeNumberPad;
    self.tfPassword.keyboardType = UIKeyboardTypeAlphabet;
    self.tfPassword.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.tfPhone.clearButtonMode=UITextFieldViewModeWhileEditing;
      // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    self.mLogin.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.mLogin.layer.cornerRadius = 5.0;
    self.mLogin.layer.borderWidth = 1.0;
    self.mLogin.layer.borderColor =[UIColor colorWithRed:1 green:.77 blue:0 alpha:1].CGColor;
    self.mRegister.backgroundColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.mRegister.layer.cornerRadius = 5.0;
    self.mRegister.layer.borderWidth = 1.0;
    self.mRegister.layer.borderColor =[UIColor colorWithRed:1 green:.77 blue:0 alpha:1].CGColor;

    
    //去掉黑线
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            if([[NSString stringWithUTF8String:object_getClassName(obj)] isEqualToString:@"_UINavigationBarBackground"]){
                UIImageView *imageView=(UIImageView *)obj;
                
                imageView.hidden=YES;
            }
        }
        
    }
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    //导航栏透明
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg.png"] forBarMetrics:UIBarMetricsCompact];
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //title 颜色
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
    
    [self.mForget addTarget:self action:@selector(toReset:) forControlEvents:UIControlEventTouchUpInside];
    [self.mRegister addTarget:self action:@selector(toRegister:) forControlEvents:UIControlEventTouchUpInside];
    [self.mLogin addTarget:self action:@selector(toLogin:) forControlEvents:UIControlEventTouchUpInside];
    NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
    if (phoneNum) {
        self.tfPhone.text = phoneNum;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[JRFHttpClient shareInstance] cancelHttpRequest:mLoginApp];
}

#pragma mark - button 功能
//找回密码页面
-(void) toReset:(id)sender {
    SetPasswordViewController *pdvc = [[SetPasswordViewController alloc] initWithNibName:@"SetPasswordViewController" bundle:nil];
    pdvc.title = @"找回密码";
    pdvc.isRegister = NO;
    pdvc.hidesBottomBarWhenPushed = YES;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
    //     [UIView transitionFromView:self.view toView:tabbar.view duration:0.1f options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
    
    
}

//用户注册页面
-(void) toRegister:(id)sender {
    SetPasswordViewController *pdvc = [[SetPasswordViewController alloc] initWithNibName:@"SetPasswordViewController" bundle:nil];
    pdvc.title = @"用户注册";
    pdvc.isRegister = YES;
    pdvc.hidesBottomBarWhenPushed = YES;
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    self.navigationItem.backBarButtonItem = backItem;
    backItem.title = @"返回";
    [self.navigationController pushViewController:pdvc animated:YES];
    [pdvc release];
    //     [UIView transitionFromView:self.view toView:tabbar.view duration:0.1f options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
    
    
}

//登录
-(void) toLogin:(id)sender {
    [self.tfPhone resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    //self.tfPhone.text = @"13127626898";
    if (![[AppFun sharedInstance] isConnectNetwork]) {
        [WFHudView showMsg:@"网络好像出了点问题" inView:nil];
        return;
    }
    NSString *phoneNum = [self.tfPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *passwordOld = [self.tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([phoneNum isEqualToString:@""]) {
        mAlertView(@"提示", @"手机号码不能为空！");
        return;
    }
    //手机格式错误
    NSString *regex = @"^1\\d{10}$";
    //    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
    //[a-z0-9A-Z]+[-|_\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}
    NSPredicate *result = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if (![result evaluateWithObject:phoneNum]) {
        mAlertView(@"提示", @"手机号码格式错误, 请您再检查下!");
        return;
    }
    if ([passwordOld isEqualToString:@""]) {
        mAlertView(@"提示", @"请输入密码！");
        return;
    }
    
    else{
        
        JRFParams *params = [[[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_] autorelease];
        NSString *reqType = mLoginApp;
        passwordOld = [SecurityUtil encryptAESData:passwordOld app_key:@"dxyxx52dxyxx1314" ] ;
        passwordOld = [passwordOld stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
      
        NSDictionary *dict = @{@"phone" : phoneNum,
                               @"password" : passwordOld};
        [params addPostParams:dict];
        [[DAXUtils sharedInstance] showProgressHud:self.view withMessage:@"正在登录..."];
        [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
            [[DAXUtils sharedInstance] hideProgressHud];
            
            if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
                if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                    //int count = [self.datasource count]-indexPath.row-1;
                    NSDictionary *temp  = [resultObj objectForKey:@"object"];
                    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970]] forKey:@"lastLoginTime"];//保存登录时间
                    [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"name"] forKey:@"name"];
                    [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"username"] forKey:@"username"];
                    [[NSUserDefaults standardUserDefaults] setObject:passwordOld forKey:@"password"];
                    [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"phone"] forKey:@"phone"];
                    [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"image"] forKey:@"portrait"];
                    [[NSUserDefaults standardUserDefaults] setObject:[temp objectForKey:@"sex"] forKey:@"sex"];
                    [[NSUserDefaults standardUserDefaults] setObject:[self transformTodate:[temp objectForKey:@"birthday"]] forKey:@"birthday"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    appDelegate().isLoginApp = true;
                    
                    [(AppDelegate*)[UIApplication sharedApplication].delegate sendProviderDeviceToken:appDelegate().token];
                    [self.navigationController popViewControllerAnimated:NO];
                    NSLog(@"temp");
                }
                else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"0"]) {
                    NSString *msg = [resultObj objectForKey:@"info"];
                    mAlertView(@"提示", msg);                    
                }
            }
            
        }];
        
    }
    
    
}


#pragma mark   ==============时间戳转时间的方法==============
- (NSString *)transformTodate:(NSString *) mTime
{
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    //设置时区,这个对于时间的处理有时很重要
    //例如你在国内发布信息,用户在国外的另一个时区,你想让用户看到正确的发布时间就得注意时区设置,时间的换算.
    //例如你发布的时间为2010-01-26 17:40:50,那么在英国爱尔兰那边用户看到的时间应该是多少呢?
    //他们与我们有7个小时的时差,所以他们那还没到这个时间呢...那就是把未来的事做了
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    // 时间戳转时间的方法
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[mTime longLongValue]/1000 ];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{   //开始编辑时，整体上移
    if (textField == self.tfPassword && [[AppDelegate deviceString] isEqualToString:@"iPhone 4"]) {
        [self moveView:-110];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{     //结束编辑时，整体下移
    if (textField == self.tfPassword && [[AppDelegate deviceString] isEqualToString:@"iPhone 4"]) {
        [self moveView:110];
    }
}
-(void)moveView:(float)move{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y +=move;//view的X轴上移
    self.view.frame = frame;
    //[UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}


//点击done 按钮 去掉键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
//点击屏幕空白处去掉键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.tfPassword resignFirstResponder];
    [self.tfPhone resignFirstResponder];
}

- (void)dealloc {
    [_mLogin release];
    [_mForget release];
    [_mRegister release];
    [super dealloc];
}
@end
