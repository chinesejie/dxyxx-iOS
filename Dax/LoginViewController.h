//
//  LoginViewController.h
//  Dax
//
//  Created by Jay_Ch on 15/4/18.
//  Copyright (c) 2015年 Jay_Ch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIButton *mRegister;
@property (retain, nonatomic) IBOutlet UIButton *mLogin;
@property (retain, nonatomic) IBOutlet UIButton *mForget;
@property (retain, nonatomic) IBOutlet UITextField *tfPassword;
@property (retain, nonatomic) IBOutlet UITextField *tfPhone;
@end
