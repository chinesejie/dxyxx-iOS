//
//  InboxViewController.m
//  DAX
//
//  Created by Jay_Ch on 15/1/25.
//  Copyright (c) 2015年 feng jia. All rights reserved.
//

#import "InboxViewController.h"
#import "InboxTableViewCell.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "FJPullTableView.h"
#import "InboxInfo.h"

#define EVERY_COUNT     20
@interface InboxViewController () <FJPullTableViewDelegate,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic, retain) FJPullTableView *pullTableView;
@property (nonatomic, retain) NSMutableArray *datasource;
@property BOOL isReloadPage;
@property BOOL isRefresh;
@property BOOL isLoadDone;
@property int offset;
@property int pagecount;
- (void)fetchInboxLists;
@end

@implementation InboxViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addPullTableView];
    self.automaticallyAdjustsScrollViewInsets = false;
    [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
    self.datasource =[NSMutableArray array];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(appDelegate().isLoginApp && !self.isLoadDone){
        self.offset = 1;
        [self fetchInboxLists];
    }
    NSArray *list=self.navigationController.navigationBar.subviews;
    for (id obj in list) {
        
        if ([obj isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView=(UIImageView *)obj;
            
            imageView.hidden=NO;
            
        }
        
    }
    self.navigationItem.title = @"收件箱";
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:.77 blue:0 alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
//    if(self.navigationItem.leftBarButtonItem == nil){
//        UIBarButtonItem *backButton= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(toBack:)];
//        self.navigationItem.leftBarButtonItem = backButton;
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [super dealloc];
}
- (void)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//收件箱消息获取
- (void)fetchInboxLists{
    NSDictionary *dict = @{@"current": [NSNumber numberWithInt:self.offset],
                           @"size": @10};
    //获取网络数据
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    //异步访问网络，回调是主线程，更新UI
    [manager GET:mInboxlist parameters:dict success:^(AFHTTPRequestOperation *operation, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                self.pagecount = [[[resultObj objectForKey:@"object"] objectForKey:@"pageCount"]  intValue];
                NSDictionary *dict = [[resultObj objectForKey:@"object"] objectForKey:@"recordList"];
                if (!dict) {
                    return ;
                }
                if(self.offset <= self.pagecount)
                    self.offset++;
                if(self.isRefresh){
                    [self.datasource removeAllObjects];
                }
                
                NSLog(@"%@", dict);
                NSMutableArray *taskArray = [NSMutableArray array];
                for (NSDictionary *tempDict in dict) {
                    InboxInfo *inboxInfo = [[InboxInfo alloc] initWithDictionary:tempDict];
                    [taskArray addObject:inboxInfo];
                    [inboxInfo release];
                }
                if (!taskArray) {
                    return;
                }
                self.isLoadDone = YES;
                [self.datasource addObjectsFromArray:taskArray];
                if (!self.pullTableView) {
                    [self addPullTableView];
                }
                [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
                
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        appDelegate().isLoginApp =  true;
                        [self fetchInboxLists];
                    }
                    else {
                        NSString *msg = [resultObj objectForKey:@"info"];
                        [WFHudView showMsg:msg inView:nil];
                        return ;
                        
                    }
                }];
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[DAXUtils sharedInstance] hideProgressHud];
        mAlertView(@"提示",@"网络好像出了点问题");
    }];
}
- (void)addPullTableView {
    
    self.pullTableView = [[[FJPullTableView alloc] initWithFrame:CGRectMake(0,self.navigationController.navigationBar.frame.size.height+20, self.view.frame.size.width, self.view.frame.size.height-115)]   autorelease];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pullTableView.delegate = self;
    self.pullTableView.dataSource = self;
    self.pullTableView.pullDelegate = self;
    [self.pullTableView setIsShowRefresh:YES];
    [self.pullTableView setIsShowLoadMore:YES];
    self.pullTableView.backgroundColor = [UIColor clearColor];
    self.pullTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.pullTableView atIndex:0];
    self.pullTableView.tableHeaderView.hidden = TRUE;
    
}


#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.datasource count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifyCell = @"InboxCell";
    InboxTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifyCell];
    if (!cell) {
        cell = (InboxTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"InboxTableViewCell"
                                                                    owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    InboxInfo *temp = (InboxInfo *)[self.datasource objectAtIndex:indexPath.row];
    if ([temp.readd isEqualToString:@"0"])
        cell.leftIconIgv.image = [UIImage imageNamed:@"unread"];
    else
        cell.leftIconIgv.image = [UIImage imageNamed:@"read"];
    [cell configInboxCell:(InboxInfo *)[self.datasource objectAtIndex:indexPath.row]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL useCustomView = NO;
    RNBlurModalView *modal;
    if (useCustomView) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        view.backgroundColor = [UIColor redColor];
        view.layer.cornerRadius = 5.f;
        view.layer.borderColor = [UIColor blackColor].CGColor;
        view.layer.borderWidth = 5.f;
        
        modal = [[RNBlurModalView alloc] initWithView:view];
    }
    else {
        InboxInfo *inboxInfo =  (InboxInfo *)[self.datasource objectAtIndex:indexPath.row];
        modal = [[RNBlurModalView alloc] initWithTitle:inboxInfo.title message:inboxInfo.content website:inboxInfo.website];
        modal.defaultHideBlock = ^{
            NSLog(@"Code called after the modal view is hidden");
        };
        modal.defaultShowBlock = ^{
            NSLog(@"open");
            appDelegate().badgecount--;
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:appDelegate().badgecount];
            InboxTableViewCell *cell=(InboxTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            cell.leftIconIgv.image = [UIImage imageNamed:@"read"];
            [InboxViewController uploadtoServer:inboxInfo.tId];
        };
    }
    //    modal.dismissButtonRight = YES;
    [modal show];
    
}
//反馈服务器
+(void) uploadtoServer:(NSString *)Id{
    NSDictionary *dict = @{@"id": Id};
    //获取网络数据
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    //异步访问网络，回调是主线程，更新UI
    [manager POST:mUploadtoserver parameters:dict success:^(AFHTTPRequestOperation *operation, id resultObj) {
        [[DAXUtils sharedInstance] hideProgressHud];
        if (resultObj && [resultObj isKindOfClass:[NSDictionary class]]) {
            if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                NSLog(@"成功");
            }
            else if([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"-1"]) {
                NSString *phoneNum =  [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
                NSString *passwordOld = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
                JRFParams *params = [[JRFParams alloc] initWithParseTag:JRF_PARSERTAG_];
                NSString *reqType = mLoginApp;
                NSDictionary *dict1 = @{@"phone" : phoneNum,
                                        @"password" : passwordOld};
                [params addPostParams:dict1];
                [[JRFHttpClient shareInstance] fetchNetworkData:reqType param:params block:^(JRFResultCode resultCode, id resultObj) {
                    [[DAXUtils sharedInstance] hideProgressHud];
                    if ([[[resultObj objectForKey:@"code"] stringValue] isEqualToString:@"1"]) {
                        appDelegate().isLoginApp =  true;
                        [InboxViewController uploadtoServer:Id];
                    }
                    else {
                        //                        NSString *msg = [resultObj objectForKey:@"info"];
                        //                        [WFHudView showMsg:msg inView:nil];
                        //                        return ;
                        
                    }
                }];
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[DAXUtils sharedInstance] hideProgressHud];
        mAlertView(@"提示",@"网络好像出了点问题");
    }];
    
}

#pragma mark - UIScrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidScroll:scrollView];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self.pullTableView) {
        [self.pullTableView fjPullScrollViewDidEndDragging:scrollView];
    }
}

#pragma mark - 下拉刷新的代理方法
- (void)beginToRefreshData:(FJPullTableView *)tableView {//模拟刷新操作
    
    
    self.isRefresh = YES;
    self.offset = 1;
    [self fetchInboxLists];
    
}
- (void)beginToLoadMoreData:(FJPullTableView *)tableView {
    self.isRefresh = NO;
    [self fetchInboxLists];
    // [self.pullTableView fjPullScrollViewDataSourceDidFinishedLoading:self.pullTableView isRefresh:self.isRefresh];
}

//返回
-(void) toBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
